﻿using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class PlacementRequestViewModel : BaseRequestViewModel
    {
        public string Placement { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string AdvertiserRefCode { get; set; }

        public string AccountRefCode { get; set; }
    }
}