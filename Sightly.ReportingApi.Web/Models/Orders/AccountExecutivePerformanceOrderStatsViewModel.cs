using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class AccountExecutivePerformanceOrderStatsViewModel
    {
        #region Properties
        /// <summary>
        /// Property that holds the Campaign's account Name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser id.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser name.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the advertiser's category id.
        /// </summary>
        public Guid AdvertiserCategoryId { get; set; }

        /// <summary>
        /// Property that holds the advertiser's category name.
        /// </summary>
        public string AdvertiserCategoryName { get; set; }

        /// <summary>
        /// Property that holds the advertiser's sub-category id.
        /// </summary>
        public Guid AdvertiserSubCategoryId { get; set; }

        /// <summary>
        /// Property that holds the advertiser's category name.
        /// </summary>
        public string AdvertiserSubCategoryName { get; set; }

        /// <summary>
        /// Property that holds the campaign's average cost per completed view.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget.
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Property that holds the campaign's id.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign's name.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the campaign's clicks.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the campaign's completed views.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the campaign's cost per completed view.
        /// </summary>
        public decimal CPCV { get; set; }

        /// <summary>
        /// Property that holds the number of remaining campaign days.
        /// </summary>
        public int DaysLeft { get; set; }

        /// <summary>
        /// Property that holds the campaign's order end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's impressions.
        /// </summary>
        public long Impressions { get; set; }

        /// <summary>
        /// Property that holds the campaign's location id.
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Property that holds the campaign's location name.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Property that holds the campaign's order id.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the campaign's order name.
        /// </summary>
        public string OrderName { get; set; }

        /// <summary>
        /// Property that holds the order reference code of the campaign.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the campaign's pacing rate.
        /// </summary>
        public decimal PacingRate { get; set; }

        /// <summary>
        /// Property that holds the campaign's projected final spendings.
        /// </summary>
        public decimal ProjectedFinalSpend { get; set; }

        /// <summary>
        /// Property that holds the campaign's total reach.
        /// </summary>
        public decimal Reach { get; set; }

        /// <summary>
        /// Property that holds the campaign's remaining budget.
        /// </summary>
        public decimal RemainingBudget { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget to spend.
        /// </summary>
        public decimal Spend { get; set; }

        /// <summary>
        /// Property that holds the campaign's spent to date budget.
        /// </summary>
        public decimal SpendToDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's order start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's view rate.
        /// </summary>
        public decimal ViewRate { get; set; }

        /// <summary>
        /// Property that holds the campaign's view time in seconds.
        /// </summary>
        public int ViewTime { get; set; }

        #endregion

        #region Read-only Properties

        /// <summary>
        /// Read-only property that returns the campaign's
        /// formatted average CPCV.
        /// </summary>
        public string FormattedAvgCPCV { get; set; }

        /// <summary>
        /// Read-only property that returns the campaign's 
        /// formatted budget.
        /// </summary>
        public string FormattedBudget { get; set; }
        /// <summary>
        /// Read-only property that returns the campaign's 
        /// formatted completed views.
        /// </summary>
        public string FormattedCompletedViews { get; set; }

        /// <summary>
        /// Read-only property that returns the campaign's 
        /// formatted CPCV.
        /// </summary>
        public string FormattedCPCV { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted  
        /// remaining days in the campaign.
        /// </summary>
        public string FormattedDaysLeft { get; set; }
        /// <summary>
        /// Read-only property that returns the formatted
        /// end date of the campaign.
        /// </summary>
        public string FormattedEndDate { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// impressions of the campaign.
        /// </summary>
        public string FormattedImpressions { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// pacing rate of the campaign.
        /// </summary>
        public string FormattedPacingRate { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// projected final spend of the campaign.
        /// </summary>
        public string FormattedProjectedFinalSpend { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// reach of the campaign.
        /// </summary>
        public string FormattedReach { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// remaining budget of the campaign.
        /// </summary>
        public string FormattedRemainingBudget { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// campaign spend.
        /// </summary>
        public string FormattedSpend { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// spend to date of the campaign.
        /// </summary>
        public string FormattedSpendToDate { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// start date of the campaign.
        /// </summary>
        public string FormattedStartDate { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// view rate of the campaign.
        /// </summary>
        public string FormattedViewRate { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// view time seconds of the campaign.
        /// </summary>
        public string FormattedViewTime { get; set; }

        /// <summary>
        /// Read-only property that returns the formatted
        /// view time hours of the campaign.
        /// </summary>
        public string FormattedViewTimeHours { get; set; }

        /// <summary>
        /// Read-only property that returns the campaign's
        /// view time hours.
        /// </summary>
        public int ViewTimeHours { get; set; }
        #endregion
    
    }
}