﻿using Sightly.ReportingApi.Core.DataManagement;
using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents the request of ad stats information.
    /// </summary>
    public class AdStatsRequestViewModel : BaseRequestViewModel
    {
        /// <summary>
        /// Property that holds the campaign ID used for filtering ad stats.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the order ID used for filtering ad stats.
        /// </summary>
        public Guid OrderId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds information regarding the requested subset.
        /// </summary>
        public SubsetRequest SubsetRequest { get; set; }
    }
}