﻿using Sightly.ReportingApi.Core.DataManagement;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents the request for retrieving
    /// stats by order IDs.
    /// </summary>
    public class StatsByOrderIdsRequestViewModel : BaseRequestViewModel
    {
        /// <summary>
        /// Property that holds the end
        /// date of the requested stats.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds the ID of orders
        /// where to get the stats from.
        /// </summary>
        public List<Guid> OrderIds { get; set; }

        /// <summary>
        /// Property that holds the start
        /// date of the requested stats.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Property that holds the paging information
        /// that would be used to determine which subset
        /// would be retrieved from the database.
        /// </summary>
        public SubsetRequest SubsetRequest { get; set; }
    }
}