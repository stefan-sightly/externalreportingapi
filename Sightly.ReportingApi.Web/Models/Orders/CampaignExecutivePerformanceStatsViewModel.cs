﻿using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents campaign performance stats
    /// and executive performance stats information.
    /// </summary>
    public class CampaignExecutivePerformanceStatsViewModel
    {
        #region Properties
        /// <summary>
        /// Property that holds the advertiser's category id.
        /// </summary>
        public Guid AdvertiserCategoryId { get; set; }

        /// <summary>
        /// Property that holds the advertiser's category name.
        /// </summary>
        public string AdvertiserCategoryName { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser id.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser name.
        /// </summary>
        public string AdvertiserName { get; set; }
        
        /// <summary>
        /// Property that holds the advertiser's sub-category id.
        /// </summary>
        public Guid AdvertiserSubCategoryId { get; set; }

        /// <summary>
        /// Property that holds the advertiser's category name.
        /// </summary>
        public string AdvertiserSubCategoryName { get; set; }

        /// <summary>
        /// Property that holds the campaign's average cost per completed view.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget.
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Property that holds the campaign's id.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign's name.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the campaign's reference code.
        /// </summary>
        public string CampaignRefCode { get; set; }

        /// <summary>
        /// Property that holds the campaign's clicks.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the campaign's completed views.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the campaign's current oder status id.
        /// </summary>
        public Guid CurrentOrderStatusId { get; set; }

        /// <summary>
        /// Property that holds the campaign's order end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's impressions.
        /// </summary>
        public long Impressions { get; set; }

        /// <summary>
        /// Property that holds the campaign's location id.
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Property that holds the campaign's location name.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Property that holds the campaign's order id.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the order reference code of the campaign.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the campaign's order status name.
        /// </summary>
        public string OrderStatusName { get; set; }

        /// <summary>
        /// Property that holds the campaign's total reach.
        /// </summary>
        public int Reach { get; set; }

        /// <summary>
        /// Property that holds the campaign's spent to date budget.
        /// </summary>
        public decimal SpendToDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's order start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Property that holds the order targeting type's id.
        /// </summary>
        public Guid TargetingTypeId { get; set; }

        /// <summary>
        /// Property that holds the order targeting type's name.
        /// </summary>
        public string TargetingTypeName { get; set; }
        
        /// <summary>
        /// Property that holds the campaign's view rate.
        /// </summary>
        public decimal ViewRate { get; set; }

        /// <summary>
        /// Property that holds the campaign's view time.
        /// </summary>
        public int ViewTime { get; set; }

        /// <summary>
        /// Property that returns the campaign's
        /// view time hours.
        /// </summary>
        public int ViewTimeHours { get; set; }
        #endregion

        #region Formatted Properties
        /// <summary>
        /// Property that returns the campaign's
        /// formatted average CPCV.
        /// </summary>
        public string FormattedAvgCPCV { get; set; }

        /// <summary>
        /// Property that returns the campaign's 
        /// formatted budget.
        /// </summary>
        public string FormattedBudget { get; set; }

        /// <summary>
        /// Property that returns the campaign's 
        /// formatted completed views.
        /// </summary>
        public string FormattedCompletedViews { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// end date of the campaign.
        /// </summary>
        public string FormattedEndDate { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// impressions of the campaign.
        /// </summary>
        public string FormattedImpressions { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// reach of the campaign.
        /// </summary>
        public string FormattedReach { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// spend to date of the campaign.
        /// </summary>
        public string FormattedSpendToDate { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// start date of the campaign.
        /// </summary>
        public string FormattedStartDate { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// view rate of the campaign.
        /// </summary>
        public string FormattedViewRate { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// view time seconds of the campaign.
        /// </summary>
        public string FormattedViewTime { get; set; }

        /// <summary>
        /// Property that returns the formatted
        /// view time hours of the campaign.
        /// </summary>
        public string FormattedViewTimeHours { get; set; }
        #endregion
    }
}