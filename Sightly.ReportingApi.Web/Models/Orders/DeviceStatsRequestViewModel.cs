﻿using System;
using Sightly.ReportingApi.Core.DataManagement;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class DeviceStatsRequestViewModel : BaseRequestViewModel
    {
        /// <summary>
        /// Property that holds the campaign ID used for filtering ad stats.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the order ID used for filtering ad stats.
        /// </summary>
        public Guid OrderId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
}