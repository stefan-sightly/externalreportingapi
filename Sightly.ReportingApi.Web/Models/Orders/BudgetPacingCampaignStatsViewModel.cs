﻿using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents budget pacing information.
    /// </summary>
    public class BudgetPacingCampaignStatsViewModel
    {
        #region Properties
        /// <summary>
        /// Property that holds the campaign's advertiser id.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser name.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget.
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Property that holds the campaign's id.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign's name.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the number of remaining campaign days.
        /// </summary>
        public int DaysLeft { get; set; }

        /// <summary>
        /// Property that holds the campaign's order end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's order id.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the order reference code of the campaign.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the campaign's pacing rate.
        /// </summary>
        public int PacingRate { get; set; }

        /// <summary>
        /// Property that holds the campaign's projected final spendings.
        /// </summary>
        public decimal ProjectedFinalSpend { get; set; }

        /// <summary>
        /// Property that holds the campaign's remaining budget.
        /// </summary>
        public decimal RemainingBudget { get; set; }

        /// <summary>
        /// Property that holds the campaign's spent to date budget.
        /// </summary>
        public decimal SpendToDate { get; set; }

        /// <summary>
        /// Property that holds the campaign's order start date.
        /// </summary>
        public DateTime StartDate { get; set; }
        #endregion

        #region Formatted Properties
        /// <summary>
        /// Property that holds the formatted
        /// campaign's budget.
        /// </summary>
        public string FormattedBudget { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// number of remaining campaign days.
        /// </summary>
        public string FormattedDaysLeft { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's order end date.
        /// </summary>
        public string FormattedEndDate { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's pacing rate.
        /// </summary>
        public string FormattedPacingRate { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's projected final spendings.
        /// </summary>
        public string FormattedProjectedFinalSpend { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's remaining budget.
        /// </summary>
        public string FormattedRemainingBudget { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's spent to date budget.
        /// </summary>
        public string FormattedSpendToDate { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's order start date.
        /// </summary>
        public string FormattedStartDate { get; set; }
        #endregion
    }
}