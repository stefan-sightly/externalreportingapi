using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class OrderStatsRequestViewModel : BaseRequestViewModel
    {
        public OrderStatsRequest OrderStatsRequest { get; set; }
        public SubsetRequest SubsetRequest { get; set; }
    }
}