﻿using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents campaign performance information
    /// </summary>
    public class CampaignPerformanceStatsViewModel
    {
        #region Properties
        /// <summary>
        /// Property that holds the campaign's advertiser id.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser name.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the campaign's average cost per completed view.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget.
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Property that holds the campaign's id.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign's name.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the campaign's reference code.
        /// </summary>
        public string CampaignRefCode { get; set; }

        /// <summary>
        /// Property that holds the campaign's clicks.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the campaign's completed views.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the campaign's impressions.
        /// </summary>
        public long Impressions { get; set; }

        /// <summary>
        /// Property that holds the campaign's total reach.
        /// </summary>
        public int Reach { get; set; }

        /// <summary>
        /// Property that holds the campaign's budget to spend.
        /// </summary>
        public decimal Spend { get; set; }

        /// <summary>
        /// Property that holds the campaign's view rate.
        /// </summary>
        public decimal ViewRate { get; set; }

        /// <summary>
        /// Property that holds the campaign's view time.
        /// </summary>
        public int ViewTime { get; set; }
        #endregion

        #region Formatted Properties
        /// <summary>
        /// Property that holds the formatted
        /// campaign's average cost per completed view.
        /// </summary>
        public string FormattedAvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's budget.
        /// </summary>
        public string FormattedBudget { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's completed views.
        /// </summary>
        public string FormattedCompletedViews { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's impressions.
        /// </summary>
        public string FormattedImpressions { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's total reach.
        /// </summary>
        public string FormattedReach { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's budget to spend.
        /// </summary>
        public string FormattedSpend { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's view rate.
        /// </summary>
        public string FormattedViewRate { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// campaign's view time.
        /// </summary>
        public string FormattedViewTime { get; set; }

        /// <summary>
        /// Property that holds the formatted
        /// view time hours of the campaign.
        /// </summary>
        public string FormattedViewTimeHours { get; set; }

        /// <summary>
        /// Property that holds the campaign's
        /// view time hours.
        /// </summary>
        public int ViewTimeHours { get; set; }
        #endregion
    }
}