﻿namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class AccountRefCodeRequestViewModel : BaseRequestViewModel
    {
        public string AccountRefCode { get; set; }
    }
}