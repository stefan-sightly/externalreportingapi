﻿using System;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class OrderRequestViewModel : BaseRequestViewModel
    {
        public Guid OrderId { get; set; }
    }
}