﻿using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents the request for aggregated campaign stats data.
    /// </summary>
    public class AggregatedCampaignStatRequestViewModel : BaseRequestViewModel
    {
        #region Properties
        /// <summary>
        /// Property that holds the campaign's id.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the start date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the end date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds values used for filtering 
        /// campaigns with the specified order status ids.
        /// </summary>
        public List<Guid> OrderStatusIds { get; set; }
        #endregion
    }
}