﻿namespace Sightly.ReportingApi.Web.Models.Orders
{
    public class AccountChildAccountRequestViewModel : BaseRequestViewModel
    {
        public string ParentAccountRefCode { get; set; }
    }
}