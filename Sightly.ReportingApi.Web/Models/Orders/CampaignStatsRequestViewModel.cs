﻿using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Web.Models.Orders
{
    /// <summary>
    /// Class that represents campaign stats request information.
    /// </summary>
    public class CampaignStatsRequestViewModel : BaseRequestViewModel
    {
        /// <summary>
        /// Property that holds values used for filtering campaign stats visible to the current user.
        /// </summary>
        public CampaignStatsRequest CampaignStatsRequest { get; set; }

        /// <summary>
        /// Property that holds information regarding the requested subset.
        /// </summary>
        public SubsetRequest SubsetRequest { get; set; }
    }
}