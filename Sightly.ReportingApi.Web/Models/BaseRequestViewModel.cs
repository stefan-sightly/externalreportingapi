﻿using System;

namespace Sightly.ReportingApi.Web.Models
{
    /// <summary>
    /// Base class of web API session request view models
    /// </summary>
    public abstract class BaseRequestViewModel
    {
        /// <summary>
        /// Property that holds the ID of the API session
        /// </summary>
        public Guid ApiSessionId { get; set; }
    }
}