﻿using AutoMapper;
using Sightly.CampaignStatsReportGenerator.Commands;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Web.Mappings.BusModels
{
    /// <summary>
    /// Class that provides mapping profile
    /// configurations for bus agent models and
    /// reporting API models.
    /// </summary>
    public class CampaignStatsReportGeneratorMappingProfile : Profile
    {
        #region Protected Methods
        /// <summary>
        /// Method used to map models related campaign
        /// stats report generator service bus.
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<ExecutivePerformanceReportRequest, SendExecutivePerformanceReport>();
            Mapper.CreateMap<BudgetPacingReportRequest, SendBudgetPacingReport>();
            Mapper.CreateMap<CampaignPerformanceReportRequest, SendCampaignPerformanceReport>();
        }
        #endregion
    }
}