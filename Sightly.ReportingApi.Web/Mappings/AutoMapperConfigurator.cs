﻿using AutoMapper;
using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Web.Mappings.BusModels;
using Sightly.ReportingApi.Web.Mappings.Models;

namespace Sightly.ReportingApi.Web.Mappings
{
    /// <summary>
    /// Class in charge of the configuration of object mappings using AutoMapper
    /// </summary>
    public class AutoMapperConfigurator : IObjectMappingConfigurator
    {
        /// <summary>
        /// Method used to initialize object mapping configurations
        /// </summary>
        public void Initialize()
        {
            Mapper.Initialize(
                       c =>
                       {
                           c.AddProfile<OrdersMappingProfile>();
                           c.AddProfile<CampaignStatsReportGeneratorMappingProfile>();
                       }
                   );
        }
    }
}