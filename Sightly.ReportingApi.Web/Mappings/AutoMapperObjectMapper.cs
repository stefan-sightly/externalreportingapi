﻿using AutoMapper;
using SightlyInfastructure = Sightly.ReportingApi.Business.Interfaces.Infrastructures;

namespace Sightly.ReportingApi.Web.Mappings
{
    /// <summary>
    /// Class that maps object data from one type to another.
    /// </summary>
    public class AutoMapperObjectMapper : SightlyInfastructure.IObjectMapper
    {
        /// <summary>
        /// Method used for converting source object to an instance of the specified type.
        /// </summary>
        /// <typeparam name="T">Mapping destination type.</typeparam>
        /// <typeparam name="S">Mapping source type.</typeparam>
        /// <param name="source">Source object to be converted to destination type.</param>
        /// <returns>The converted source instance in destination type form.</returns>
        public T Map<T, S>(S source)
        {
            return Mapper.Map<S, T>(source);
        }
    }
}