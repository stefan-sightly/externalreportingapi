﻿using AutoMapper;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Models.Orders;

namespace Sightly.ReportingApi.Web.Mappings.Models
{
    /// <summary>
    /// Class that provides mapping profile configurations for models under the Orders schema
    /// </summary>
    public class OrdersMappingProfile : Profile
    {
        #region Protected Methods
        /// <summary>
        /// Method used to map models under the Orders schema
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<AggregatedCampaignStatRequestViewModel, AggregatedCampaignStatRequest>();
            Mapper.CreateMap<BudgetPacingReportRequestViewModel, BudgetPacingReportRequest>();
            Mapper.CreateMap<CampaignOrdersRequestViewModel, CampaignOrdersRequest>();
            Mapper.CreateMap<CampaignPerformanceReportRequestViewModel, CampaignPerformanceReportRequest>();
            Mapper.CreateMap<CampaignStats, BudgetPacingCampaignStatsViewModel>();
            Mapper.CreateMap<CampaignStats, CampaignExecutivePerformanceStatsViewModel>();
            Mapper.CreateMap<CampaignStats, CampaignPerformanceStatsViewModel>();
            Mapper.CreateMap<CampaignStats, ExecutivePerformanceCampaignStatsViewModel>();
            Mapper.CreateMap<CampaignTargetedGeographyRequestViewModel, CampaignTargetedGeographyRequest>();
            Mapper.CreateMap<CampaignTargetedPopulationRequestViewModel, CampaignTargetedPopulationRequest>();
            Mapper.CreateMap<ExecutivePerformanceReportRequestViewModel, ExecutivePerformanceReportRequest>();
            Mapper.CreateMap<OrderStats, AccountExecutivePerformanceOrderStatsViewModel>();
        }
        #endregion
    }
}