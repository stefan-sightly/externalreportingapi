﻿using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to order record
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class OrderController : ApiController
    {
        #region Fields
        private IOrderRepository orderRepository;
        private IObjectMapper objectMapper;
        #endregion

        #region Constructor
        public OrderController(
                   IObjectMapper objectMapper,
                   IOrderRepository orderRepository
               )
        {
            this.objectMapper = objectMapper;
            this.orderRepository = orderRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving list of orders under a 
        /// campaign
        /// </summary>
        /// <param name="campaignOrdersRequest">
        ///     Object that holds filtering values that will 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     List of campaign orders under a campaign
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetCampaignOrders")]
        public IHttpActionResult GetCampaignOrders(CampaignOrdersRequestViewModel requestViewModel)
        {
            var request = objectMapper.Map<
                              CampaignOrdersRequest,
                              CampaignOrdersRequestViewModel
                          >(requestViewModel);

            var campaignOrders = orderRepository.GetCampaignOrders(request);

            return Ok(campaignOrders);
        }

        /// <summary>
        /// Action used for retrieving the target population
        /// on a specific order
        /// </summary>
        /// <param name="orderId">
        ///     An identifier that is used to retrieve a target population 
        ///     under a specified campaign.
        /// </param>
        /// <returns>
        ///     A computed number of a target population under a campaign.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetTargetPopulation")]
        public IHttpActionResult GetTargetPopulation(Guid sid, Guid orderId)
        {
            var population = orderRepository.GetTargetPopulation(orderId);

            return Ok(population.HasValue ? population.Value.ToString("N0") : "0");
        }

        /// <summary>
        /// Action used for retrieving order reference
        /// codes together with their order ID
        /// under the current user's account.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <returns>
        ///     List of order reference codes together
        ///     with their order ID.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetReferenceCodeList")]
        public IHttpActionResult GetReferenceCodeList(Guid sid)
        {
            var referenceCodes = orderRepository.GetReferenceCodeList(sid);

            return Ok(referenceCodes);
        }


        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetOrderContextBySessionId")]
        public IHttpActionResult GetOrderContextBySessionId(SessionRequestViewModel request)
        {
            List<OrderContext> orderContexts = orderRepository.GetOrderContextBySessionId(request.ApiSessionId);
            return Ok(orderContexts);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetOrderFlightDataByOrderId")]
        public IHttpActionResult GetOrderFlightDataByOrderId(OrderRequestViewModel request)
        {
            OrderFlightData flightData = orderRepository.GetOrderFlightDataByOrderId(request.OrderId);
            return Ok(flightData);
        }
        


        #endregion
    }
}
