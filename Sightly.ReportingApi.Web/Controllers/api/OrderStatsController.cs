﻿using System.Collections.Generic;
using System.Web.Http;
using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models;
using Sightly.ReportingApi.Web.Models.Orders;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    [ApiSessionAuthorize]
    [CacheControl]
    public class OrderStatsController : ApiController
    {
        private IOrderStatsRepository orderStatsRepository;
        private IObjectMapper objectMapper;

        public OrderStatsController(IOrderStatsRepository orderStatsRepository, IObjectMapper objectMapper)
        {
            this.orderStatsRepository = orderStatsRepository;
            this.objectMapper = objectMapper;
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAccountExecutivePerformanceOrderStats")]
        public IHttpActionResult GetAccountExecutivePerforanceOrderStats(OrderStatsRequestViewModel request)
        {
            var orderStatsSubset = orderStatsRepository.GetExecutivePerformanceOrderStats(
                request.ApiSessionId,
                request.OrderStatsRequest,
                request.SubsetRequest);

            var list = objectMapper.Map<
                    List<AccountExecutivePerformanceOrderStatsViewModel>,
                    List<OrderStats>
                >(orderStatsSubset.RequestedSubset);

            return Ok(new
            {
                RequestedSubset = list,
                SubsetCount = orderStatsSubset.SubsetCount,
                TotalCount = orderStatsSubset.TotalCount
            });
        }
    }
}
