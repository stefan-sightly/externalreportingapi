﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DataStore.Dapper.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles order status related requests.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class OrderStatusController : ApiController
    {
        #region Fields
        private IOrderStatusRepository orderStatusRepository;
        #endregion

        #region Constructor
        public OrderStatusController(IOrderStatusRepository orderStatusRepository)
        {
            this.orderStatusRepository = orderStatusRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving a list of order statuses
        /// under the current user's account.
        /// </summary>
        /// <param name="sid">Session ID.</param>
        /// <returns>
        ///     List of order status that will be used for filtering 
        ///     data by their status.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var orderStatuses = orderStatusRepository.GetList();

            return Ok(orderStatuses);
        }
        #endregion
    }
}