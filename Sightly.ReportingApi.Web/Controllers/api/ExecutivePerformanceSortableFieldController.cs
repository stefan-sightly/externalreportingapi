﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to executive performance sortable fields.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class ExecutivePerformanceSortableFieldController : ApiController
    {
        #region Fields
        private IExecutivePerformanceSortableFieldRepository executivePerformanceSortableFieldRepository;
        #endregion

        #region Constructors
        public ExecutivePerformanceSortableFieldController(
                    IExecutivePerformanceSortableFieldRepository executivePerformanceSortableFieldRepository
                )
        {
            this.executivePerformanceSortableFieldRepository = executivePerformanceSortableFieldRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all 
        /// executive performance sortable 
        /// fields
        /// </summary>
        /// <returns>
        ///     List of Executive Performance summary 
        ///     field that has the can be sorted. 
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var results = this.executivePerformanceSortableFieldRepository.GetList();

            return Ok(results);
        }
        #endregion
    }
}