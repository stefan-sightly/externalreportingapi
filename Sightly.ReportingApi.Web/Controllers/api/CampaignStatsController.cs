﻿using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles campaign stats related requests.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class CampaignStatsController : ApiController
    {
        #region Fields
        private ICampaignStatsRepository campaignStatsRepository;
        private IObjectMapper objectMapper;
        #endregion

        #region Constructors
        public CampaignStatsController(
                   ICampaignStatsRepository campaignStatsRepository,
                   IObjectMapper objectMapper
               )
        {
            this.campaignStatsRepository = campaignStatsRepository;
            this.objectMapper = objectMapper;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action that retrieves aggregated campaign stats.
        /// </summary>
        /// <param name="aggregatedCampaignStatsRequest">
        ///     A request object that contains properties that 
        ///     filters Campaign Stats records.
        /// </param>
        /// <returns>
        ///     List of aggregated campaign stats.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAggregatedCampaignStats")]
        public IHttpActionResult GetAggregatedCampaignStats(AggregatedCampaignStatRequestViewModel requestViewModel)
        {
            var request = objectMapper.Map<
                              AggregatedCampaignStatRequest,
                              AggregatedCampaignStatRequestViewModel
                          >(requestViewModel);

            var aggregatedCampaignStats = campaignStatsRepository.GetAggregatedCampaignStats(request);

            return Ok(objectMapper.Map<CampaignPerformanceStatsViewModel, CampaignStats>(aggregatedCampaignStats));
        }

        /// <summary>
        /// Action that handles request for retrieving budget 
        /// pacing campaign stats that were visible to the 
        /// currently logged-in user.
        /// </summary>
        /// <param name="request">
        ///     Filtering data that would be used to retrieve 
        ///     executive budget pacing stats data.
        /// </param>
        /// <returns>
        ///     List of budget pacing campaign stats.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetBudgetPacingCampaignStats")]
        public IHttpActionResult GetBudgetPacingCampaignStats(CampaignStatsRequestViewModel request)
        {
            var campaignStatsSubset = campaignStatsRepository.GetBudgetPacingCampaignStats(
                                                                  request.ApiSessionId,
                                                                  request.CampaignStatsRequest,
                                                                  request.SubsetRequest
                                                              );

            var list =  objectMapper.Map<
                            List<BudgetPacingCampaignStatsViewModel>,
                            List<CampaignStats>
                        >(campaignStatsSubset.RequestedSubset);

            return Ok(new 
            {
                RequestedSubset = list,
                SubsetCount = campaignStatsSubset.SubsetCount,
                TotalCount = campaignStatsSubset.TotalCount
            });
        }

        /// <summary>
        /// Action that handles request for retrieving
        /// campaign executive performance.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="orderId">
        ///     ID of the order where to get campaign stats from.
        /// </param>
        /// <returns>
        ///     Campaign executive performance record.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetCampaignExecutivePerformanceById")]
        public IHttpActionResult GetCampaignExecutivePerformanceById(Guid sid, Guid orderId)
        {
            var campaignStats = campaignStatsRepository.GetCampaignExecutivePerformanceById(orderId);

            return Ok(objectMapper.Map<CampaignExecutivePerformanceStatsViewModel, CampaignStats>(campaignStats));
        }

        /// <summary>
        /// Action that handles request for retrieving executive 
        /// performance campaign stats that were visible to the 
        /// currently logged-in user.
        /// </summary>
        /// <param name="request">
        ///     Filtering data that would be used to retrieve 
        ///     executive performance campaign stats data.
        /// </param>
        /// <returns>
        ///     List of executive performance campaign stats.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetExecutivePerformanceCampaignStats")]
        public IHttpActionResult GetExecutivePerformanceCampaignStats(CampaignStatsRequestViewModel request)
        {
            var campaignStatsSubset = campaignStatsRepository.GetExecutivePerformanceCampaignStats(
                                                                  request.ApiSessionId,
                                                                  request.CampaignStatsRequest,
                                                                  request.SubsetRequest
                                                              );

            var list = objectMapper.Map<
                           List<ExecutivePerformanceCampaignStatsViewModel>,
                           List<CampaignStats>
                       >(campaignStatsSubset.RequestedSubset);

            return Ok(new
            {
                RequestedSubset = list,
                SubsetCount = campaignStatsSubset.SubsetCount,
                TotalCount = campaignStatsSubset.TotalCount
            });
        }

        /// <summary>
        /// Method used for issuing commands for sending 
        /// budget pacing summary report to campaign stats
        /// report generator service.
        /// </summary>
        /// <param name="requestViewModel">
        ///     Object that contains parameters for
        ///     generating the budget pacing
        ///     report.
        /// </param>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("SendBudgetPacingReport")]
        public IHttpActionResult SendBudgetPacingReport(BudgetPacingReportRequestViewModel requestViewModel)
        {
            var request = objectMapper.Map<
                              BudgetPacingReportRequest,
                              BudgetPacingReportRequestViewModel
                          >(requestViewModel);

            campaignStatsRepository.SendBudgetPacingReportRequest(requestViewModel.ApiSessionId, request);
            return Ok();
        }

        /// <summary>
        /// Method used for issuing commands for sending
        /// campaign performance report to campaign report
        /// generator service.
        /// </summary>
        /// <param name="requestViewModel">
        ///     Object that contains parameters for
        ///     generating the campaign performance 
        ///     report.
        /// </param>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("SendCampaignPerformanceReport")]
        public IHttpActionResult SendCampaignPerformanceReport(CampaignPerformanceReportRequestViewModel requestViewModel)
        {
            var request = objectMapper.Map<
                             CampaignPerformanceReportRequest,
                             CampaignPerformanceReportRequestViewModel
                         >(requestViewModel);

            campaignStatsRepository.SendCampaignPerformanceReportRequest(requestViewModel.ApiSessionId, request);

            return Ok();
        }

        /// <summary>
        /// Method used for issuing commands for sending 
        /// executive performance report to campaign stats
        /// report generator service.
        /// </summary>
        /// <param name="requestViewModel">
        ///     Object that contains parameters for
        ///     generating the executive performance
        ///     report.
        /// </param>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("SendExecutivePerformanceReport")]
        public IHttpActionResult SendExecutivePerformanceReport(ExecutivePerformanceReportRequestViewModel requestViewModel)
        {
            var request = objectMapper.Map<
                              ExecutivePerformanceReportRequest,
                              ExecutivePerformanceReportRequestViewModel
                          >(requestViewModel);

            campaignStatsRepository.SendExecutivePeformanceReportRequest(requestViewModel.ApiSessionId, request);
            return Ok();
        }
        #endregion
    }
}