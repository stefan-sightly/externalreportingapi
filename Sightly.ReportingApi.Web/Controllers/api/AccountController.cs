﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.ReportingApi.Web.Models.Orders;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request
    /// related to account records
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class AccountController : ApiController
    {
        #region Fields
        private IAccountRepository accountRepository;
        #endregion

        #region Constructor
        public AccountController(IAccountRepository accountRepository)
        {
            this.accountRepository = accountRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used to get a list of the user's sub-accounts
        /// </summary>
        /// <param name="sid">Session id of the user.</param>
        /// <returns>List of accounts under the account of the provided user's session id.</returns>
        [ApiSessionExtender]
        [ActionName("GetSubAccounts")]
        public IHttpActionResult GetSubAccounts(Guid sid)
        {
            var responseData = new List<Account>();
            responseData.AddRange(this.accountRepository.GetSubAccounts(sid));

            return Ok(responseData);
        }


        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetSubAccountsByParentRefCode")]
        public IHttpActionResult GetSubAccountsByParentRefCode(AccountChildAccountRequestViewModel request)
        {
            var responseData = new List<AccountRefCode>();
            responseData.AddRange(accountRepository.GetSubaccountsByParentRefCode(request.ApiSessionId, request.ParentAccountRefCode));

            return Ok(responseData);
        }

        #endregion
    }
}
