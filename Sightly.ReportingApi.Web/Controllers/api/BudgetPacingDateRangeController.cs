﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to budget pacing date range record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class BudgetPacingDateRangeController : ApiController
    {
        private IBudgetPacingDateRangeRepository budgetPacingDateRangeRepository;

        #region Constructor
        public BudgetPacingDateRangeController(
                    IBudgetPacingDateRangeRepository budgetPacingDateRangeRepository
                )
        {
            this.budgetPacingDateRangeRepository = budgetPacingDateRangeRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all 
        /// budget pacing date range
        /// </summary>
        /// <returns>
        ///     Budget Pacing date range needed for the 
        ///     filtering of data by dates.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var list = this.budgetPacingDateRangeRepository.GetList();

            return Ok(list);
        }
        #endregion
    }
}