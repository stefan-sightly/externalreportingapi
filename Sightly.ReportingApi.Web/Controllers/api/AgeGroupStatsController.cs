﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to age group stats.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class AgeGroupStatsController : ApiController
    {
        #region Fields
        private IAgeGroupStatsRepository ageGroupStatsRepository;
        #endregion

        #region Constructor
        public AgeGroupStatsController(
                   IAgeGroupStatsRepository ageGroupStatsRepository
               )
        {
            this.ageGroupStatsRepository = ageGroupStatsRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving age group list 
        /// of a specified order.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="orderId">
        ///     ID of order that owns the target 
        ///     age group-stats.
        /// </param>
        /// <returns>
        ///     List of age group stats of the
        ///     specified order.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByParentId")]
        public IHttpActionResult GetListByParentId(Guid sid, Guid orderId)
        {
            var list = this.ageGroupStatsRepository.GetListByParentId(orderId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated age
        /// group list under a specified campaign id.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="campaignId">
        ///     A filtering id that will be used to retrieve 
        ///     aggregated age group stats.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByCampaignId")]
        public IHttpActionResult GetListByCampaignId(Guid sid, Guid campaignId)
        {
            var list = this.ageGroupStatsRepository.GetListByCampaignId(campaignId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated age group
        /// stats using the provided order IDs.
        /// </summary>
        /// <param name="request">
        ///     Request info that contains ID of orders
        ///     where stats will be fetched from.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats based on
        ///     the specified order IDs.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByOrderIds")]
        public IHttpActionResult GetListByOrderIds(StatsByOrderIdsRequestViewModel request)
        {
            var list = this.ageGroupStatsRepository.GetListByOrderIds(
                                                        request.OrderIds,
                                                        request.StartDate,
                                                        request.EndDate
                                                    );

            return Ok(list);
        }
        #endregion
    }
}
