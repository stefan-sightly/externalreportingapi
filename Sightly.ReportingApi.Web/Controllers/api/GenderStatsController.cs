﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to gender stats record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class GenderStatsController : ApiController
    {
        #region Fields
        private IGenderStatsRepository genderStatsRepository;
        #endregion

        #region Constructor
        public GenderStatsController(
                   IGenderStatsRepository genderStatsRepository
               )
        {
            this.genderStatsRepository = genderStatsRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all gender stats 
        /// under a campaign stats record.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="orderId">
        ///     ID of order that owns the target 
        ///     gender-stats.
        /// </param>
        /// <returns>
        ///     List of gender stats records under the
        ///     specified order.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByParentId")]
        public IHttpActionResult GetListByParentId(Guid sid, Guid orderId)
        {
            var list = this.genderStatsRepository.GetListByParentId(orderId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated gender
        /// stats list of a specified campaign id.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="campaignId">
        ///     Campaign id of aggregated gender stats record.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats under the specified
        ///     campaign id.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByCampaignId")]
        public IHttpActionResult GetListByCampaignId(Guid sid, Guid campaignId)
        {
            var list = this.genderStatsRepository.GetListByCampaignId(campaignId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated gender stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="request">
        ///     Request info that contains ID of orders
        ///     where stats will be fetched from.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats based on
        ///     the specified order IDs.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByOrderIds")]
        public IHttpActionResult GetListByOrderIds(StatsByOrderIdsRequestViewModel request)
        {
            var list = this.genderStatsRepository.GetListByOrderIds(
                                                            request.OrderIds,
                                                            request.StartDate,
                                                            request.EndDate
                                                  );

            return Ok(list);
        }
        #endregion
    }
}