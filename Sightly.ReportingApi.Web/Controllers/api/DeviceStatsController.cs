﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to device stats record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class DeviceStatsController : ApiController
    {
        #region Fields
        private IDeviceStatsRepository deviceStatsRepository;
        #endregion

        #region Constructor
        public DeviceStatsController(IDeviceStatsRepository deviceStatsRepository)
        {
            this.deviceStatsRepository = deviceStatsRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all device stats 
        /// under a campaign stats record.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="orderId">
        ///     Id of order that owns the target 
        ///     device-stats.
        /// </param>
        /// <returns>
        ///     List of device stats records under the
        ///     specified order.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByParentId")]
        public IHttpActionResult GetListByParentId(Guid sid, Guid orderId)
        {
            var list = this.deviceStatsRepository.GetListByParentId(orderId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated device stats
        /// list of a specified campaign id.
        /// </summary>
        /// <param name="sid">
        ///     API session ID.
        /// </param>
        /// <param name="campaignId">
        ///     Campaign id of aggregated device stats record.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats under the
        ///     specified campaign id.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetListByCampaignId")]
        public IHttpActionResult GetListByCampaignId(Guid sid, Guid campaignId)
        {
            var list = this.deviceStatsRepository.GetListByCampaignId(campaignId);

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving aggregated device
        /// stats using the provided order IDs.
        /// </summary>
        /// <param name="request">
        ///     Request info that contains ID of orders
        ///     where stats will be fetched from.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats based on
        ///     the specified order IDs.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByOrderIds")]
        public IHttpActionResult GetListByOrderIds(StatsByOrderIdsRequestViewModel request)
        {
            var list = this.deviceStatsRepository.GetListByOrderIds(
                                                      request.OrderIds,
                                                      request.StartDate,
                                                      request.EndDate
                                                  );

            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetDeviceStatsByOrderAndDate")]
        public IHttpActionResult GetDeviceStatsByOrderAndDate(DeviceStatsRequestViewModel request)
        {
            var stats = this.deviceStatsRepository.GetDeviceStatsByOrderAndDates(
                request.OrderId,
                request.StartDate,
                request.EndDate,
                request.ApiSessionId);

            return Ok(stats);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByCampaignAndDate")]
        public IHttpActionResult GetListByCampaignAndDate(DeviceStatsRequestViewModel request)
        {
            var list = this.deviceStatsRepository.GetListByCampaignAndDates(
                request.CampaignId,
                request.StartDate,
                request.EndDate,
                request.ApiSessionId
                );

            return Ok(list);
        }
        #endregion
    }
}