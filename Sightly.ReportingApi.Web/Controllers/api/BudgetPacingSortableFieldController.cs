﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to budget pacing sortable fields.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class BudgetPacingSortableFieldController : ApiController
    {
        #region Fields
        private IBudgetPacingSortableFieldRepository budgetPacingSortableFieldRepository;
        #endregion

        #region Constructors
        public BudgetPacingSortableFieldController(
                    IBudgetPacingSortableFieldRepository budgetPacingSortableFieldRepository
                )
        {
            this.budgetPacingSortableFieldRepository = budgetPacingSortableFieldRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all 
        /// budget pacing sortable fields
        /// </summary>
        /// <returns>
        ///     List of Budget Pacing summary field 
        ///     that can be sorted. 
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var results = this.budgetPacingSortableFieldRepository.GetList();

            return Ok(results);
        }
        #endregion
    }
}