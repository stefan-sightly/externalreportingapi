﻿using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System.Collections.Generic;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to targeted geographies records.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class TargetedGeographyController : ApiController
    {
        #region Fields
        private IObjectMapper objectMapper;
        private ITargetedGeographyRepository targetedGeographyRepository;
        #endregion

        #region Constructor
        public TargetedGeographyController(
                   IObjectMapper objectMapper,
                   ITargetedGeographyRepository targetedGeographyRepository
               )
        {
            this.objectMapper = objectMapper;
            this.targetedGeographyRepository = targetedGeographyRepository;
        }
        #endregion

        #region Public Methods
        /// <summary>
        ///     Action used for retrieving list of targeted geographies
        ///     under a campaign, date range and order status.
        /// </summary>
        /// <param name="campaignTargetedGeographyRequest">
        ///     Object that holds the filtering of values that 
        ///     would be used for querying data.
        /// </param>
        /// <returns>
        ///     List of targeted geographies under the specified
        ///     campaign, date range and order status..
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetCampaignTargetedGeographies")]
        public List<CampaignTargetedGeography> GetCampaignTargetedGeographies(CampaignTargetedGeographyRequestViewModel campaignTargetedGeographyRequest)
        {
            var request = objectMapper.Map<
                              CampaignTargetedGeographyRequest,
                              CampaignTargetedGeographyRequestViewModel
                          >(campaignTargetedGeographyRequest);

            return targetedGeographyRepository.GetCampaignTargetedGeographies(request);
        }
        #endregion
    }
}