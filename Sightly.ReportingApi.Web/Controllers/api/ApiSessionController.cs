﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Users;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller for the API Session
    /// </summary>
    [CacheControl]
    public class ApiSessionController : ApiController
    {
        #region Fields
        private IApiSessionRepository apiSessionRepository;
        private IUserRepository userRepository;
        #endregion

        #region Constructors
        public ApiSessionController(
                   IApiSessionRepository apiSessionRepository,
                   IUserRepository userRepository
               )
        {
            this.apiSessionRepository = apiSessionRepository;
            this.userRepository = userRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action that handles login request to an API session
        /// </summary>
        /// <param name="user">
        /// User's credentials that includes email address (for username) and password
        /// </param>
        /// <returns>
        /// This action returns the Session ID if the user is authenticated;
        /// otherwise it returns an error message.
        /// It also returns a flag for the validity of the request.
        /// </returns>
        [HttpPost]
        [ActionName("Login")]
        public IHttpActionResult Login(User user)
        {
            var authenticatedUser = userRepository.GetAuthenticatedUser(user);
            string errorMessage = string.Empty;
            bool isValid = false;
            Guid? sessionId = null;

            if (authenticatedUser == null)
            {
                errorMessage = "The Username or Password provided is incorrect.";
            }
            else
            {
                var session = new ApiSession { UserId = authenticatedUser.UserId };

                apiSessionRepository.Insert(session);
                isValid = true;
                sessionId = session.ApiSessionId;
            }

            return Json(new
            {
                ErrorMessage = errorMessage,
                IsValid = isValid,
                ApiSessionId = sessionId
            });
        }

        /// <summary>
        /// Action that handles logout request to the API session
        /// </summary>
        /// <param name="session">API session information that contains the Session ID</param>
        /// <returns>
        /// This action returns an error message if the request is not valid.
        /// </returns>
        [HttpPost]
        [ActionName("Logout")]
        public IHttpActionResult Logout(ApiSession session)
        {
            string errorMessage = string.Empty;

            if (session == null || session.ApiSessionId == Guid.Empty)
                errorMessage = "The API session information provided is invalid.";
            else
                apiSessionRepository.Logout(session);

            return Json(new
            {
                ErrorMessage = errorMessage
            });
        }
        #endregion
    }
}
