﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;
using Sightly.ReportingApi.Web.Models.Orders;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to advertiser record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class AdvertiserController : ApiController
    {
        #region Fields
        private IAdvertiserRepository advertiserRepository;
        #endregion

        #region Constructor
        public AdvertiserController(IAdvertiserRepository advertiserRepository)
        {
            this.advertiserRepository = advertiserRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all advertisers 
        /// under the current user's account.
        /// </summary>
        /// <param name="sid">Session id of the user.</param>
        /// <returns>
        ///     List of advertisers
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var advertisers = this.advertiserRepository.GetAdvertiserList(sid);

            return Ok(advertisers);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAdvertiserListByAccountRefCode")]
        public IHttpActionResult GetAdvertiserRefCodeListByAccountRefCode(AccountRefCodeRequestViewModel request)
        {
            var advertiserRefCodes = advertiserRepository.GetAdvertiserRefCodeListByAccountRefCode(request.AccountRefCode, request.ApiSessionId);

            return Ok(advertiserRefCodes);

        }

        #endregion
    }
}
