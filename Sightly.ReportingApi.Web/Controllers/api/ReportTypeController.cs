﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller for the report type lookup.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class ReportTypeController : ApiController
    {
        #region Fields
        private IBudgetPacingReportTypeRepository budgetPacingReportTypeRepository;
        private ICampaignPerformanceReportTypeRepository campaignPerformanceReportTypeRepository;
        private IExecutivePerformanceReportTypeRepository executivePerformanceReportTypeRepository;
        #endregion

        #region Constructor
        public ReportTypeController(
                   IBudgetPacingReportTypeRepository budgetPacingReportTypeRepository,
                   ICampaignPerformanceReportTypeRepository campaignPerformanceReportTypeRepository,
                   IExecutivePerformanceReportTypeRepository executivePerformanceReportTypeRepository
               )
        {
            this.budgetPacingReportTypeRepository = budgetPacingReportTypeRepository;
            this.campaignPerformanceReportTypeRepository = campaignPerformanceReportTypeRepository;
            this.executivePerformanceReportTypeRepository = executivePerformanceReportTypeRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        ///     Action used for retrieving the list
        ///     of budget pacing report types.
        /// </summary>
        /// <param name="sid">Session id.</param>
        /// <returns>
        ///     A list of report types that will be needed when 
        ///     creating a report for Budget Pacing Summary.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetBudgetPacingReportTypeList")]
        public IHttpActionResult GetBudgetPacingReportTypeList(Guid sid)
        {
            var reportTypes = budgetPacingReportTypeRepository.GetList();

            return Ok(reportTypes);
        }

        /// <summary>
        /// Action used for retrieving the list
        /// of report types.
        /// </summary>
        /// <returns>
        ///     A list of report types that will be needed when 
        ///     creating a report for Campaign Performance Summary.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetCampaignPerformanceReportTypeList")]
        public IHttpActionResult GetCampaignPerformanceReportTypeList(Guid sid)
        {
            var reportTypes = campaignPerformanceReportTypeRepository.GetList();

            return Ok(reportTypes);
        }

        /// <summary>
        ///     Action used for retrieving the list 
        ///     of executive performance report types.
        /// </summary>
        /// <param name="sid">Session id.</param>
        /// <returns>
        ///     A list of report types that will be needed when 
        ///     creating a report for Executive Performance Summary.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetExecutivePerformanceReportTypeList")]
        public IHttpActionResult GetExecutivePerformanceReportTypeList(Guid sid)
        {
            var reportTypes = executivePerformanceReportTypeRepository.GetList();

            return Ok(reportTypes);
        }
        #endregion
    }
}
