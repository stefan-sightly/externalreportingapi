﻿using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles campaign related requests.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class CampaignController : ApiController
    {
        #region Fields
        private ICampaignRepository campaignRepository;
        private IObjectMapper objectMapper;
        #endregion

        #region Constructor
        public CampaignController(
                   ICampaignRepository campaignRepository,
                   IObjectMapper objectMapper
               )
        {
            this.campaignRepository = campaignRepository;
            this.objectMapper = objectMapper;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Action used for retrieving the total
        /// population under a campaign
        /// </summary>
        /// <param name="campaignTargetedPopulationRequest">
        ///     Filtering data that would be used to retrieve 
        ///     the campaign total population
        /// </param>
        /// <returns>
        ///     Computed value of a population in a campaign 
        ///     record.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetCampaignTotalPopulation")]
        public IHttpActionResult GetCampaignTotalPopulation(CampaignTargetedPopulationRequestViewModel campaignTargetedPopulationRequest)
        {
            var request = objectMapper.Map<
                              CampaignTargetedPopulationRequest,
                              CampaignTargetedPopulationRequestViewModel
                          >(campaignTargetedPopulationRequest);

            var totalPopulation = campaignRepository.GetCampaignTargetedPopulation(request);

            return Ok(totalPopulation.HasValue ? totalPopulation.Value.ToString("N0") : "0");
        }
        #endregion
    }
}