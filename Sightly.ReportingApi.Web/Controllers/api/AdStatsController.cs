﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to ad stats record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class AdStatsController : ApiController
    {
        #region Fields
        private IAdStatsRepository adStatsRepository;
        #endregion

        #region Constructor
        public AdStatsController(IAdStatsRepository adStatsRepository)
        {
            this.adStatsRepository = adStatsRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all ad stats 
        /// under a campaign stats record.
        /// </summary>
        /// <param name="request">
        ///     Filtering data that would be used to retrieve 
        ///     ads stats data.
        /// </param>
        /// <returns>
        ///     List of ads stats.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAdStats")]
        public IHttpActionResult GetAdStats(AdStatsRequestViewModel request)
        {
            var list = this.adStatsRepository.GetAdStats(
                                                  request.OrderId,
                                                  request.SubsetRequest,
                                                  request.ApiSessionId
                                              );

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving ad stats list
        /// of a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of ad stats under the specified campaign id.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByCampaignId")]
        public IHttpActionResult GetListByCampaignId(AdStatsRequestViewModel request)
        {
            var list = this.adStatsRepository.GetAdStatsByCampaignId(
                                                  request.CampaignId,
                                                  request.SubsetRequest,
                                                  request.ApiSessionId
                                              );

            return Ok(list);
        }

        /// <summary>
        /// Action used for retrieving ad stats list
        /// by the specified order IDs.
        /// </summary>
        /// <param name="request">
        ///     Request info that contains ID of orders
        ///     where stats will be fetched from and
        ///     date range for filtering ad stats.
        /// </param>
        /// <returns>
        ///     List of ad stats under the specified orders.
        /// </returns>
        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetListByOrderIds")]
        public IHttpActionResult GetListByOrderIds(StatsByOrderIdsRequestViewModel request)
        {
            var list = adStatsRepository.GetAdStatsByOrderIds(
                                             request.OrderIds,
                                             request.StartDate,
                                             request.EndDate,
                                             request.SubsetRequest
                                         );

            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAdStatsByOrderAndDate")]
        public IHttpActionResult GetAdStatsByOrderAndDate(AdStatsRequestViewModel request)
        {
            var adStats = adStatsRepository.GetAdStatsByOrderAndDates(
                                                request.OrderId, 
                                                request.StartDate,
                                                request.EndDate, 
                                                request.ApiSessionId);
            return Ok(adStats);
        }


        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAdStatByCampaignAndDate")]
        public IHttpActionResult GetAdStatByCampaignAndDate(AdStatsRequestViewModel request)
        {
            var list = this.adStatsRepository.GetAdStatByCampaignAndDates(
                                                  request.CampaignId,
                                                  request.StartDate,
                                                  request.EndDate,
                                                  request.ApiSessionId
                                              );

            return Ok(list);
        }

        #endregion
    }
}
