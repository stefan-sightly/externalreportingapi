﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Web.Filters;
using System;
using System.Web.Http;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    /// <summary>
    /// Web API controller that handles all request 
    /// related to executive performance date range record.
    /// </summary>
    [ApiSessionAuthorize]
    [CacheControl]
    public class ExecutivePerformanceDateRangeController : ApiController
    {
        #region Fields
        private IExecutivePerformanceDateRangeRepository executivePerformanceDateRangeRepository;
        #endregion

        #region Constructor
        public ExecutivePerformanceDateRangeController(IExecutivePerformanceDateRangeRepository executivePerformanceDateRangeRepository)
        {
            this.executivePerformanceDateRangeRepository = executivePerformanceDateRangeRepository;
        }
        #endregion

        #region Actions
        /// <summary>
        /// Action used for retrieving all 
        /// executive performance date range
        /// </summary>
        /// <returns>
        ///     List of Executive Performance date range 
        ///     that will be used for filtering.
        /// </returns>
        [ApiSessionExtender]
        [ActionName("GetList")]
        public IHttpActionResult GetList(Guid sid)
        {
            var list = this.executivePerformanceDateRangeRepository.GetList();

            return Ok(list);
        }
        #endregion
    }
}