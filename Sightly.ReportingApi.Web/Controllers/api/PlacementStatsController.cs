﻿using System.Web.Http;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Web.Filters;
using Sightly.ReportingApi.Web.Models.Orders;

namespace Sightly.ReportingApi.Web.Controllers.api
{
    [ApiSessionAuthorize]
    [CacheControl]
    public class PlacementStatsController : ApiController
    {
        private IPlacementStatsRepository placementStatsRepository;

        public PlacementStatsController(IPlacementStatsRepository placementStatsRepository)
        {
            this.placementStatsRepository = placementStatsRepository;
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetPlacementsByAdvertiserRefCode")]
        public IHttpActionResult GetListOfPlacementsByAdvertiserRefCode(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetKnownPlacementsByAdvertiserRefCode(request.ApiSessionId, request.AdvertiserRefCode);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetPlacementsBySession")]
        public IHttpActionResult GetListOfPlacementsBySession(PlacementRequestViewModel request)
        {
            var list =  placementStatsRepository.GetKnownPlacementsBySession(request.ApiSessionId);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetStatsByPlacementAndDates")]
        public IHttpActionResult GetAggregatedPlacementStats(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetPlacementStatsAggregatedByDates(request.Placement, request.StartDate, request.EndDate);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetDailyStatsByPlacementAndDates")]
        public IHttpActionResult GetDailyAggregatedPlacementStats(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetPlacementStatsDailyAggregatedByDates(request.Placement, request.StartDate, request.EndDate);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetDailyPlusExtendedStatsByPlacementAndDate_WithSession")]
        public IHttpActionResult GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(
            PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(
                request.Placement, 
                request.StartDate, 
                request.EndDate, 
                request.ApiSessionId);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAllStatsByRefCodeAndDates")]
        public IHttpActionResult GetAllStatsByRefCodeAndDates(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetAllStatsByRefCodeAndDates(request.AdvertiserRefCode,
                request.StartDate, request.EndDate);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAllDailyStatsByRefCodeAndDates")]
        public IHttpActionResult GetAllDailyStatsByRefCodeAndDates(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetAllDailyStatsByRefCodeAndDates(request.AdvertiserRefCode,
                request.StartDate, request.EndDate);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetDailyPlacementStatsByAccountRefCodeAndDates")]
        public IHttpActionResult GetDailyPlacementStatsByAccountRefCodeAndDates(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetDailyPlacementStatsByAccountRefCodeAndDates(
                request.AccountRefCode,
                request.StartDate,
                request.EndDate,
                request.ApiSessionId
            );
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetExtendedStatsAggregatedByPlacementAndDate")]
        public IHttpActionResult GetExtendedStatsAggregatedByPlacementAndDate(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetExtendedStatsAggregatedByPlacementAndDate(request.Placement, request.StartDate, request.EndDate, request.ApiSessionId);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetExtendedStatsDailyByPlacementAndDate")]
        public IHttpActionResult GetExtendedStatsDailyByPlacementAndDate(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetExtendedStatsDailyByPlacementAndDate(request.Placement, request.StartDate, request.EndDate, request.ApiSessionId);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetAggregatedDeviceStatsByPlacementAndDates")]
        public IHttpActionResult GetAggregatedDeviceStatsByPlacementAndDates(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetAggregatedDeviceStatsByPlacementAndDates(request.Placement, request.StartDate, request.EndDate, request.ApiSessionId);
            return Ok(list);
        }

        [ApiSessionExtender]
        [HttpPost]
        [ActionName("GetDailyDeviceStatsByPlacementAndDates")]
        public IHttpActionResult GetDailyDeviceStatsByPlacementAndDates(PlacementRequestViewModel request)
        {
            var list = placementStatsRepository.GetDailyDeviceStatsByPlacementAndDates(request.Placement, request.StartDate, request.EndDate, request.ApiSessionId);
            return Ok(list);
        }

    }
}
