﻿using System.Web.Mvc;

namespace Sightly.ReportingApi.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}