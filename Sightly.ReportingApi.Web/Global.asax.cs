﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Web.DependencyInversion;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Sightly.ReportingApi.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-AspNetMvc-Version");
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = UnityContainerBuilder.Build();
            var objectMappingConfigurator = container.Resolve<IObjectMappingConfigurator>();
            objectMappingConfigurator.Initialize();

           // var configurationStore = container.Resolve<IConfigurationStore>();
            //NServiceBusActivator.Start(configurationStore);
        }
    }
}
