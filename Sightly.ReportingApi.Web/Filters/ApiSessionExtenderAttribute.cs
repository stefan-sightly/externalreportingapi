﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Web.DependencyInversion;
using Sightly.ReportingApi.Web.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Sightly.ReportingApi.Web.Filters
{
    /// <summary>
    /// Action filter class that can be utilized to extend an API session
    /// </summary>
    public class ApiSessionExtenderAttribute : ActionFilterAttribute
    {
        #region Fields
        private IApiSessionRepository apiSessionRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the API session extender attribute
        /// </summary>
        public ApiSessionExtenderAttribute()
        {
            var container = UnityContainerBuilder.Build();
            apiSessionRepository = container.Resolve<IApiSessionRepository>();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Method used to obtain the API session ID passed with the web API request
        /// </summary>
        /// <param name="actionContext">Request context to be handled</param>
        /// <returns>API session ID</returns>
        private Guid GetApiSessionId(HttpActionContext actionContext)
        {
            if (actionContext.Request.Method == HttpMethod.Get)
                return (Guid)actionContext.ActionArguments["sid"];

            var model = actionContext.ActionArguments
                                     .Values
                                     .First(m => m is BaseRequestViewModel);

            return ((BaseRequestViewModel)model).ApiSessionId;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method overridden that would extend an API session
        /// </summary>
        /// <param name="actionExecutedContext">Request context to be handled</param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            apiSessionRepository.Extend(GetApiSessionId(actionExecutedContext.ActionContext));
            base.OnActionExecuted(actionExecutedContext);
        }
        #endregion
    }
}