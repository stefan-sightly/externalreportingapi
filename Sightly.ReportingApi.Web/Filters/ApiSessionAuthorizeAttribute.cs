﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Web.DependencyInversion;
using Sightly.ReportingApi.Web.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Sightly.ReportingApi.Web.Filters
{
    /// <summary>
    /// Action filter class that can be utilized to prevent
    /// unauthorized users from accessing web API action
    /// </summary>
    public class ApiSessionAuthorizeAttribute : ActionFilterAttribute
    {
        #region Fields
        private IApiSessionRepository apiSessionRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the API session authorize attribute
        /// </summary>
        public ApiSessionAuthorizeAttribute()
        {
            var container = UnityContainerBuilder.Build();
            apiSessionRepository = container.Resolve<IApiSessionRepository>();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Method used to obtain the API session ID passed with the web API request
        /// </summary>
        /// <param name="actionContext">Request context to be handled</param>
        /// <returns>API session ID</returns>
        private Guid? GetApiSessionId(HttpActionContext actionContext)
        {
            Guid? apiSessionId = null;

            if (actionContext.Request.Method == HttpMethod.Get)
            {
                const string apiSessionIdKey = "sid";

                if (actionContext.ActionArguments.ContainsKey(apiSessionIdKey))
                    apiSessionId = actionContext.ActionArguments[apiSessionIdKey] as Guid?;
            }
            else if (actionContext.Request.Method == HttpMethod.Post)
            {
                var model = actionContext.ActionArguments
                                         .Values
                                         .FirstOrDefault(m => m is BaseRequestViewModel);

                if (model != null)
                    apiSessionId = ((BaseRequestViewModel)model).ApiSessionId;
            }

            return apiSessionId;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method overridden that would send unauthorized (401) response
        /// to request sent from user sessions of ineligible users
        /// </summary>
        /// <param name="actionContext">Request context to be handled</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            Guid? sessionId = GetApiSessionId(actionContext);

            if (!sessionId.HasValue ||
                sessionId.Value == Guid.Empty ||
                !apiSessionRepository.CheckIfValid(sessionId.Value))
                throw new HttpResponseException(HttpStatusCode.Unauthorized);

            base.OnActionExecuting(actionContext);
        }
        #endregion
    }
}