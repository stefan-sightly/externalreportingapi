﻿using System;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace Sightly.ReportingApi.Web.Filters
{
    /// <summary>
    /// Action class that would disable 
    /// the caching for all web API controllers
    /// </summary>
    public class CacheControlAttribute : ActionFilterAttribute
    {
        #region Fields
        /// <summary>
        /// Property that holds the max age of the cache
        /// </summary>
        public int MaxAge { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the cache control attribute
        /// </summary>
        public CacheControlAttribute()
        {
            MaxAge = 1;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that would override cache control
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            if (context.Response != null)
                context.Response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(MaxAge)
                };

            base.OnActionExecuted(context);
        }
        #endregion
    }
}