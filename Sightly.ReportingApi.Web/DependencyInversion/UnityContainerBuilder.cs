﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Web.DependencyInversion.Registrars;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Web.DependencyInversion
{
    /// <summary>
    /// Class that builds unity container out of registrar classes provided
    /// </summary>
    public class UnityContainerBuilder
    {
        #region Private Fields
        private static IUnityContainer container;
        #endregion

        #region Methods
        /// <summary>
        /// Method that builds unity container out of dependency registrars provided
        /// </summary>
        /// <returns>Unity container with dependency mappings</returns>
        public static IUnityContainer Build()
        {
            if (container != null)
                return container;

            container = new UnityContainer();

            var registrars = new List<DependencyRegistrar>()
                             {
                                 new DataStoreRegistrar(),
                                 new InfrastructureRegistrar(),
                                 new RepositoryRegistrar(),
                                 new UtilityRegistrar(),
                                 new ServiceAgentRegistrar()
                             };

            foreach (var registrar in registrars)
            {
                registrar.RegisterMappings(container);
            }

            return container;
        }
        #endregion
    }
}