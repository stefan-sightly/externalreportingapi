﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus.CampaignStatsReportGenerator;
using Sightly.ReportingApi.ServiceAgents.ServiceBus.CampaignStatsReportGenerator;

namespace Sightly.ReportingApi.Web.DependencyInversion.Registrars
{
    /// <summary>
    /// Class in-charge of registering dependency mappings 
    /// between service agent interfaces and their 
    /// implementations.
    /// </summary>
    public class ServiceAgentRegistrar : DependencyRegistrar
    {
        #region Helper Methods
        /// <summary>
        /// Method that registers dependency mappings between bus agent abstract and concrete data stores
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        public void RegisterBusAgentMappings(IUnityContainer container)
        {
            container.RegisterType<IExecutivePerformanceReportSenderAgent, ExecutivePerformanceReportSenderAgent>();
            container.RegisterType<IBudgetPacingReportSenderAgent, BudgetPacingReportSenderAgent>();
            container.RegisterType<ICampaignPerformanceReportSenderAgent, CampaignPerformanceReportSenderAgent>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that registers dependency mappings 
        /// between service agent interfaces and their 
        /// implementations
        /// </summary>
        /// <param name="container">
        ///     Unity container that will be used 
        ///     to register dependency mappings
        /// </param>
        public override void RegisterMappings(IUnityContainer container)
        {
            //KILL THIS BUS SHIT!!!!
            //            RegisterBusAgentMappings(container);
        }
        #endregion
    }
}