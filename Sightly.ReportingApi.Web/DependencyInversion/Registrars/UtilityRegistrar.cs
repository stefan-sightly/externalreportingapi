﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Business.Interfaces.Utilities.CryptographyServices;
using Sightly.ReportingApi.Business.Interfaces.Utilities.Generators;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Utilities.CryptographyServices;
using Sightly.ReportingApi.Utilities.Generators;
using Sightly.ReportingApi.Utilities.SessionHelpers;

namespace Sightly.ReportingApi.Web.DependencyInversion.Registrars
{
    #region Public Methods
    /// <summary>
    /// Class in-charge of registering dependency mappings between utility interfaces and their implementations
    /// </summary>
    public class UtilityRegistrar : DependencyRegistrar
    {
        /// <summary>
        /// Method that registers dependency mappings between utility interfaces and their implementations
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        public override void RegisterMappings(IUnityContainer container)
        {
            container.RegisterType<IApiSessionHelper, ApiSessionHelper>();
            container.RegisterType<IConfigurationStore, WebConfigurationStore>();
            container.RegisterType<IConnectionStringStore, WebConnectionStringStore>();
            container.RegisterType<IGuidGenerator, GuidGenerator>();
            container.RegisterType<IHashService, HashService>();
        }
    }
    #endregion
}