﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Web.Mappings;

namespace Sightly.ReportingApi.Web.DependencyInversion.Registrars
{
    #region Public Methods
    /// <summary>
    ///  Class in-charge of registering dependency mappings
    ///  between infrastructure interfaces and their
    ///  implementations.
    /// </summary>
    public class InfrastructureRegistrar : DependencyRegistrar
    {
        /// <summary>
        /// Method that registers dependency mappings
        /// between infrastructure interfaces and
        /// their implementations.
        /// </summary>
        /// <param name="container">
        ///     Unity container that will be used to register 
        ///     dependency mappings.
        /// </param>
        public override void RegisterMappings(IUnityContainer container)
        {
            container.RegisterType<IObjectMapper, AutoMapperObjectMapper>();
            container.RegisterType<IObjectMappingConfigurator, AutoMapperConfigurator>();
        }
    } 
    #endregion
}