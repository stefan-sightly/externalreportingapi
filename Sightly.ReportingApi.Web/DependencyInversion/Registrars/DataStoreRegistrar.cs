﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Users;
using Sightly.ReportingApi.DataStore.Dapper.Accounts;
using Sightly.ReportingApi.DataStore.Dapper.Lookups;
using Sightly.ReportingApi.DataStore.Dapper.Orders;
using Sightly.ReportingApi.DataStore.Dapper.Users;

namespace Sightly.ReportingApi.Web.DependencyInversion.Registrars
{
    /// <summary>
    /// Class in-charge of registering dependency mappings between data store interfaces and their implementations
    /// </summary>
    public class DataStoreRegistrar : DependencyRegistrar
    {
        #region Helper Methods
        /// <summary>
        /// Method that registers dependency mappings between account-related abstract and concrete data stores
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterAccountMappings(IUnityContainer container)
        {
            container.RegisterType<IApiSessionDataStore, ApiSessionDataStore>();
            container.RegisterType<IAccountDataStore, AccountDataStore>();
        }

        /// <summary>
        /// Method that registers dependency mappings between lookups-related abstract and concrete repositories
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterLookupsMappings(IUnityContainer container)
        {
            container.RegisterType<IBudgetPacingDateRangeDataStore, BudgetPacingDateRangeDataStore>();
            container.RegisterType<IBudgetPacingReportTypeDataStore, BudgetPacingReportTypeDataStore>();
            container.RegisterType<IBudgetPacingSortableFieldDataStore, BudgetPacingSortableFieldDataStore>();
            container.RegisterType<ICampaignPerformanceReportTypeDataStore, CampaignPerformanceReportTypeDataStore>();
            container.RegisterType<IExecutivePerformanceDateRangeDataStore, ExecutivePerformanceDateRangeDataStore>();
            container.RegisterType<IExecutivePerformanceReportTypeDataStore, ExecutivePerformanceReportTypeDataStore>();
            container.RegisterType<IExecutivePerformanceSortableFieldDataStore, ExecutivePerformanceSortableFieldDataStore>();
            container.RegisterType<IOrderStatusDataStore, OrderStatusDataStore>();
        }

        /// <summary>
        /// Method that registers dependency mappings between order-related abstract and concrete data stores
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterOrderMappings(IUnityContainer container)
        {
            container.RegisterType<IAdStatsDataStore, AdStatsDataStore>();
            container.RegisterType<IAdvertiserDataStore, AdvertiserDataStore>();
            container.RegisterType<IAgeGroupStatsDataStore, AgeGroupStatsDataStore>();
            container.RegisterType<ICampaignDataStore, CampaignDataStore>();
            container.RegisterType<ICampaignStatsDataStore, CampaignStatsDataStore>();
            container.RegisterType<IDeviceStatsDataStore, DeviceStatsDataStore>();
            container.RegisterType<IGenderStatsDataStore, GenderStatsDataStore>();
            container.RegisterType<IOrderDataStore, OrderDataStore>();
            container.RegisterType<IOrderStatsDataStore, OrderStatsDataStore>();
            container.RegisterType<IPlacementStatsDataStore, PlacementStatsDataStore>();
            container.RegisterType<ITargetedGeographyDataStore, TargetedGeographyDataStore>();
        }

        /// <summary>
        /// Method that registers dependency mappings between user-related abstract and concrete data stores
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterUserMappings(IUnityContainer container)
        {
            container.RegisterType<IUserDataStore, UserDataStore>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that registers dependency mappings between data store interfaces and their implementations
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        public override void RegisterMappings(IUnityContainer container)
        {
            RegisterAccountMappings(container);
            RegisterLookupsMappings(container);
            RegisterOrderMappings(container);
            RegisterUserMappings(container);
        }
        #endregion
    }
}