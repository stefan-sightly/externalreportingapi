﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Users;
using Sightly.ReportingApi.Business.Repositories.Accounts;
using Sightly.ReportingApi.Business.Repositories.Lookups;
using Sightly.ReportingApi.Business.Repositories.Orders;
using Sightly.ReportingApi.Business.Repositories.Users;

namespace Sightly.ReportingApi.Web.DependencyInversion.Registrars
{
    /// <summary>
    /// Class in-charge of registering dependency mappings between repository interfaces and their implementations
    /// </summary>
    public class RepositoryRegistrar : DependencyRegistrar
    {
        #region Helper Methods
        /// <summary>
        /// Method that registers dependency mappings between account-related abstract and concrete repositories
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterAccountMappings(IUnityContainer container)
        {
            container.RegisterType<IApiSessionRepository, ApiSessionRepository>();
            container.RegisterType<IAccountRepository, AccountRepository>();
        }

        /// <summary>
        /// Method that registers dependency mappings between order-related abstract and concrete repositories
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterLookupsMappings(IUnityContainer container)
        {
            container.RegisterType<IBudgetPacingDateRangeRepository, BudgetPacingDateRangeRepository>();
            container.RegisterType<IBudgetPacingReportTypeRepository, BudgetPacingReportTypeRepository>();
            container.RegisterType<IBudgetPacingSortableFieldRepository, BudgetPacingSortableFieldRepository>();
            container.RegisterType<ICampaignPerformanceReportTypeRepository, CampaignPerformanceReportTypeRepository>();
            container.RegisterType<IExecutivePerformanceDateRangeRepository, ExecutivePerformanceDateRangeRepository>();
            container.RegisterType<IExecutivePerformanceReportTypeRepository, ExecutivePerformanceReportTypeRepository>();
            container.RegisterType<IExecutivePerformanceSortableFieldRepository, ExecutivePerformanceSortableFieldRepository>();
            container.RegisterType<IOrderStatusRepository, OrderStatusRepository>();
        }

        /// <summary>
        /// Method that registers dependency mappings between order-related abstract and concrete repositories
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterOrderMappings(IUnityContainer container)
        {
            container.RegisterType<IAdStatsRepository, AdStatsRepository>();
            container.RegisterType<IAdvertiserRepository, AdvertiserRepository>();
            container.RegisterType<IAgeGroupStatsRepository, AgeGroupStatsRepository>();
            container.RegisterType<ICampaignRepository, CampaignRepository>();
            container.RegisterType<ICampaignStatsRepository, CampaignStatsRepository>();
            container.RegisterType<IDeviceStatsRepository, DeviceStatsRepository>();
            container.RegisterType<IGenderStatsRepository, GenderStatsRepository>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IOrderStatsRepository, OrderStatsRepository>();
            container.RegisterType<IPlacementStatsRepository, PlacementStatsRepository>();
            container.RegisterType<ITargetedGeographyRepository, TargetedGeographyRepository>();
        }

        /// <summary>
        /// Method that registers dependency mappings between user-related abstract and concrete repositories
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        private void RegisterUserMappings(IUnityContainer container)
        {
            container.RegisterType<IUserRepository, UserRepository>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that registers dependency mappings between repository interfaces and their implementations
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        public override void RegisterMappings(IUnityContainer container)
        {
            RegisterAccountMappings(container);
            RegisterLookupsMappings(container);
            RegisterOrderMappings(container);
            RegisterUserMappings(container);
        }
        #endregion
    }
}