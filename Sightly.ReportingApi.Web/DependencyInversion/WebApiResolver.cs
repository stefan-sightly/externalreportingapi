﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Web.Http.Dependencies;

namespace Sightly.ReportingApi.Web.DependencyInversion
{
    /// <summary>
    /// Class used for resolving Web API dependency mappings using Unity Framework™
    /// </summary>
    public class WebApiResolver : IDependencyResolver
    {
        /// <summary>
        /// Unity container instance used for resolving dependency mappings
        /// </summary>
        protected IUnityContainer container;

        #region Public Methods
        /// <summary>
        /// Method used for starting a resolution scope
        /// </summary>
        /// <returns>The dependency scope.</returns>
        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new WebApiResolver(child);
        }

        /// <summary>
        /// Method used for disposing the stored container.
        /// </summary>
        public void Dispose()
        {
            container.Dispose();
        }

        /// <summary>
        /// Method used for resolving singly registered services that support arbitrary object creation.
        /// </summary>
        /// <param name="serviceType">The type of the requested service or object</param>
        /// <returns> The requested service or object.</returns>
        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        /// <summary>
        /// Resolves multiply registered services.
        /// </summary>
        /// <param name="serviceType">The type of the requested services.</param>
        /// <returns>The requested services</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return new List<object>();
            }
        }

        /// <summary>
        /// Constructor method of WebApiResolver class
        /// </summary>
        /// <param name="container">Unity container instance that would be used for resolving dependency mappings.</param>
        public WebApiResolver(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }
        #endregion
    }
}