﻿using Microsoft.Practices.Unity;

namespace Sightly.ReportingApi.Web.DependencyInversion
{
    /// <summary>
    /// Base class of all dependency inversion mapping registrars
    /// </summary>
    public abstract class DependencyRegistrar
    {
        /// <summary>
        /// Abstract method used for registering dependency mappings to a unity container
        /// </summary>
        /// <param name="container">Unit container that will be used to register dependency mappings</param>
        public abstract void RegisterMappings(IUnityContainer container);
    }
}