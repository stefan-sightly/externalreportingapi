﻿using Microsoft.Practices.Unity;
using Sightly.ReportingApi.Web.DependencyInversion;
using System.Web.Mvc;

namespace Sightly.ReportingApi.Web.App_Start
{
    /// <summary>
    /// Class used for configuration dependency inversion-related configurations
    /// </summary>
    public class DependencyInversionConfig
    {
        #region Fields
        /// <summary>
        /// Unity container that will be used on resolving dependency injection mappings
        /// </summary>
        static IUnityContainer container;
        #endregion

        #region Public
        /// <summary>
        /// Method used for registering unity container on MVC dependency resolver class
        /// </summary>
        public static void Initialise()
        {
            container = UnityContainerBuilder.Build();

            DependencyResolver.SetResolver(new UnityResolver(container));
        }

        /// <summary>
        /// Method used for resolving dependencies base on provided abstraction.
        /// </summary>
        /// <typeparam name="T">Abstract type that will be used on searching for mapped abstractions inside the unity container</typeparam>
        /// <returns>Concrete implementation instance of the abstract type provided.</returns>
        public static T Resolve<T>()
        {
            T ret = default(T);

            if (container.IsRegistered(typeof(T)))
            {
                ret = container.Resolve<T>();
            }

            return ret;
        }
        #endregion
    }
}