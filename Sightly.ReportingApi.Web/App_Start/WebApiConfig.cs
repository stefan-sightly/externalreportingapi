﻿using Newtonsoft.Json;
using Sightly.ReportingApi.Web.DependencyInversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Sightly.ReportingApi.Web
{
    /// <summary>
    /// Class used for registering all web API related configurations
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Method used for registering all web API related configurations
        /// </summary>
        /// <param name="config">Represents the configuration of the HTTP server</param>
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.DependencyResolver = new WebApiResolver(UnityContainerBuilder.Build());
        }
    }
}
