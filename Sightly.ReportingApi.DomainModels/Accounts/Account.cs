﻿using System;

namespace Sightly.ReportingApi.DomainModels.Accounts
{
    /// <summary>
    /// Class that would represent account records.
    /// </summary>
    public class Account
    {
        #region Properties
        /// <summary>
        /// Property that holds the account id.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the account code.
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Property that holds the account name.
        /// </summary>
        public string Name { get; set; }
        #endregion 
    }
}