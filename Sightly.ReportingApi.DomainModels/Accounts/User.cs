﻿using System;

namespace Sightly.ReportingApi.DomainModels.Accounts
{
    /// <summary>
    /// Class the would represent the user records.
    /// </summary>
    public class User
    {
        #region Properties
        /// <summary>
        /// Property that holds the user's account id
        /// </summary>
        public Guid AccountId { get; set; }

        /// Property that holds the user's account user code
        /// </summary>
        public string AccountUserCode { get; set; }

        /// <summary>
        /// Property that holds the user's email address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Property that holds the user's first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Property that holds the user's last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Property that holds the user's password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Property that holds the user's id
        /// </summary>
        public Guid UserId { get; set; }
        #endregion

        #region Read only Properties
        /// <summary>
        /// Property that holds the user's full name
        /// </summary>
        public string Name
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        #endregion
    }
}