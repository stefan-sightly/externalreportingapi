﻿using System;

namespace Sightly.ReportingApi.DomainModels.Accounts
{
    /// <summary>
    /// Class that represents an API session of the current logged in user
    /// </summary>
    public class ApiSession
    {
        /// <summary>
        /// Property that holds a unique token that identifies the API session
        /// </summary>
        public Guid ApiSessionId { get; set; }

        /// <summary>
        /// Property that holds the expiration time of the API session
        /// </summary>
        public DateTime? ExpirationTime { get; set; }

        /// <summary>
        /// Property that holds the last time the user created an API request
        /// </summary>
        public DateTime? LastRequestTime { get; set; }

        /// <summary>
        /// Property that holds time when the user logged in
        /// </summary>
        public DateTime? LoginTime { get; set; }

        /// <summary>
        /// Property that holds the time when the user logged out
        /// </summary>
        public DateTime? LogoutTime { get; set; }

        /// <summary>
        /// Property that holds the ID of the current logged in user
        /// </summary>
        public Guid UserId { get; set; }
    }
}
