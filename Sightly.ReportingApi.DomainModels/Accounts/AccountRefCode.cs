﻿namespace Sightly.ReportingApi.DomainModels.Accounts
{
    public class AccountRefCode
    {
        public string ParentAccountRefCode { get; set; }
        public string ParentAccountName { get; set; }
        public string ChildAccountRefCode { get; set; }
        public string ChildAccountName { get; set; }
    }
}