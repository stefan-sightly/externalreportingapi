﻿using System;

namespace Sightly.ReportingApi.DomainModels.Users
{
    /// <summary>
    /// Class that represents the abstraction of a user claim.
    /// </summary>
    public class UserClaim
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the role in which the user claim is assigned to.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Property that holds the type of the user claim.
        /// </summary>
        public string ClaimType { get; set; }

        /// <summary>
        /// Property that hold the value of the user claim.
        /// </summary>
        public string ClaimValue { get; set; }

        /// <summary>
        /// Property that holds the type of the user claim value.
        /// </summary>
        public string ClaimValueType { get; set; }

        /// <summary>
        /// Property that holds the target of the user claim.
        /// </summary>
        public string ClaimTarget { get; set; }

        /// <summary>
        /// Property that holds the type of the user claim target.
        /// </summary>
        public string ClaimTargetType { get; set; }
        #endregion
    }
}