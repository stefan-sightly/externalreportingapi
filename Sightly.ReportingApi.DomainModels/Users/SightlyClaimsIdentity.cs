﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Sightly.ReportingApi.DomainModels.Users
{
    /// <summary>
    /// Class that represents claims identity for Sightly
    /// </summary>
    public class SightlyClaimsIdentity : ClaimsIdentity
    {
        /// <summary>
        /// Constructor of Sightly claims identity that accepts claims and authentication type
        /// </summary>
        /// <param name="claims">Claims</param>
        /// <param name="authenticationType">Authentication</param>
        public SightlyClaimsIdentity(IEnumerable<Claim> claims, string authenticationType)
            : base(claims, authenticationType)
        {
        }

        /// <summary>
        /// Property that holds a request verification token
        /// </summary>
        public string RequestVerificationToken { get; set; }
    }
}