﻿using System;

namespace Sightly.ReportingApi.DomainModels.Users
{
    /// <summary>
    /// Class that represents the abstraction of a user profile.
    /// </summary>
    public class UserProfile
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the user that owns the profile
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Property that holds the first name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Property that holds the last name of the user
        /// </summary>
        public string LastName { get; set; }
        #endregion
    }
}