﻿
namespace Sightly.ReportingApi.DomainModels.Users
{
    /// <summary>
    /// Class that represents password 
    /// reset request records.
    /// </summary>
    public class PasswordResetRequest
    {
        #region Properties
        /// <summary>
        /// Property that represents the user's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Property that represents the user's ip address
        /// </summary>
        public string IpAddress { get; set; }
        #endregion
    }
}