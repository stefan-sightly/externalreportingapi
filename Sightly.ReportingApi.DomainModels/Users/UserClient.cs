﻿using System;

namespace Sightly.ReportingApi.DomainModels.Users
{
    /// <summary>
    /// Class that represents the abstraction of a user client.
    /// </summary>
    public class UserClient
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the user that the client is assigned to
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Property that holds the id of the client that was assigned to the user.
        /// </summary>
        public Guid ClientId { get; set; }
        #endregion
    }
}