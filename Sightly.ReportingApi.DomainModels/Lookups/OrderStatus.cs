﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents order status records.
    /// </summary>
    public class OrderStatus
    {
        #region Properties
        /// <summary>
        /// Property that holds the order status id.
        /// </summary>
        public Guid OrderStatusId { get; set; }

        /// <summary>
        /// Property that holds the order status name.
        /// </summary>
        public string OrderStatusName { get; set; }
        #endregion
    }
}