﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents budget pacing date range records.
    /// </summary>
    public class BudgetPacingDateRange
    {
        #region Properties
        /// <summary>
        /// Property that holds the budget pacing date range id.
        /// </summary>
        public Guid BudgetPacingDateRangeId { get; set; }

        /// <summary>
        /// Property that holds the budget pacing date range name.
        /// </summary>
        public string Name { get; set; }
        #endregion
    }
}
