﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents executive performance date range records.
    /// </summary>
    public class ExecutivePerformanceDateRange
    {
        #region Properties
        /// <summary>
        /// Property that holds the executive performance date range id.
        /// </summary>
        public Guid ExecutivePerformanceDateRangeId
        {
            get
            {
                return PerformanceSummaryReportTypeId;
            }
        }

        /// <summary>
        /// Property that holds the executive performance date range name.
        /// </summary>
        public string Name
        {
            get
            {
                return PerformanceSummaryReportTypeName;
            }
        }
        #endregion

        #region TargetView2 Integration Properties
        /// <summary>
        /// Property that holds the ID of a 
        /// performance summary report type.
        /// </summary>
        Guid PerformanceSummaryReportTypeId { get; set; }

        /// <summary>
        /// Property that holds the name of a 
        /// performance summary report type.
        /// </summary>
        string PerformanceSummaryReportTypeName { get; set; }
        #endregion
    }
}