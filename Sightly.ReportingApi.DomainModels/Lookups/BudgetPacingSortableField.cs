﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents budget pacing
    /// sortable fields.
    /// </summary>
    public class BudgetPacingSortableField
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the
        /// budget pacing sortable field record.
        /// </summary>
        public Guid? BudgetPacingSortableFieldId
        {
            get
            {
                return BudgetPacingFieldId;
            }
        }

        /// <summary>
        /// Property that holds the name of the
        /// budget pacing sortable field.
        /// </summary>
        public string Name
        {
            get
            {
                return BudgetPacingFieldName;
            }
        }
        #endregion

        #region TargetView2 Integration Properties
        /// <summary>
        /// Property that holds the ID of a budget pacing field.
        /// </summary>
        Guid? BudgetPacingFieldId { get; set; }

        /// <summary>
        /// Property that holds the name of a budget pacing field.
        /// </summary>
        string BudgetPacingFieldName { get; set; }
        #endregion
    }
}