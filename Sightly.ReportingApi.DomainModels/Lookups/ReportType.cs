﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents report type records.
    /// </summary>
    public class ReportType
    {
        #region Properties
        /// <summary>
        /// Property that holds the report type id.
        /// </summary>
        public Guid ReportTypeId { get; set; }

        /// <summary>
        /// Property that holds the report type name.
        /// </summary>
        public string Name { get; set; }
        #endregion
    }
}