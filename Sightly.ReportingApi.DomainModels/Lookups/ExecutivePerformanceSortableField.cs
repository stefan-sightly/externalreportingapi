﻿using System;

namespace Sightly.ReportingApi.DomainModels.Lookups
{
    /// <summary>
    /// Class that represents executive 
    /// performance sortable fields.
    /// </summary>
    public class ExecutivePerformanceSortableField
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the
        /// executive performance sortable 
        /// field record.
        /// </summary>
        public Guid ExecutivePerformanceSortableFieldId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// executive performance sortable field.
        /// </summary>
        public string Name { get; set; }
        #endregion
    }
}