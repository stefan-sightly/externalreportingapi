﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents request 
    /// for generating the budget pacing report.
    /// </summary>
    public class BudgetPacingReportRequest
    {
        /// <summary>
        /// Property that holds the id of the account associated to the campaign 
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the campaign's advertiser id.
        /// </summary>
        public Guid? AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the end date that would 
        /// be used for filtering the data to be 
        /// generated in the report.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the order status ids 
        /// to include in generating the report
        /// </summary>
        public List<Guid> OrderStatusIds { get; set; }

        /// <summary>
        /// Property that holds the email address of 
        /// the user to use for sending the report
        /// </summary>
        public string RecipientEmailAddress { get; set; }

        /// <summary>
        /// Property that holds the first name of the user
        /// </summary>
        public string RecipientFirstName { get; set; }

        /// <summary>
        /// Property that holds the type of the report to generate
        /// </summary>
        public string ReportFormat { get; set; }

        /// <summary>
        /// Property that holds the search term to use to filter our the list
        /// of campaigns to include in the report
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Property that holds the start date that would 
        /// be used for filtering the data to be generated in the report.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the id of the sub-account to use
        /// for loading the campaign list for a specific sub account
        /// </summary>
        public Guid? SubAccountId { get; set; }
    }
}
