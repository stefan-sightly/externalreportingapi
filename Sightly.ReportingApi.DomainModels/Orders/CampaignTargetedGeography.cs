﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents request for targeted geography data.
    /// </summary>
    public class CampaignTargetedGeography
    {
        #region Properties
        /// <summary>
        /// Property that holds the id of the location.
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Property that holds the name of the location.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Property that holds the id of the geography.
        /// </summary>
        public Guid GeographyId { get; set; }

        /// <summary>
        /// Property that holds the name of the geography.
        /// </summary>
        public string GeographyName { get; set; }
        #endregion
    }
}