﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents order records.
    /// </summary>
    public class Order
    {
        #region Properties
        /// <summary>
        /// Property that holds the ID 
        /// of the order record.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the reference
        /// code of the order.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order's advertiser category.
        /// </summary>
        public Guid AdvertiserCategoryId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// order's advertiser category.
        /// </summary>
        public string AdvertiserCategoryName { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order's advertiser sub-category.
        /// </summary>
        public Guid? AdvertiserSubCategoryId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// order's advertiser sub-category
        /// name.
        /// </summary>
        public string AdvertiserSubCategoryName { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order's current status.
        /// </summary>
        public Guid CurrentOrderStatusId { get; set; }

        /// <summary>
        /// Property that holds the order's 
        /// current status.
        /// </summary>
        public string OrderStatusName { get; set; }

        /// <summary>
        /// Property that holds the order targeting type's id.
        /// </summary>
        public Guid TargetingTypeId { get; set; }

        /// <summary>
        /// Property that holds the order targeting type's name.
        /// </summary>
        public string TargetingTypeName { get; set; }
        #endregion
    }
}