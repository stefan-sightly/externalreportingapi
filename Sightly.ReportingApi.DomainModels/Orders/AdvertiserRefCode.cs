﻿namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class AdvertiserRefCode
    {
        public string ParentAccountRefCode { get; set; }
        public string ParentAccountName { get; set; }
        public string ChildAdvertiserRefCode { get; set; }
        public string ChildAdvertiserName { get; set; }
    }
}