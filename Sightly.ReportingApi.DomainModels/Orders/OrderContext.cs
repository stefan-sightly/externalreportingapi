﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class OrderContext
    {
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountRefCode { get; set; }

        public Guid AdvertisertId { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserRefCode { get; set; }

        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignRefCode { get; set; }

        public Guid InsertionOrderId { get; set; }
        public string InsertionOrderName { get; set; }
        public string InsertionOrderRefCode { get; set; }
        public string PlacementValue { get; set; }
        public string PlacementName { get; set; }   
    }
}