﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class OrderStatsRequest
    {
        #region Properties
        /// <summary>
        /// Property that holds the start date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Property that holds the end date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime EndDate { get; set; }

        
        /// <summary>
        /// Property that holds the value used for retrieving 
        /// campaigns under the specified account.
        /// </summary>
        public Guid AccountId { get; set; }

        #endregion
    }
}