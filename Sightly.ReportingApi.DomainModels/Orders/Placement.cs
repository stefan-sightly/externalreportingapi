﻿namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class Placement
    {
        public string PlacementValue { get; set; }
        public string PlacementName { get; set; }
        public string AdvertiserName { get; set; }
    }
}