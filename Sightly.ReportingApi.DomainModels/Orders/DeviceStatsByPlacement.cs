﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class DeviceStatsByPlacement
    {
        public string PlacementValue { get; set; }
        public string PlacementName { get; set; }
        public string Device { get; set; }
        public long Clicks { get; set; }
        public long CompletedViews { get; set; }
        public long Impressions { get; set; }
        public DateTime StatDate { get; set; }
        public decimal TotalSpend { get; set; }
    }
}