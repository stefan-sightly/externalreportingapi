﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents Campaign Stats Id and Order Ref Code key value pair
    /// </summary>
    public class OrderRefCodeOption
    {
        #region Properties
        /// <summary>
        /// Property that holds the order record ID.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the order reference code of the campaign.
        /// </summary>
        public string OrderRefCode { get; set; }
        #endregion
    }
}