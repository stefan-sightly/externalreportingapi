﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents device stats records.
    /// </summary>
    public class DeviceStats
    {
        #region Properties
        /// <summary>
        /// Property that holds the device type name.
        /// </summary>
        public string DeviceTypeName { get; set; }

        /// <summary>
        /// Property that holds the device types's clicks.
        /// </summary>
        public long? Clicks { get; set; }

        /// <summary>
        /// Property that holds the color value of each device type when displayed to chart.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Property that holds the completed views 
        /// for the device type.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the average cost
        /// per completed views for the device type.
        /// </summary>
        public decimal? AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the view rate 
        /// for the device type.
        /// </summary>
        public float? ViewRate { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// for the device type.
        /// </summary>
        public long? Impressions { get; set; }

        /// <summary>
        /// Property that holds if a device type is targeted
        /// </summary>
        public bool IsTargeted { get; set; }
        #endregion

        #region Read-only Properties
        public string FormattedCompletedViews
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.CompletedViews.HasValue)
                        return this.CompletedViews.Value.ToString("N0");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedAvgCPCV
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.AvgCPCV.HasValue)
                        return this.AvgCPCV.Value.ToString("C2");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedViewRate
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.ViewRate.HasValue)
                        return this.ViewRate.Value.ToString("0%");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedImpressions
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.Impressions.HasValue)
                        return this.Impressions.Value.ToString("N0");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }
        #endregion
    }
}