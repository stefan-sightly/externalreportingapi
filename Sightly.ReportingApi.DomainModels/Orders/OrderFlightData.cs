﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class OrderFlightData
    {
        public Guid InsertionOrderId { get; set; }
        public DateTime FlightStart { get; set; }
        public DateTime FlightEnd { get; set; }
        public decimal FlightBudget { get; set; }
    }
}