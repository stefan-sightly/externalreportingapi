﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents advertiser records.
    /// </summary>
    public class Advertiser
    {
        #region Properties
        /// <summary>
        /// Property that holds the advertiser id.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the advertiser name.
        /// </summary>
        public string AdvertiserName { get; set; }

        public string AdvertiserRefCode { get; set; }
        #endregion
    }
}
