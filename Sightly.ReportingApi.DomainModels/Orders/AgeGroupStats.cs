﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents age group stats records.
    /// </summary>
    public class AgeGroupStats
    {
        #region Properties
        /// <summary>
        /// Property that holds the age group's name.
        /// </summary>
        public string AgeGroupName { get; set; }

        /// <summary>
        /// Property that holds the age group's clicks.
        /// </summary>
        public long Clicks { get; set; }
        
        /// <summary>
        /// Property that holds the color value of each age group when displayed to chart.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Property that holds the age group's completed views.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the age group's average
        /// cost per completed view.
        /// </summary>
        public decimal? AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the age group's view rate.
        /// </summary>
        public float? ViewRate { get; set; }

        /// <summary>
        /// Property that holds the age group's impression count.
        /// </summary>
        public long? Impressions { get; set; }

        /// <summary>
        /// Property that holds if an age group is targeted
        /// </summary>
        public bool IsTargeted { get; set; }
        #endregion

        #region Read-only Properties
        public string FormattedAvgCPCV
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.AvgCPCV.HasValue)
                        return this.AvgCPCV.Value.ToString("C2");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedCompletedView
        {
            get 
            { 
                if(IsTargeted)
                {
                    if (this.CompletedViews.HasValue)
                        return this.CompletedViews.Value.ToString("N0");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedImpressions
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.Impressions.HasValue)
                        return this.Impressions.Value.ToString("N0");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }

        public string FormattedViewRate
        {
            get
            {
                if (IsTargeted)
                {
                    if (this.ViewRate.HasValue)
                        return this.ViewRate.Value.ToString("0%");
                    else
                        return "";
                }
                else
                {
                    return "x";
                }
            }
        }
        #endregion
    }
}