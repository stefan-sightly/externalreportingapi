﻿namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class AdStatsAggregate
    {
        /// <summary>
        /// Property that holds the ad's average
        /// cost per completed view.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the ad's clicks.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the ad's completed views.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the ad's impression count.
        /// </summary>
        public long Impressions { get; set; }

        public float Quartile25Percents { get; set; }
        public float Quartile50Percents { get; set; }
        public float Quartile75Percents { get; set; }
        public float Quartile100Percents { get; set; }

        /// <summary>
        /// Property that holds the ad's video name.
        /// </summary>
        public string VideoAdName { get; set; }

        /// <summary>
        /// Property that holds the ad's view rate.
        /// </summary>
        public float ViewRate { get; set; }

        /// <summary>
        /// Property that holds the ad's view time.
        /// </summary>
        public int ViewTime { get; set; }

        /// <summary>
        /// Property that holds the video length in seconds.
        /// </summary>
        public int? VideoLengthInSeconds { get; set; }
    }
}