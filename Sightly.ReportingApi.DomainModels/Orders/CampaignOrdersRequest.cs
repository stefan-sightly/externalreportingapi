﻿using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents request for retrieval
    /// of all orders under a specific campaign.
    /// </summary>
    public class CampaignOrdersRequest
    {
        #region Properties
        /// <summary>
        /// Property that holds the ID of the
        /// target order's parent campaign.
        /// </summary>
        public Guid CampaignId { get; set; }

        /// <summary>
        /// Property that holds the end date 
        /// that would be utilized to filter
        /// down the orders that falls under
        /// the campaign.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the start date
        /// that would be utilized to filter
        /// down the orders that falls under
        /// the campaign.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the order status
        /// IDs of requested orders.
        /// </summary>
        public List<Guid> OrderStatusIds { get; set; }
        #endregion
    }
}