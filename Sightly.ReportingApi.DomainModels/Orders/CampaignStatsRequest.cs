﻿using Sightly.ReportingApi.Core.DataManagement;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents request for campaign stats data.
    /// </summary>
    public class CampaignStatsRequest
    {
        #region Properties
        /// <summary>
        /// Property that holds the start date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the end date that would 
        /// be used for filtering campaign stats data.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the search term that would be used 
        /// to search for specific advertiser or campaign records.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Property that holds the value used for retrieving 
        /// campaigns under the specified advertiser.
        /// </summary>
        public Guid? AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the value used for retrieving 
        /// campaigns under the specified account.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the value used for retrieving 
        /// campaigns under the specified sub-account.
        /// </summary>
        public Guid? SubAccountId { get; set; }

        /// <summary>
        /// Property that holds values used for filtering 
        /// campaigns with the specified order status ids.
        /// </summary>
        public List<Guid> OrderStatusIds { get; set; }
        #endregion
    }
}