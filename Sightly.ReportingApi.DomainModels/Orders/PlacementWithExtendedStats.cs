﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    public class PlacementWithExtendedStats
    {
        public string PlacementValue { get; set; }
        public string PlacementName { get; set; }
        public DateTime StatDate { get; set; }
        public long Clicks { get; set; }
        public long CompletedViews { get; set; }
        public long Impressions { get; set; }
        public decimal TotalSpend { get; set; }
        public decimal ViewTime { get; set; }
        public decimal Reach { get; set; }
        public decimal AudienceRetention25 { get; set; }
        public decimal AudienceRetention50 { get; set; }
        public decimal AudienceRetention75 { get; set; }
        public decimal AudienceRetention100 { get; set; }
    }
}