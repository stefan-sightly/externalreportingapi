﻿using System;

namespace Sightly.ReportingApi.DomainModels.Orders
{
    /// <summary>
    /// Class that represents ad stats records.
    /// </summary>
    public class AdStats
    {
        #region Properties
        /// <summary>
        /// Property that holds the value of the ad 
        /// record Id that owns the stats instance.
        /// </summary>
        public Guid AdId { get; set; }

        /// <summary>
        /// Property that holds the ad's audience
        /// retention to 100 percentile.
        /// </summary>
        public int AudienceRetention100 { get; set; }

        /// <summary>
        /// Property that holds the ad's audience
        /// retention to 25 percentile.
        /// </summary>
        public int AudienceRetention25 { get; set; }

        /// <summary>
        /// Property that holds the ad's audience
        /// retention to 50 percentile.
        /// </summary>
        public int AudienceRetention50 { get; set; }

        /// <summary>
        /// Property that holds the ad's audience
        /// retention to 75 percentile.
        /// </summary>
        public int AudienceRetention75 { get; set; }

        /// <summary>
        /// Property that holds the ad's average
        /// cost per completed view.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the ad's clicks.
        /// </summary>
        public long Clicks { get; set; }
        
        /// <summary>
        /// Property that holds the ad's completed views.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the ad's impression count.
        /// </summary>
        public long Impressions { get; set; }

        /// <summary>
        /// Property that holds the ad's video name.
        /// </summary>
        public string VideoAdName { get; set; }

        /// <summary>
        /// Property that holds the ad's view rate.
        /// </summary>
        public float ViewRate { get; set; }

        /// <summary>
        /// Property that holds the ad's view time.
        /// </summary>
        public int ViewTime { get; set; }

        /// <summary>
        /// Property that holds the video length in seconds.
        /// </summary>
        public int? VideoLengthInSeconds { get; set; }
        #endregion

        #region Read Only Properties
        public string FormattedAvgCPCV
        {
            get
            {
                return this.AvgCPCV.ToString("C2");
            }
        }

        public string FormattedCompletedViews
        {
            get
            {
                return this.CompletedViews.ToString("N0");
            }
        }

        public string FormattedImpressions
        {
            get
            {
                return this.Impressions.ToString("N0");
            }
        }

        public string FormattedViewRate
        {
            get
            {
                return this.ViewRate.ToString("0%");
            }
        }

        public string FormattedViewTime
        {
            get
            {
                return this.ViewTime.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that returns the formatted
        /// view time hours of the campaign.
        /// </summary>
        public string FormattedViewTimeHours
        {
            get
            {
                return this.ViewTimeHours.ToString("N0") + " hours";
            }
        }

        /// <summary>
        /// Read-only property that returns the campaign's
        /// view time hours.
        /// </summary>
        public int ViewTimeHours
        {
            get
            {
                return (this.ViewTime / 60) / 60;
            }
        }
        #endregion
    }
}