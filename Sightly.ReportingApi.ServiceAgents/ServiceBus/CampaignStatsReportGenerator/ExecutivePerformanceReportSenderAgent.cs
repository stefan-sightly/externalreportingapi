﻿using NServiceBus;
using Sightly.CampaignStatsReportGenerator.Commands;
using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.Infrastructures;
using Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus.CampaignStatsReportGenerator;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.ServiceAgents.ServiceBus.CampaignStatsReportGenerator
{
    /// <summary>
    /// Class that publishes send executive performance 
    /// report commands to campaign stats report generation
    /// service bus.
    /// </summary>
    public class ExecutivePerformanceReportSenderAgent : IExecutivePerformanceReportSenderAgent
    {
        #region Fields
        private IBus bus;
        private IConfigurationStore configurationStore;
        private IObjectMapper objectMapper;
        #endregion

        #region Constructor
        public ExecutivePerformanceReportSenderAgent(
                    IBus bus,
                    IConfigurationStore configurationStore,
                    IObjectMapper objectMapper
            )
        {
            this.bus = bus;
            this.configurationStore = configurationStore;
            this.objectMapper = objectMapper;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for sending executive 
        /// performance reports.
        /// </summary>
        /// <param name="user">
        ///     The object that contains the logged in 
        ///     user information
        /// </param>
        /// <param name="request">
        ///     Object that contains information regarding
        ///     the executive performance report that would 
        ///     be sent to end user.
        /// </param>
        public void SendGenerateReportRequest(User user, ExecutivePerformanceReportRequest request)
        {
            //request.AccountId = user.AccountId;
            //request.RecipientFirstName = user.FirstName;
            //request.RecipientEmailAddress = user.EmailAddress;

            SendExecutivePerformanceReport command = objectMapper.Map<
                              SendExecutivePerformanceReport,
                              ExecutivePerformanceReportRequest
                          >(request);

            var endpointName = configurationStore.GetKeyValue(ConfigurationKey.ReportGeneratorEndpointName);
            bus.Send(endpointName, command);
        }
        #endregion
    }
}
