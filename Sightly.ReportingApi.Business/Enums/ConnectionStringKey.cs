﻿namespace Sightly.ReportingApi.Business.Enums
{
    /// <summary>
    /// Enumeration that contains key used for obtaining connection strings
    /// </summary>
    public enum ConnectionStringKey
    {
        /// <summary>
        /// The key that represents Operations database connection string
        /// </summary>
        Operations,

        /// <summary>
        /// The key that represents Reporting Interface database connection string
        /// </summary>
        ReportingInterface,

        /// <summary>
        /// The key that represents User Management database connection string
        /// </summary>
        UserManagement
    }
}