﻿namespace Sightly.ReportingApi.Business.Enums
{
    /// <summary>
    /// Enumeration that contains keys used for obtaining configuration values
    /// </summary>
    public enum ConfigurationKey
    {
        /// <summary>
        /// Member that represents the settings for
        /// the endpoint name of the campaign stats
        /// report generator service bus.
        /// </summary>
        ReportGeneratorEndpointName
    }
}