﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of executive performance date range data-accessing 
    /// members
    /// </summary>
    public interface IExecutivePerformanceDateRangeDataStore :
        IListRetriever<ExecutivePerformanceDateRange>
    {
    }
}