﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of order status data-accessing members
    /// </summary>
    public interface IOrderStatusDataStore : IListRetriever<OrderStatus>
    {
    }
}