﻿using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of campaign stats data-accessing members.
    /// </summary>
    public interface ICampaignDataStore
    {
        /// <summary>
        ///     Method used for retrieving the campaign
        ///     targeted population on a specific campaign
        /// </summary>
        /// <param name="request">
        ///     Object that holds filtering values that would 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     Campaign targeted population.
        /// </returns>
        long? GetCampaignTargetedPopulation(CampaignTargetedPopulationRequest request);
    }
}