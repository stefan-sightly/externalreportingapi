﻿using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of advertiser data-accessing members
    /// </summary>
    public interface IAdvertiserDataStore
    {
        /// <summary>
        /// Method used for retrieving all advertisers under 
        /// the user's account and its sub-account. 
        /// </summary>
        /// <param name="accountId">
        ///     Account id that owns the advertisers that 
        ///     would be retrieved.
        /// </param>
        /// <param name="subAccountIds">
        ///     List of sub-account ids that owns advertisers 
        ///     that would be included in the result set.
        /// </param>
        /// <returns>List of advertisers under the provided account ids.</returns>
        List<Advertiser> GetAdvertiserList(Guid accountId, List<Guid> subAccountIds, Guid apiSessionId);

        List<AdvertiserRefCode> GetAdvertiserRefCodeListByAccountRefCode(string accountRefCode, Guid apiSessionId);
    }
}
