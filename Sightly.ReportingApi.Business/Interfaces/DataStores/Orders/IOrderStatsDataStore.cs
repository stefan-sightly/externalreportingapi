﻿using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    public interface IOrderStatsDataStore
    {
        SubsetRequestResult<OrderStats> GetExecutivePerformanceOrderStats(
            OrderStatsRequest orderStatsRequest,
            SubsetRequest targetSubset,
            User user
            );
    }
}