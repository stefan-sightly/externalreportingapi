﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of age group stats data-accessing members.
    /// </summary>
    public interface IAgeGroupStatsDataStore : IListRetrieverByParentId<AgeGroupStats, Guid>
    {
        /// <summary>
        /// Method that retrieves aggregated age group stats record.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id of the record.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats.
        /// </returns>
        List<AgeGroupStats> GetListByCampaignId(Guid campaignId);

        /// <summary>
        /// Method used to retrieve aggregated age group stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the age group stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the age group stats.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats based on
        ///     the specified order IDs.
        /// </returns>
        List<AgeGroupStats> GetListByOrderIds(
                                    List<Guid> orderIds,
                                    DateTime startDate,
                                    DateTime endDate
                            );
    }
}