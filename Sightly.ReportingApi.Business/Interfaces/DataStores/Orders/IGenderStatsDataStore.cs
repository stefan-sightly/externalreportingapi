﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of gender stats data-accessing members.
    /// </summary>
    public interface IGenderStatsDataStore : IListRetrieverByParentId<GenderStats, Guid>
    {
        /// <summary>
        /// Method that retrieves aggregated gender stats record.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id of the record.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats.
        /// </returns>
        List<GenderStats> GetListByCampaignId(Guid campaignId);

        /// <summary>
        /// Method used to retrieve aggregated gender stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the gender stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the gender stats.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats based on
        ///     the specified order IDs.
        /// </returns>
        List<GenderStats> GetListByOrderIds(
                                    List<Guid> orderIds,
                                    DateTime startDate,
                                    DateTime endDate
                          );
    }
}