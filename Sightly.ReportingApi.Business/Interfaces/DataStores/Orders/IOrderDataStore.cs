﻿using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of order data-accessing members
    /// </summary>
    public interface IOrderDataStore
    {
        /// <summary>
        /// Method used for retrieving the target population
        /// on a specific order.
        /// </summary>
        /// <param name="orderId">Order ID.</param>
        /// <returns>Target population.</returns>
        long? GetTargetPopulation(Guid orderId);

        /// <summary>
        /// Method used for retrieving list of orders under a 
        /// given request.
        /// </summary>
        /// <param name="campaignOrdersRequest">
        ///     Object that holds filtering values that will 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     List of orders under the specified request.
        /// </returns>
        List<Order> GetCampaignOrders(CampaignOrdersRequest campaignOrdersRequest);

        /// <summary>
        /// Method used for retrieving a list of order reference codes 
        /// together with the campaign stats ID under
        /// the user's account and its sub-account.
        /// </summary>
        /// <param name="accountId">
        ///     Account ID of the user.
        /// </param>
        /// <param name="subAccoundIds">
        ///     List of Sub Account IDs.
        /// </param>
        /// <returns>
        ///     List of order reference codes together 
        ///     with their order ID.
        /// </returns>
        List<OrderRefCodeOption> GetReferenceCodeList(Guid accountId, List<Guid> subAccountIds, Guid apiSessionId);

        List<OrderContext> GetOrderContextBySessionId(Guid sessionId);
        OrderFlightData GetOrderFlightDataByOrderId(Guid orderId);
    }
}