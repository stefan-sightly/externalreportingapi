﻿using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of campaign stats data-accessing members.
    /// </summary>
    public interface ICampaignStatsDataStore
    {
        /// <summary>
        /// Method that retrieves aggregated campaign stats records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Request object that holds filtering values
        ///     that would be used for querying data.
        /// </param>
        /// <returns>
        ///     Aggregated campaign stats under a campaign
        /// </returns>
        CampaignStats GetAggregatedCampaignStats(AggregatedCampaignStatRequest campaignStatsRequest);

        /// <summary>
        /// Method that retrieves budget pacing summary records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="user">
        ///     User that requested the budget pacing summary records.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes budget pacing records.
        /// </returns>
        SubsetRequestResult<CampaignStats> GetBudgetPacingCampaignStats(
                                                CampaignStatsRequest campaignStatsRequest,
                                                SubsetRequest targetSubset,
                                                User user
                                           );

        /// <summary>
        /// Method that retrieves campaign executive performance
        /// summary of the specified campaign stats.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the order where to get campaign stats from.
        /// </param>
        /// <returns>
        ///     Campaign executive performance summary of the
        ///     specified order.
        /// </returns>
        CampaignStats GetCampaignExecutivePerformanceById(Guid orderId);

        /// <summary>
        /// Method that retrieves executive performance summary records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="user">
        ///     User that requested the executive performance 
        ///     summary records.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes executive performance summary records.
        /// </returns>
        SubsetRequestResult<CampaignStats> GetExecutivePerformanceCampaignStats(
                                                CampaignStatsRequest campaignStatsRequest,
                                                SubsetRequest targetSubset,
                                                User user, 
                                                Guid apiSessionId
                                           );
    }
}