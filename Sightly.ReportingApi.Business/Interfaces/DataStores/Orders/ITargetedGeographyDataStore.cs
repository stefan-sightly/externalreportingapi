﻿using Sightly.ReportingApi.DomainModels.Orders;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation
    /// of targeted geography accessing members.
    /// </summary>
    public interface ITargetedGeographyDataStore
    {
        /// <summary>
        ///     Method used for retrieving list of targeted geographies.
        /// </summary>
        /// <param name="campaignTargetedGeographieRequest">
        ///     Object that holds filtering values that would be 
        ///     used for querying data.
        /// </param>
        /// <returns>
        ///     List of targeted geographies under
        ///     the specified campaign id.
        /// </returns>
        List<CampaignTargetedGeography> GetCampaignTargetedGeographies(CampaignTargetedGeographyRequest campaignTargetedGeographieRequest);
    }
}