﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Orders
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of device stats data-accessing members.
    /// </summary>
    public interface IDeviceStatsDataStore : IListRetrieverByParentId<DeviceStats, Guid>
    {
        /// <summary>
        /// Method that retrieves aggregated device stats records.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id of the record.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats records.
        /// </returns>
        List<DeviceStats> GetListByCampaignId(Guid campaignId);

        /// <summary>
        /// Method used to retrieve aggregated device stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter device stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter device stats.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats based on
        ///     the specified order IDs.
        /// </returns>
        List<DeviceStats> GetListByOrderIds(
                              List<Guid> orderIds,
                              DateTime startDate,
                              DateTime endDate
                          );

        List<DeviceStats> GetDeviceStatsByCampaignAndDate(
                            Guid campaignId, 
                            DateTime startDate, 
                            DateTime endDate, 
                            Guid apiSessionId);
        List<DeviceStats> GetDeviceStatsByOrderAndDates(
                            Guid orderId, 
                            DateTime startDate, 
                            DateTime endDate, 
                            Guid apiSessionId);
    }
}