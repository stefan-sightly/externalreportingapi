﻿using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts
{
    /// <summary>
    /// Interface used for mandating implementation 
    /// of account data-accessing members
    /// </summary>
    public interface IAccountDataStore
    {
        /// <summary>
        /// Method used for retrieving all sub-account ids under an account.
        /// </summary>
        /// <param name="accountId">Parent account of account ids to be fetched.</param>
        /// <returns>List of sub-account ids under an account.</returns>
        List<Guid> GetSubAccountIds(Guid accountId, Guid apiSessionId);

        /// <summary>
        /// Method used for retrieving all sub-accounts under an account
        /// </summary>
        /// <param name="accountId">Parent account if of the accounts to be fetched</param>
        /// <returns>List of sub-accounts under an account.</returns>
        List<Account> GetSubAccounts(Guid accountId, Guid apiSessionId);

        List<AccountRefCode> GetSubAccountsByParentRefCode(string accountRefCode, Guid apiSessionId);
    }
}
