﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.Core.Interfaces.Basic;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;

namespace Sightly.ReportingApi.Business.Interfaces.DataStores.Users
{
    /// <summary>
    /// Interface used for mandating implementation of user data accessing members
    /// </summary>
    public interface IUserDataStore :
        IListRetrieverByIds<User, Guid>,
        IRetrieverByProperty<User, string>,
        IRetrieverById<User, Guid>
    {
        /// <summary>
        /// Method used for checking if the user has a valid credentials
        /// </summary>
        /// <param name="user">User information</param>
        /// <returns>True: user has a valid credentials; False: user does not have a valid credentials</returns>
        bool CheckIfValidCredential(User user);
    }
}
