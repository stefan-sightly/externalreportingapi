﻿using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus.CampaignStatsReportGenerator
{
    /// <summary>
    /// Interface used for mandating implementation of
    /// method used in sending of budget pacing reports.
    /// </summary>
    public interface IBudgetPacingReportSenderAgent
    {
        /// <summary>
        /// Method used for sending budget
        /// pacing reports.
        /// </summary>
        /// <param name="user">
        ///     Object that holds the information of the
        ///     logged in user
        /// </param>
        /// <param name="request">
        ///     Object that contains information regarding
        ///     the budget pacing report that would be sent
        ///     to the end user.
        /// </param>
        void SendGenerateReportRequest(User user, BudgetPacingReportRequest request);
    }
}
