﻿using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus.CampaignStatsReportGenerator
{
    /// <summary>
    /// Interface used for mandating implementation of
    /// methods used in sending of executive performance
    /// reports.
    /// </summary>
    public interface IExecutivePerformanceReportSenderAgent
    {
        /// <summary>
        /// Method used for sending executive 
        /// performance reports.
        /// </summary>
        /// <param name="user">
        ///     The object that contains the logged in 
        ///     user information
        /// </param>
        /// <param name="request">
        ///     The object that contains information regarding
        ///     the executive performance report that would 
        ///     be sent to end user.
        /// </param>
        void SendGenerateReportRequest(User user, ExecutivePerformanceReportRequest request);
    }
}
