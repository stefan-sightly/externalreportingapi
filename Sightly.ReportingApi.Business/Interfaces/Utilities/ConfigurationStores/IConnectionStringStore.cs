﻿using Sightly.ReportingApi.Business.Enums;

namespace Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores
{
    /// <summary>
    /// Interface used for mandating implementation of connection string retrieval methods
    /// </summary>
    public interface IConnectionStringStore
    {
        /// <summary>
        /// Method used for fetching a connection string value using the provided key
        /// </summary>
        /// <param name="key">The key to the connection string value to be retrieved</param>
        /// <returns>The connection string value obtained using the connection string key</returns>
        string GetKeyValue(ConnectionStringKey key);
    }
}