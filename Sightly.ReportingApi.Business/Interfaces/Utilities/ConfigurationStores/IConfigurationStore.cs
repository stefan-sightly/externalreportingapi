﻿using Sightly.ReportingApi.Business.Enums;

namespace Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores
{
    /// <summary>
    /// Interface used for mandating implementation of key value retrieval methods
    /// </summary>
    public interface IConfigurationStore
    {
        /// <summary>
        /// Method used for fetching value of provided keys
        /// </summary>
        /// <param name="key">ConfigurationKey member that represents the key of configuration value to be retrieved</param>
        /// <returns>The configuration value obtained using the configuration key</returns>
        string GetKeyValue(ConfigurationKey key);
    }
}