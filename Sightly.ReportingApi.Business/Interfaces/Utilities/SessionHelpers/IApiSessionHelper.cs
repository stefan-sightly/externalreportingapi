﻿using Sightly.ReportingApi.DomainModels.Accounts;
using System;

namespace Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers
{
    /// <summary>
    /// Interface that mandates the implementation of API session helper
    /// </summary>
    public interface IApiSessionHelper
    {
        /// <summary>
        /// Method used to the retrieve the user of the specified API session ID
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>User information</returns>
        User GetUser(Guid apiSessionId);
    }
}
