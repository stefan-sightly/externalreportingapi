﻿using System;

namespace Sightly.ReportingApi.Business.Interfaces.Utilities.Generators
{
    /// <summary>
    /// Interface used for mandating implementation for ID or unique identifier generation methods
    /// </summary>
    public interface IGuidGenerator
    {
        /// <summary>
        /// Method used to generate a unique identifier or ID
        /// </summary>
        /// <returns>Generated ID or unique identifier</returns>
        Guid GenerateComb();

        /// <summary>
        /// Method used to generate multiple IDs or unique identifiers
        /// </summary>
        /// <param name="count">Number of IDs to generate</param>
        /// <returns>A collection of IDs or unique identifiers</returns>
        Guid[] GenerateCombs(int count);
    }
}
