﻿namespace Sightly.ReportingApi.Business.Interfaces.Utilities.CryptographyServices
{
    public interface IHashService
    {
        /// <summary>
        /// Creates a salted PBKDF2 hash of the password.
        /// </summary>
        /// <param name="password">
        ///     The password to hash.
        /// </param>
        /// <returns>
        ///     The hash of the password.
        /// </returns>
        string CreateHash(string password);

        /// <summary>
        /// Method used for generating a SHA-256 hash of string values.
        /// </summary>
        /// <param name="input">
        ///     String value to hash.
        /// </param>
        /// <returns>
        ///     Hashed form of the input string.
        /// </returns>
        string GetSha256Hash(string input);

        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="access">
        ///     Hashed password input to check
        /// </param>
        /// <param name="goodHash">
        ///     A hash of the correct password.
        /// </param>
        /// <returns>
        ///     True if the password is correct. False otherwise.
        /// </returns>
        bool ValidateString(string password, string goodHash);
    }
}
