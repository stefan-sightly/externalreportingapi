﻿namespace Sightly.ReportingApi.Business.Interfaces.Infrastructures
{
    /// <summary>
    /// Interface used for mandating the implementation of object mapping configuration members
    /// </summary>
    public interface IObjectMappingConfigurator
    {
        /// <summary>
        /// Method used to initialize object mapping configurations
        /// </summary>
        void Initialize();
    }
}