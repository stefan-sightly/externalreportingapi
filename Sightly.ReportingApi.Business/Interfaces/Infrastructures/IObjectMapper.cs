﻿namespace Sightly.ReportingApi.Business.Interfaces.Infrastructures
{
    /// <summary>
    /// Interface that mandates implementation of object mapping methods.
    /// </summary>
    public interface IObjectMapper
    {
        /// <summary>
        /// Method used for converting source object to an instance of the specified type.
        /// </summary>
        /// <typeparam name="T">Mapping destination type.</typeparam>
        /// <typeparam name="S">Mapping source type.</typeparam>
        /// <param name="source">Source object to be converted to destination type.</param>
        /// <returns>The converted source instance in destination type form.</returns>
        T Map<T, S>(S source);
    }
}
