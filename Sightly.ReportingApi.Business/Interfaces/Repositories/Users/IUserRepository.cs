﻿using Sightly.ReportingApi.DomainModels.Accounts;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Users
{
    /// <summary>
    /// Interface that mandates implementation of user-related repository methods
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Method used to get the authenticated user using credentials
        /// </summary>
        /// <param name="user">User credentials which includes username (email address) and password</param>
        /// <returns>Information of the authenticated user</returns>
        User GetAuthenticatedUser(User user);
    }
}
