﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups
{
    public interface ICampaignPerformanceReportTypeRepository :
        IListRetriever<ReportType>
    {
    }
}
