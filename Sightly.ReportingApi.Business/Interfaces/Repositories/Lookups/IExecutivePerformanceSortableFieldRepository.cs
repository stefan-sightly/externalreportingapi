﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// executive performance sortable field business 
    /// work flow members.
    /// </summary>
    public interface IExecutivePerformanceSortableFieldRepository :
        IListRetriever<ExecutivePerformanceSortableField>
    {
    }
}