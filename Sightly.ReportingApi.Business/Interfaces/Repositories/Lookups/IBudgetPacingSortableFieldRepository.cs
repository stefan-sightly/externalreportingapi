﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// budget pacing sortable field business 
    /// work-flow members.
    /// </summary>
    public interface IBudgetPacingSortableFieldRepository :
        IListRetriever<BudgetPacingSortableField>
    {
    }
}