﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Lookups;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// report type business-work flow members.
    /// </summary>
    public interface IExecutivePerformanceReportTypeRepository :
        IListRetriever<ReportType>
    {
    }
}