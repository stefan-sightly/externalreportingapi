﻿using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// order business work-flow members.
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Method used for retrieving the target population
        /// on a specific order.
        /// </summary>
        /// <param name="orderId">
        ///     An identifier that contains a record of a 
        ///     target population.
        /// </param>
        /// <returns>
        ///     Computed numbers of target population.
        /// </returns>
        long? GetTargetPopulation(Guid orderId);

        /// <summary>
        /// Method used for retrieving orders under a
        /// given request.
        /// </summary>
        /// <param name="campaignOrdersRequest">
        ///     Object that holds filtering values that will 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     List of orders.
        /// </returns>
        List<Order> GetCampaignOrders(CampaignOrdersRequest campaignOrdersRequest);

        /// <summary>
        /// Method used for retrieving order reference codes
        /// together with their order ID
        /// under the current user's account.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <returns>
        ///     List of order reference codes together 
        ///     with their order ID.
        /// </returns>
        List<OrderRefCodeOption> GetReferenceCodeList(Guid apiSessionId);

        List<OrderContext> GetOrderContextBySessionId(Guid sessionId);
        OrderFlightData GetOrderFlightDataByOrderId(Guid orderId);
    }
}