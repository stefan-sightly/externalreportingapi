﻿using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of
    /// campaign business work-flow members.
    /// </summary>
    public interface ICampaignRepository
    {
        /// <summary>
        ///     Method used for retrieving the campaign targeted
        ///     population on a specific request.
        /// </summary>
        /// <param name="request">
        ///     Object that holds filtering values that would 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     Campaign targeted population.
        /// </returns>
        long? GetCampaignTargetedPopulation(CampaignTargetedPopulationRequest request);
    }
}