﻿using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// advertiser business-work flow members.
    /// </summary>
    public interface IAdvertiserRepository
    {
        /// <summary>
        /// Method used for retrieving all advertisers under 
        /// the user's account and its sub-account. 
        /// </summary>
        /// <param name="apiSessionId">
        ///     Session id of the user that owns the advertisers 
        ///     that would be retrieved.
        /// </param>
        /// <returns>List of advertisers under the provided user's session id.</returns>
        List<Advertiser> GetAdvertiserList(Guid apiSessionId);

        List<AdvertiserRefCode> GetAdvertiserRefCodeListByAccountRefCode(string AccountRefCode, Guid apiSessionId);
    }
}
