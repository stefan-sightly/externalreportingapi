﻿using System;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    public interface IOrderStatsRepository
    {
        SubsetRequestResult<OrderStats> GetExecutivePerformanceOrderStats(
            Guid apiSessionId,
            OrderStatsRequest orderStatsRequest,
            SubsetRequest targetSubset
            );
    }
}