﻿using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;
using System;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// campaign stats business work-flow members.
    /// </summary>
    public interface ICampaignStatsRepository
    {
        /// <summary>
        /// Method used for retrieving aggregated campaign stats.
        /// </summary>
        /// <param name="aggregatedCampaignStatsRequest">
        ///     Request object that is needed to retrieve records.
        /// </param>
        /// <returns>
        ///    Aggregated campaign stats under a campaign.
        /// </returns>
        CampaignStats GetAggregatedCampaignStats(AggregatedCampaignStatRequest aggregatedCampaignStatsRequest);

        /// <summary>
        /// Method that retrieves budget pacing summary records.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes budget pacing records.
        /// </returns>
        SubsetRequestResult<CampaignStats> GetBudgetPacingCampaignStats(
                                                Guid apiSessionId,
                                                CampaignStatsRequest campaignStatsRequest,
                                                SubsetRequest targetSubset
                                           );

        /// <summary>
        /// Method that retrieves campaign executive performance
        /// summary of the specified campaign stats.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the order where to get campaign stats from.
        /// </param>
        /// <returns>
        ///     Campaign executive performance summary of the
        ///     specified order.
        /// </returns>
        CampaignStats GetCampaignExecutivePerformanceById(Guid orderId);

        /// <summary>
        /// Method that retrieves executive performance summary records.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes executive performance summary records.
        /// </returns>
        SubsetRequestResult<CampaignStats> GetExecutivePerformanceCampaignStats(
                                                        Guid apiSessionId,
                                                        CampaignStatsRequest campaignStatsRequest,
                                                        SubsetRequest targetSubset
                                                   );

        /// <summary>
        /// Method used for sending executive 
        /// performance reports.
        /// </summary>
        ///  <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="request">
        ///     Command that contains information regarding
        ///     the executive performance report that would 
        ///     be sent to end user.
        /// </param>
        void SendExecutivePeformanceReportRequest(Guid apiSessionId, ExecutivePerformanceReportRequest request);

        /// <summary>
        /// Method used for sending executive
        /// performance reports.
        /// </summary>
        ///  <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="request">
        ///     Command that contains information regarding
        ///     the budget pacing report that would be sent
        ///     to end user.
        /// </param>
        void SendBudgetPacingReportRequest(Guid apiSessionId, BudgetPacingReportRequest request);

        /// <summary>
        /// Method used for sending campaign
        /// performance reports.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="request">
        ///     Object that contains information regarding
        ///     the campaign performance report that would
        ///     be sent to end user.
        /// </param>
        void SendCampaignPerformanceReportRequest(Guid apiSessionId, CampaignPerformanceReportRequest request);
    }
}
