﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// gender stats business work-flow members.
    /// </summary>
    public interface IGenderStatsRepository : IListRetrieverByParentId<GenderStats, Guid>
    {
        /// <summary>
        /// Method that used to retrieves aggregated gender
        /// stats under the specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id of the aggregated gender stats record.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats record.
        /// </returns>
        List<GenderStats> GetListByCampaignId(Guid campaignId);

        /// <summary>
        /// Method used to retrieve aggregated gender stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be 
        ///     fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the gender stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the gender stats.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats based on
        ///     the specified order IDs.
        /// </returns>
        List<GenderStats> GetListByOrderIds(
                                    List<Guid> orderIds,
                                    DateTime startDate,
                                    DateTime endDate
                          );
    }
}