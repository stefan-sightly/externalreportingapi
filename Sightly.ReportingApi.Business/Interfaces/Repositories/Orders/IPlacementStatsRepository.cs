﻿using System;
using System.Collections.Generic;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    public interface IPlacementStatsRepository
    {
        List<Placement> GetKnownPlacementsBySession(Guid sessionId);
        List<Placement> GetKnownPlacementsByAdvertiserRefCode(Guid sessionId, string advertiserRefCode);
        List<PlacementStats> GetPlacementStatsAggregatedByDates(string placement, DateTime startDate, DateTime endDate);
        List<PlacementStatsWithDate> GetPlacementStatsDailyAggregatedByDates(string placement, DateTime startDate, DateTime endDate);
        List<PlacementStats> GetAllStatsByRefCodeAndDates(string advertiserRefCode, DateTime startDate, DateTime endDate);
        List<PlacementStatsWithDate> GetAllDailyStatsByRefCodeAndDates(string advertiserRefCode, DateTime startDate, DateTime endDate);
        List<AdvertiserPlacementStatsWithDate> GetDailyPlacementStatsByAccountRefCodeAndDates(string accountRefCode, DateTime startDate, DateTime endDate, Guid apiSessionId);
        ExtendedPlacementStats GetExtendedStatsAggregatedByPlacementAndDate(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId);
        List<ExtendedPlacementStats> GetExtendedStatsDailyByPlacementAndDate(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId);
        List<DeviceStatsByPlacement> GetAggregatedDeviceStatsByPlacementAndDates(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId);
        List<DeviceStatsByPlacement> GetDailyDeviceStatsByPlacementAndDates(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId);
        List<PlacementWithExtendedStats> GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(string placement, DateTime startDate, DateTime endDate, Guid apiSessionId);
    }
}