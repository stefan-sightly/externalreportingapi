﻿using Sightly.ReportingApi.DomainModels.Orders;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation
    /// of targeted geography business work-flow.
    /// </summary>
    public interface ITargetedGeographyRepository
    {
        /// <summary>
        /// Method used for retrieving list of targeted geography
        /// under a campaign id.
        /// </summary>
        /// <param name="campaignTargetedGeographyRequest">
        ///     Request object that holds filtering values that 
        ///     would be used for querying data.
        /// </param>
        /// <returns>
        ///     List of targeted geographies under the specified
        ///     campaign id.
        /// </returns>
        List<CampaignTargetedGeography> GetCampaignTargetedGeographies(CampaignTargetedGeographyRequest campaignTargetedGeographyRequest);
    }
}