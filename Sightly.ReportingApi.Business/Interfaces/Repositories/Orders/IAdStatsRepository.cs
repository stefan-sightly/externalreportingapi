﻿using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Orders
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// ad stats business-workflow members.
    /// </summary>
    public interface IAdStatsRepository
    {
        /// <summary>
        /// Method that retrieves ad stats data.
        /// </summary>
        /// <param name="orderId">
        ///     Order ID is used for filtering 
        ///     ad stats data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <returns>
        ///     List ad stats instances for Campaign/Order Performance.
        /// </returns>
        SubsetRequestResult<AdStats> GetAdStats(
                                         Guid orderId,
                                         SubsetRequest targetSubset,
                                         Guid apiSessionId
                                     );

        /// <summary>
        /// Method that retrieves ad stats of the specified
        /// campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Id needed to retrieve specific ad stats record.
        /// </param>
        /// <returns>
        ///     List of ad stats retrieved using campaign id.
        /// </returns>
        SubsetRequestResult<AdStats> GetAdStatsByCampaignId(
                                         Guid campaignId,
                                         SubsetRequest targetSubset,
                                         Guid apiSessionId
                                     );

        /// <summary>
        /// Method used to retrieve ad stats by order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     Order IDs that is used for filtering
        ///     ad stats data.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter ad stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter ad stats.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        ///     List of aggregated ad stats instances for
        ///     Campaign/Order Performance
        /// </returns>
        SubsetRequestResult<AdStats> GetAdStatsByOrderIds(
                                         List<Guid> orderIds,
                                         DateTime startDate,
                                         DateTime endDate,
                                         SubsetRequest targetSubset
                                     );

        AdStatsAggregate GetAdStatByCampaignAndDates(
            Guid campaignId,
            DateTime startDate,
            DateTime endDate,
            Guid apiSessionId
            );

        AdStatsAggregate GetAdStatsByOrderAndDates(
            Guid orderId, 
            DateTime startDate, 
            DateTime endDate, 
            Guid apiSessionId
            );
    }
}
