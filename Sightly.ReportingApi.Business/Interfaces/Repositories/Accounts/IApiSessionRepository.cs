﻿using Sightly.ReportingApi.Core.Basic.Interfaces;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts
{
    /// <summary>
    /// Interface used for mandating implementation of API session business logic methods
    /// </summary>
    public interface IApiSessionRepository : IInsertor<ApiSession>
    {
        /// <summary>
        /// Method used to check if an API session is valid
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>True if the API session is valid; otherwise false</returns>
        bool CheckIfValid(Guid apiSessionId);

        /// <summary>
        /// Method used to extend an API session
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        void Extend(Guid apiSessionId);

        /// <summary>
        /// Method used to log out the API session of the current user
        /// </summary>
        /// <param name="session">API session information</param>
        void Logout(ApiSession session);
    }
}
