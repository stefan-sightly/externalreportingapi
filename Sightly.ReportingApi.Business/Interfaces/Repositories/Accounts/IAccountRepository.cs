﻿using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts
{
    /// <summary>
    /// Interface used for mandating implementation of 
    /// account business-work flow members.
    /// </summary>
    public interface IAccountRepository
    {
        /// <summary>
        /// Method used for retrieving all sub accounts under
        /// the specified account.
        /// </summary>
        /// <param name="apiSessionId">
        ///     Session id of the user that owns the sub accounts
        ///     that would be retrieved.
        /// </param>
        /// <returns>List of accounts under the provided user's session id.</returns>
        List<Account> GetSubAccounts(Guid apiSessionId);

        List<AccountRefCode> GetSubaccountsByParentRefCode(Guid apiSessionId, string accountRefCode);
    }
}
