﻿using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DataStore.Dapper.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    public class OrderStatusRepository : IOrderStatusRepository
    {
        #region Fields
        private IOrderStatusDataStore orderStatusDataStore;
        #endregion

        #region Constructors
        public OrderStatusRepository(IOrderStatusDataStore orderStatusDataStore)
        {
            this.orderStatusDataStore = orderStatusDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of order status.
        /// </summary>
        /// <returns>List of order status.</returns>
        public List<OrderStatus> GetList()
        {
            return orderStatusDataStore.GetList();
        }
        #endregion
    }
}