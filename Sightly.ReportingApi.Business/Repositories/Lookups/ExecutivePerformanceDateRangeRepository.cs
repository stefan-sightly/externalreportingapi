﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to executive performance date range.
    /// </summary>
    public class ExecutivePerformanceDateRangeRepository : IExecutivePerformanceDateRangeRepository
    {
        #region Fields
        private IExecutivePerformanceDateRangeDataStore executivePerformanceDateRangeDataStore;
        #endregion

        #region Constructor
        public ExecutivePerformanceDateRangeRepository(IExecutivePerformanceDateRangeDataStore executivePerformanceDateRangeDataStore)
        {
            this.executivePerformanceDateRangeDataStore = executivePerformanceDateRangeDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving executive performance date range records.
        /// </summary>
        /// <returns>List of executive performance date range records.</returns>
        public List<ExecutivePerformanceDateRange> GetList()
        {
            return executivePerformanceDateRangeDataStore.GetList();
        }
        #endregion
    }
}