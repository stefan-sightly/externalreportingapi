﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business workflow
    /// members related to executive performance 
    /// sortable fields.
    /// </summary>
    public class ExecutivePerformanceSortableFieldRepository : IExecutivePerformanceSortableFieldRepository
    {
        #region Constructors
        public ExecutivePerformanceSortableFieldRepository(
                    IExecutivePerformanceSortableFieldDataStore executivePerformanceSortableFieldDataStore
               )
        {
            this.executivePerformanceSortableFieldDataStore = executivePerformanceSortableFieldDataStore;
        }
        #endregion

        #region Fields
        private IExecutivePerformanceSortableFieldDataStore executivePerformanceSortableFieldDataStore;
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of 
        /// executive performance sortable fields.
        /// </summary>
        /// <returns>
        ///     List of executive performance sortable fields.
        /// </returns>
        public List<ExecutivePerformanceSortableField> GetList()
        {
            return this.executivePerformanceSortableFieldDataStore.GetList();
        }
        #endregion
    }
}