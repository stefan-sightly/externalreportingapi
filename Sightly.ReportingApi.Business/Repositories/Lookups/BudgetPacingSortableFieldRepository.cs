﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to budget pacing sortable
    /// fields.
    /// </summary>
    public class BudgetPacingSortableFieldRepository : IBudgetPacingSortableFieldRepository
    {
        #region Constructors
        public BudgetPacingSortableFieldRepository(
                    IBudgetPacingSortableFieldDataStore budgetPacingSortableFieldDataStore
               )
        {
            this.budgetPacingSortableFieldDataStore = budgetPacingSortableFieldDataStore;
        }
        #endregion

        #region Fields
        private IBudgetPacingSortableFieldDataStore budgetPacingSortableFieldDataStore;
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of 
        /// budget pacing sortable fields.
        /// </summary>
        /// <returns>
        ///     List of budget pacing sortable fields.
        /// </returns>
        public List<BudgetPacingSortableField> GetList()
        {
            return this.budgetPacingSortableFieldDataStore.GetList();
        }
        #endregion
    }
}