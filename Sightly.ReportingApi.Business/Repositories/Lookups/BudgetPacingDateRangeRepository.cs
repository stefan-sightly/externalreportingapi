﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to budget pacing date range.
    /// </summary>
    public class BudgetPacingDateRangeRepository : IBudgetPacingDateRangeRepository
    {
        #region Fields
        private IBudgetPacingDateRangeDataStore budgetPacingDateRangeDataStore;
        #endregion

        #region Constructor
        public BudgetPacingDateRangeRepository(IBudgetPacingDateRangeDataStore budgetPacingDateRangeDataStore)
        {
            this.budgetPacingDateRangeDataStore = budgetPacingDateRangeDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list 
        /// of budget pacing date range.
        /// </summary>
        /// <returns>List of budget pacing date range.</returns>
        public List<BudgetPacingDateRange> GetList()
        {
            return budgetPacingDateRangeDataStore.GetList();
        }
        #endregion
    }
}
