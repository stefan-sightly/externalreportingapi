﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to report type.
    /// </summary>
    public class BudgetPacingReportTypeRepository : IBudgetPacingReportTypeRepository
    {
        #region Fields
        private IBudgetPacingReportTypeDataStore reportTypeDataStore;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor of the report type repository
        /// </summary>
        /// <param name="reportTypeDataStore">
        ///     Report type data store.
        /// </param>
        public BudgetPacingReportTypeRepository(IBudgetPacingReportTypeDataStore reportTypeDataStore)
        {
            this.reportTypeDataStore = reportTypeDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Action used for retrieving the list of
        /// report types.
        /// </summary>
        /// <returns>
        ///     List of report types.
        /// </returns>
        public List<ReportType> GetList()
        {
            return reportTypeDataStore.GetList();
        }
        #endregion
    }
}
