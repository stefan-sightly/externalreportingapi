﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Lookups;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Lookups
{
    /// <summary>
    /// Class that implements business workflow
    /// members related to report type.
    /// </summary>
    public class CampaignPerformanceReportTypeRepository : ICampaignPerformanceReportTypeRepository
    {
        #region Fields
        private ICampaignPerformanceReportTypeDataStore reportTypeDataStore;
        #endregion

        #region Constructors
        public CampaignPerformanceReportTypeRepository(ICampaignPerformanceReportTypeDataStore reportTypeDataStore)
        {
            this.reportTypeDataStore = reportTypeDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Action used for retrieving the list of
        /// report types.
        /// </summary>
        /// <returns>
        ///     List of report types.
        /// </returns>
        public List<ReportType> GetList()
        {
            return reportTypeDataStore.GetList();
        }
        #endregion
    }
}
