﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business workflow
    /// members related to ad stats.
    /// </summary>
    public class AdStatsRepository : IAdStatsRepository
    {
        #region Fields
        private IAdStatsDataStore adStatsDataStore;
        private IApiSessionHelper apiSessionHelper;
        #endregion

        #region Constructors
        public AdStatsRepository(
                   IAdStatsDataStore adStatsDataStore,
                   IApiSessionHelper apiSessionHelper
               )
        {
            this.adStatsDataStore = adStatsDataStore;
            this.apiSessionHelper = apiSessionHelper;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that retrieves ad stats data.
        /// </summary>
        /// <param name="orderId">
        ///     Order ID is used for filtering 
        ///     ad stats data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <returns>
        ///     List ad stats instances for Campaign/Order Performance.
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStats(
                                                Guid orderId,
                                                SubsetRequest targetSubset,
                                                Guid apiSessionId
                                            )
        {
            return adStatsDataStore.GetAdStats(
                                        orderId,
                                        targetSubset,
                                        apiSessionHelper.GetUser(apiSessionId)
                                    );
        }

        /// <summary>
        /// Method that retrieves ad stats of the specified
        /// campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Id needed to retrieve specific ad stats record.
        /// </param>
        /// <returns>
        ///     List of ad stats retrieved using campaign id.
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStatsByCampaignId(
                                                Guid campaignId,
                                                SubsetRequest targetSubset,
                                                Guid apiSessionId
                                            )
        {
            return adStatsDataStore.GetAdStatsByCampaignId(campaignId, targetSubset, apiSessionId);
        }

        /// <summary>
        /// Method used to retrieve ad stats by order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     Order IDs that is used for filtering
        ///     ad stats data.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter ad stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter ad stats.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        ///     List of aggregated ad stats instances for
        ///     Campaign/Order Performance
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStatsByOrderIds(
                                                List<Guid> orderIds,
                                                DateTime startDate,
                                                DateTime endDate,
                                                SubsetRequest targetSubset
                                            )
        {
            return adStatsDataStore.GetAdStatsByOrderIds(
                                        orderIds,
                                        startDate,
                                        endDate,
                                        targetSubset
                                    );
        }


        public AdStatsAggregate GetAdStatByCampaignAndDates(
                                                Guid campaignId,
                                                DateTime startDate,
                                                DateTime endDate,
                                                Guid apiSessionId
                                            )
        {
            return adStatsDataStore.GetAdStatByCampaignAndDate(campaignId, startDate, endDate, apiSessionId);
        }

        public AdStatsAggregate GetAdStatsByOrderAndDates(Guid orderId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return adStatsDataStore.GetAdStatsByOrderAndDates(orderId, startDate, endDate, apiSessionId);
        }

        #endregion
    }
}
