﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to targeted geography.
    /// </summary>
    public class TargetedGeographyRepository : ITargetedGeographyRepository
    {
        #region Fields
        private ITargetedGeographyDataStore targetedGeographyDataStore;
        #endregion

        #region Constructor
        public TargetedGeographyRepository(ITargetedGeographyDataStore targetedGeographyDataStore)
        {
            this.targetedGeographyDataStore = targetedGeographyDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        ///     Method used for retrieving list of targeted geography
        ///     under a campaign id.
        /// </summary>
        /// <param name="campaignTargetedGeographyRequest">
        ///     Request object that holds filtering values that 
        ///     would be used for querying data.
        /// </param>
        /// <returns>
        ///     List of targeted geographies under the specified
        ///     campaign id.
        /// </returns>
        public List<CampaignTargetedGeography> GetCampaignTargetedGeographies(CampaignTargetedGeographyRequest campaignTargetedGeographyRequest)
        {
            return targetedGeographyDataStore.GetCampaignTargetedGeographies(campaignTargetedGeographyRequest);
        }
        #endregion
    }
}