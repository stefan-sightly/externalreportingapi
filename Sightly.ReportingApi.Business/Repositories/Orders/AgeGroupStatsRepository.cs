﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to age group stats.
    /// </summary>
    public class AgeGroupStatsRepository : IAgeGroupStatsRepository
    {
        #region Fields
        private IAgeGroupStatsDataStore ageGroupStatsDataStore;
        #endregion

        #region Constructor
        public AgeGroupStatsRepository(IAgeGroupStatsDataStore ageGroupStatsDataStore)
        {
            this.ageGroupStatsDataStore = ageGroupStatsDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving age group list
        /// of the specified order.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the related order record.
        /// </param>
        /// <returns>
        ///     List of age group stats of the specified
        ///     order.
        /// </returns>
        public List<AgeGroupStats> GetListByParentId(Guid orderId)
        {
            return ageGroupStatsDataStore.GetListByParentId(orderId);
        }

        /// <summary>
        /// Method used for retrieving aggregated age 
        /// group stats of a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats.
        /// </returns>
        public List<AgeGroupStats> GetListByCampaignId(Guid campaignId)
        {
            return ageGroupStatsDataStore.GetListByCampaignId(campaignId);
        }

        /// <summary>
        /// Method used to retrieve aggregated age group stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the age group stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the age group stats.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<AgeGroupStats> GetListByOrderIds(
                                            List<Guid> orderIds,
                                            DateTime startDate,
                                            DateTime endDate
                                   )
        {
            return ageGroupStatsDataStore.GetListByOrderIds(
                                                orderIds,
                                                startDate,
                                                endDate
                                          );
        }
        #endregion
    }
}
