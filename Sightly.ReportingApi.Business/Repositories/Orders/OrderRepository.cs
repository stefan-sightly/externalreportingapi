﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business workflow
    /// members related to order.
    /// </summary>
    public class OrderRepository : IOrderRepository
    {
        #region Fields
        private IAccountDataStore accountDataStore;
        private IApiSessionHelper apiSessionHelper;
        private IOrderDataStore orderDataStore;
        #endregion

        #region Constructor
        public OrderRepository(
                   IAccountDataStore accountDataStore,
                   IApiSessionHelper apiSessionHelper,
                   IOrderDataStore orderDataStore
               )
        {
            this.accountDataStore = accountDataStore;
            this.apiSessionHelper = apiSessionHelper;
            this.orderDataStore = orderDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the target population
        /// on a specific order.
        /// </summary>
        /// <param name="orderId">
        ///     Order ID
        /// </param>
        /// <returns>
        ///     Target population
        /// </returns>
        public long? GetTargetPopulation(Guid orderId)
        {
            return orderDataStore.GetTargetPopulation(orderId);
        }

        /// <summary>
        /// Method used for retrieving orders under a
        /// given request.
        /// </summary>
        /// <param name="campaignOrdersRequest">
        ///     Object that holds filtering values that will 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     List of orders.
        /// </returns>
        public List<Order> GetCampaignOrders(CampaignOrdersRequest campaignOrdersRequest)
        {
            return orderDataStore.GetCampaignOrders(campaignOrdersRequest);
        }

        /// <summary>
        /// Method used for retrieving order reference
        /// codes together with their order ID
        /// under the current user's account.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <returns>
        ///     List of order reference codes together 
        ///     with their order ID.
        /// </returns>
        public List<OrderRefCodeOption> GetReferenceCodeList(Guid apiSessionId)
        {
            Guid accountId = apiSessionHelper.GetUser(apiSessionId).AccountId;
            var subAccountIds = accountDataStore.GetSubAccountIds(accountId, apiSessionId);

            return orderDataStore.GetReferenceCodeList(accountId, subAccountIds, apiSessionId);
        }

        public List<OrderContext> GetOrderContextBySessionId(Guid sessionId)
        {
            return orderDataStore.GetOrderContextBySessionId(sessionId);
        }

        public OrderFlightData GetOrderFlightDataByOrderId(Guid orderId)
        {
            return orderDataStore.GetOrderFlightDataByOrderId(orderId);
        }

        #endregion
    }
}