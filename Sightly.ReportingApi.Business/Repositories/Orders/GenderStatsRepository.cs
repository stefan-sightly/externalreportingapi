﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to gender stats.
    /// </summary>
    public class GenderStatsRepository : IGenderStatsRepository
    {
        #region Fields
        private IGenderStatsDataStore genderStatsDataStore;
        #endregion

        #region Constructor
        public GenderStatsRepository(IGenderStatsDataStore genderStatsDataStore)
        {
            this.genderStatsDataStore = genderStatsDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all gender 
        /// stats under an order record.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the related order ID record.
        /// </param>
        /// <returns>
        ///     List of all gender stats under the
        ///     order record.
        /// </returns>
        public List<GenderStats> GetListByParentId(Guid orderId)
        {
            return genderStatsDataStore.GetListByParentId(orderId);
        }

        /// <summary>
        /// Method that used to retrieves aggregated gender
        /// stats under the specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id of the aggregated gender stats record.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats record.
        /// </returns>
        public List<GenderStats> GetListByCampaignId(Guid campaignId)
        {
            return genderStatsDataStore.GetListByCampaignId(campaignId);
        }

        /// <summary>
        /// Method used to retrieve aggregated gender stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the gender stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the gender stats.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<GenderStats> GetListByOrderIds(
                                        List<Guid> orderIds,
                                        DateTime startDate,
                                        DateTime endDate
                                 )
        {
            return genderStatsDataStore.GetListByOrderIds(
                                                orderIds,
                                                startDate,
                                                endDate
                                        );
        }
        #endregion
    }
}