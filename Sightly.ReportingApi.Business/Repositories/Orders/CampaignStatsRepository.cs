﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus;
using Sightly.ReportingApi.Business.Interfaces.ServiceAgents.ServiceBus.CampaignStatsReportGenerator;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to campaign stats.
    /// </summary>
    public class CampaignStatsRepository : ICampaignStatsRepository
    {
        #region Fields
        private readonly IApiSessionHelper apiSessionHelper;
        private ICampaignStatsDataStore campaignStatsDataStore;
        private IExecutivePerformanceReportSenderAgent executivePerformanceReportSenderAgent;
        private IBudgetPacingReportSenderAgent budgetPacingReportSenderAgent;
        private ICampaignPerformanceReportSenderAgent campaignPerformanceSenderAgent;
        #endregion

        #region Constructors
        public CampaignStatsRepository(
                   IApiSessionHelper apiSessionHelper,
                   ICampaignStatsDataStore campaignStatsDataStore,
                   IExecutivePerformanceReportSenderAgent executivePerformanceReportSenderAgent,
                   IBudgetPacingReportSenderAgent budgetPacingReportSenderAgent,
                   ICampaignPerformanceReportSenderAgent campaignPerformanceSenderAgent
               )
        {
            this.apiSessionHelper = apiSessionHelper;
            this.campaignStatsDataStore = campaignStatsDataStore;
            this.executivePerformanceReportSenderAgent = executivePerformanceReportSenderAgent;
            this.budgetPacingReportSenderAgent = budgetPacingReportSenderAgent;
            this.campaignPerformanceSenderAgent = campaignPerformanceSenderAgent;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that retrieves aggregated campaign stats.
        /// </summary>
        /// <param name="aggregatedCampaignStatsRequest">
        ///     Request object that is needed to retrieve records.
        /// </param>
        /// <returns>
        ///     Aggregated campaign stats under a campaign.
        /// </returns>
        public CampaignStats GetAggregatedCampaignStats(AggregatedCampaignStatRequest aggregatedCampaignStatsRequest)
        {
            return campaignStatsDataStore.GetAggregatedCampaignStats(aggregatedCampaignStatsRequest);
        }

        /// <summary>
        /// Method that retrieves budget pacing summary records.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes budget pacing records.
        /// </returns>
        public SubsetRequestResult<CampaignStats> GetBudgetPacingCampaignStats(
                                                Guid apiSessionId,
                                                CampaignStatsRequest campaignStatsRequest,
                                                SubsetRequest targetSubset
                                           )
        {
            var user = apiSessionHelper.GetUser(apiSessionId);

            return campaignStatsDataStore.GetBudgetPacingCampaignStats(campaignStatsRequest, targetSubset, user);
        }

        /// <summary>
        /// Method that retrieves campaign executive performance
        /// summary of the specified campaign stats.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the order where to get campaign stats from.
        /// </param>
        /// <returns>
        ///     Campaign executive performance summary of the
        ///     specified order.
        /// </returns>
        public CampaignStats GetCampaignExecutivePerformanceById(Guid orderId)
        {
            return campaignStatsDataStore.GetCampaignExecutivePerformanceById(orderId);
        }

        /// <summary>
        /// Method that retrieves executive performance summary records.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes executive performance summary records.
        /// </returns>
        public SubsetRequestResult<CampaignStats> GetExecutivePerformanceCampaignStats(
                                                        Guid apiSessionId,
                                                        CampaignStatsRequest campaignStatsRequest,
                                                        SubsetRequest targetSubset
                                                   )
        {
            var user = apiSessionHelper.GetUser(apiSessionId);

            return campaignStatsDataStore.GetExecutivePerformanceCampaignStats(campaignStatsRequest, targetSubset, user, apiSessionId);
        }

        /// <summary>
        /// Method used for sending executive 
        /// performance reports.
        /// </summary>
        /// <param name="apiSessionId">
        ///     The session id of the logged in user that 
        ///     sends the request to generate the report
        /// </param>
        /// <param name="request">
        ///     Command that contains information regarding
        ///     the executive performance report that would 
        ///     be sent to end user.
        /// </param>
        public void SendExecutivePeformanceReportRequest(Guid apiSessionId, ExecutivePerformanceReportRequest request)
        {
            User user = apiSessionHelper.GetUser(apiSessionId);
            executivePerformanceReportSenderAgent.SendGenerateReportRequest(user, request);
        }

        /// <summary>
        /// Method used for sending budget pacing reports.
        /// </summary>
        /// <param name="apiSessionId">
        ///     The session id of the logged in user that 
        ///     sends the request to generate the report
        /// </param>
        /// <param name="request">
        ///     Command that contains information regarding
        ///     the budget pacing report that would 
        ///     be sent to end user.
        /// </param>
        public void SendBudgetPacingReportRequest(Guid apiSessionId, BudgetPacingReportRequest request)
        {
            User user = apiSessionHelper.GetUser(apiSessionId);
            budgetPacingReportSenderAgent.SendGenerateReportRequest(user, request);
        }

        /// <summary>
        /// Method used for sending campaign
        /// performance reports.
        /// </summary>
        /// <param name="apiSessionId">
        ///     API session ID.
        /// </param>
        /// <param name="request">
        ///     Object that contains information regarding
        ///     the campaign performance report that would
        ///     be sent to end user.
        /// </param>
        public void SendCampaignPerformanceReportRequest(Guid apiSessionId, CampaignPerformanceReportRequest request)
        {
            User user = apiSessionHelper.GetUser(apiSessionId);
            campaignPerformanceSenderAgent.SendGenerateReportRequest(user, request);
        }
        #endregion
    }
}
