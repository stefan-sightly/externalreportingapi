﻿using System;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    public class OrderStatsRepository: IOrderStatsRepository
    {

        private readonly IApiSessionHelper _apiSessionHelper;
        private readonly IOrderStatsDataStore _orderStatsDataStore;

        public OrderStatsRepository(IApiSessionHelper apiSessionHelper, IOrderStatsDataStore orderStatsDataStore)
        {
            _apiSessionHelper = apiSessionHelper;
            _orderStatsDataStore = orderStatsDataStore;
        }

        public SubsetRequestResult<OrderStats> GetExecutivePerformanceOrderStats(
                                                    Guid apiSessionId, 
                                                    OrderStatsRequest orderStatsRequest,
                                                    SubsetRequest targetSubset)
        {
            var user = _apiSessionHelper.GetUser(apiSessionId);
            return _orderStatsDataStore.GetExecutivePerformanceOrderStats(orderStatsRequest, targetSubset, user);
        }
    }
}
