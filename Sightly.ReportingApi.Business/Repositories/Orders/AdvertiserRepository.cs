﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to advertiser.
    /// </summary>
    public class AdvertiserRepository : IAdvertiserRepository
    {
        #region Fields
        private IAccountDataStore accountDataStore;
        private IAdvertiserDataStore advertiserDataStore;
        private IApiSessionHelper sessionHelper;
        #endregion

        #region Constructor
        public AdvertiserRepository(
                    IAccountDataStore accountDataStore,
                    IAdvertiserDataStore advertiserDataStore,
                    IApiSessionHelper sessionHelper
               )
        {
            this.accountDataStore = accountDataStore;
            this.advertiserDataStore = advertiserDataStore;
            this.sessionHelper = sessionHelper;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all advertisers under 
        /// the user's account and its sub-account. 
        /// </summary>
        /// <param name="apiSessionId">
        ///     Session id of the user that owns the advertisers 
        ///     that would be retrieved.
        /// </param>
        /// <returns>List of advertisers under the provided session id.</returns>
        public List<Advertiser> GetAdvertiserList(Guid apiSessionId)
        {
            var user = sessionHelper.GetUser(apiSessionId);
            var subAccountIds = accountDataStore.GetSubAccountIds(user.AccountId, apiSessionId);

            return advertiserDataStore.GetAdvertiserList(user.AccountId, subAccountIds, apiSessionId);
        }

        public List<AdvertiserRefCode> GetAdvertiserRefCodeListByAccountRefCode(string accountRefCode, Guid apiSessionId)
        {
            return advertiserDataStore.GetAdvertiserRefCodeListByAccountRefCode(accountRefCode, apiSessionId);
        }

        #endregion
    }
}
