﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to device stats.
    /// </summary>
    public class DeviceStatsRepository : IDeviceStatsRepository
    {
        #region Fields
        private IDeviceStatsDataStore deviceStatsDataStore;
        #endregion

        #region Constructors
        public DeviceStatsRepository(IDeviceStatsDataStore deviceStatsDataStore)
        {
            this.deviceStatsDataStore = deviceStatsDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all device stats
        /// under an order record.
        /// </summary>
        /// <param name="orderId">
        ///     ID of order that owns the target
        ///     device-stats.
        /// </param>
        /// <returns>
        ///     List of device stats records under the
        ///     provided order.
        public List<DeviceStats> GetListByParentId(Guid orderId)
        {
            return deviceStatsDataStore.GetListByParentId(orderId);
        }

        /// <summary>
        /// Method used for retrieving aggregated device
        /// stats of a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats.
        /// </returns>
        public List<DeviceStats> GetListByCampaignId(Guid campaignId)
        {
            return deviceStatsDataStore.GetListByCampaignId(campaignId);
        }

        /// <summary>
        /// Method used to retrieve aggregated device stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter device stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter device stats.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<DeviceStats> GetListByOrderIds(
                                     List<Guid> orderIds,
                                     DateTime startDate,
                                     DateTime endDate
                                 )
        {
            return deviceStatsDataStore.GetListByOrderIds(
                                            orderIds,
                                            startDate,
                                            endDate
                                        );
        }

        public List<DeviceStats> GetListByCampaignAndDates(Guid campaignId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return deviceStatsDataStore.GetDeviceStatsByCampaignAndDate(
                campaignId, 
                startDate, 
                endDate, 
                apiSessionId);
        }

        public List<DeviceStats> GetDeviceStatsByOrderAndDates(Guid orderId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return deviceStatsDataStore.GetDeviceStatsByOrderAndDates(
                orderId,
                startDate,
                endDate,
                apiSessionId);
        }

        #endregion
    }
}