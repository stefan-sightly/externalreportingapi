﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    public class PlacementStatsRepository :IPlacementStatsRepository
    {
        private IAdvertiserRepository advertiserRepository;
        private IPlacementStatsDataStore placementStatsDataStore;

        public PlacementStatsRepository(IAdvertiserRepository advertiserRepository, IPlacementStatsDataStore placementStatsDataStore)
        {
            this.advertiserRepository = advertiserRepository;
            this.placementStatsDataStore = placementStatsDataStore;
        }

        public List<Placement> GetKnownPlacementsBySession(Guid sessionId)
        {
            var advertisers = advertiserRepository.GetAdvertiserList(sessionId);
            var returningList = new List<Placement>();
            advertisers.ForEach(advertiser =>
            {
                returningList.AddRange(placementStatsDataStore.GetKnownPlacementsByAdvertiser(advertiser.AdvertiserId)); 
            });
            return returningList;
        }

        public List<Placement> GetKnownPlacementsByAdvertiserRefCode(Guid sessionId, string advertiserRefCode)
        {
            var advertisers = advertiserRepository.GetAdvertiserList(sessionId);
            var returningList = new List<Placement>();
            advertisers.Where(adv => adv.AdvertiserRefCode == advertiserRefCode).ToList().ForEach(advertiser =>
            {
                returningList.AddRange(placementStatsDataStore.GetKnownPlacementsByAdvertiser(advertiser.AdvertiserId));
            });
            return returningList;
        }

        public List<PlacementStats> GetPlacementStatsAggregatedByDates(string placement, DateTime startDate, DateTime endDate)
        {
            return placementStatsDataStore.GetPlacementStatsAggregatedByDates(placement, startDate, endDate);
        }

        public List<PlacementStatsWithDate> GetPlacementStatsDailyAggregatedByDates(string placement, DateTime startDate, DateTime endDate)
        {
            return placementStatsDataStore.GetPlacementStatsDailyAggregatedByDates(placement, startDate, endDate);
        }

        public List<PlacementStats> GetAllStatsByRefCodeAndDates(string advertiserRefCode, DateTime startDate, DateTime endDate)
        {
            return placementStatsDataStore.GetAllStatsByRefCodeAndDates(advertiserRefCode, startDate, endDate);
        }

        public List<PlacementStatsWithDate> GetAllDailyStatsByRefCodeAndDates(string advertiserRefCode, DateTime startDate, DateTime endDate)
        {
            return placementStatsDataStore.GetAllDailyStatsByRefCodeAndDates(advertiserRefCode, startDate, endDate);
        }

        public List<AdvertiserPlacementStatsWithDate> GetDailyPlacementStatsByAccountRefCodeAndDates(string accountRefCode, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return placementStatsDataStore.GetDailyPlacementStatsByAccountRefCodeAndDates(accountRefCode, startDate, endDate, apiSessionId);
        }

        public ExtendedPlacementStats GetExtendedStatsAggregatedByPlacementAndDate(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return placementStatsDataStore.GetExtendedStatsAggregatedByPlacementAndDate(placementId, startDate, endDate, apiSessionId);
        }

        public List<ExtendedPlacementStats> GetExtendedStatsDailyByPlacementAndDate(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return placementStatsDataStore.GetExtendedStatsDailyByPlacementAndDate(placementId, startDate, endDate, apiSessionId);
        }

        public List<DeviceStatsByPlacement> GetAggregatedDeviceStatsByPlacementAndDates(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return placementStatsDataStore.GetAggregatedDeviceStatsByPlacementAndDates(placementId, startDate, endDate, apiSessionId);
        }

        public List<DeviceStatsByPlacement> GetDailyDeviceStatsByPlacementAndDates(string placementId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return placementStatsDataStore.GetDailyDeviceStatsByPlacementAndDates(placementId, startDate, endDate, apiSessionId);
        }

        public List<PlacementWithExtendedStats> GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(string placement, DateTime startDate, DateTime endDate,
            Guid apiSessionId)
        {
            return placementStatsDataStore.GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(placement, startDate, endDate, apiSessionId);
        }
    }
}