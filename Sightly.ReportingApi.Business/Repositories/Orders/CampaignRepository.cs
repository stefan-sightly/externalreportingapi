﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Orders;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.Business.Repositories.Orders
{
    /// <summary>
    /// Class that implements business work-flow
    /// members related to campaign.
    /// </summary>
    public class CampaignRepository : ICampaignRepository
    {
        #region Fields
        private ICampaignDataStore campaignTargetedPopulationDataStore;
        #endregion

        #region Constructor
        public CampaignRepository(ICampaignDataStore campaignTargetedPopulationDataStore)
        {
            this.campaignTargetedPopulationDataStore = campaignTargetedPopulationDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        ///     Method used for retrieving the campaign targeted
        ///     population on a specific request.
        /// </summary>
        /// <param name="request">
        ///     Object that holds filtering values that would 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     Campaign targeted population.
        /// </returns>
        public long? GetCampaignTargetedPopulation(CampaignTargetedPopulationRequest request)
        {
            return campaignTargetedPopulationDataStore.GetCampaignTargetedPopulation(request);
        }
        #endregion
    }
}