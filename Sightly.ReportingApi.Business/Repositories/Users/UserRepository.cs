﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Users;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Users;
using Sightly.ReportingApi.Business.Interfaces.Utilities.CryptographyServices;
using Sightly.ReportingApi.DomainModels.Accounts;

namespace Sightly.ReportingApi.Business.Repositories.Users
{
    /// <summary>
    /// Class that contains business workflows related to users
    /// </summary>
    public class UserRepository : IUserRepository
    {
        #region Fields
        private IHashService hashService;
        private IUserDataStore userDataStore;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the user repository
        /// </summary>
        /// <param name="hashService">Cryptographic hashing service utility</param>
        /// <param name="userDataStore">User data store</param>
        public UserRepository(
                   IHashService hashService,
                   IUserDataStore userDataStore
               )
        {
            this.userDataStore = userDataStore;
            this.hashService = hashService;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to get the authenticated user using the user's credentials
        /// </summary>
        /// <param name="user">
        /// User's credentials which includes username (email address) and password
        /// </param>
        /// <returns>Information of the authenticated user</returns>
        public User GetAuthenticatedUser(User user)
        {
            var isCredentialsValid = userDataStore.CheckIfValidCredential(user);

            if (isCredentialsValid)
            {
                var userInfo = userDataStore.GetByProperty(user.EmailAddress);
                var access = userInfo.UserId + hashService.GetSha256Hash(user.Password);
                var isPasswordValid = hashService.ValidateString(access, userInfo.Password);

                if (isPasswordValid)
                    return userInfo;
            }

            return null;
        }
        #endregion
    }
}
