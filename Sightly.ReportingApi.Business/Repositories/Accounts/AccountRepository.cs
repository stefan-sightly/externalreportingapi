﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Business.Repositories.Accounts
{
    /// <summary>
    /// Class that implements business work flow
    /// members related to account.
    /// </summary>
    public class AccountRepository : IAccountRepository
    {
        #region Fields
        private IAccountDataStore accountDataStore;
        private IApiSessionHelper sessionHelper;
        #endregion

        #region Constructor
        public AccountRepository(IAccountDataStore accountDataStore, IApiSessionHelper sessionHelper)
        {
            this.accountDataStore = accountDataStore;
            this.sessionHelper = sessionHelper;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all sub-accounts under an account.
        /// </summary>
        /// <param name="accountId">
        ///     Session id of the user that owns the sub-accounts
        ///     that would be retrieved.
        /// </param>
        /// <returns>List of accounts under an account.</returns>
        public List<Account> GetSubAccounts(Guid apiSessionId)
        {
            var user = sessionHelper.GetUser(apiSessionId);
            return accountDataStore.GetSubAccounts(user.AccountId, apiSessionId);
        }

        public List<AccountRefCode> GetSubaccountsByParentRefCode(Guid apiSessionId, string accountRefCode)
        {
            return accountDataStore.GetSubAccountsByParentRefCode(accountRefCode, apiSessionId);
        }

        #endregion
    }
}
