﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Repositories.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Utilities.Generators;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;

namespace Sightly.ReportingApi.Business.Repositories.Accounts
{
    /// <summary>
    /// Class that contains business workflows related to API session
    /// </summary>
    public class ApiSessionRepository : IApiSessionRepository
    {
        #region Fields
        private IApiSessionDataStore apiSessionDataStore;
        private IGuidGenerator guidGenerator;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the API session repository
        /// </summary>
        /// <param name="apiSessionDataStore">API session data store</param>
        /// <param name="guidGenerator">Unique identifier generator</param>
        public ApiSessionRepository(
                   IApiSessionDataStore apiSessionDataStore,
                   IGuidGenerator guidGenerator
               )
        {
            this.apiSessionDataStore = apiSessionDataStore;
            this.guidGenerator = guidGenerator;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to check if an API session is valid
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>True if the API session is valid; otherwise false</returns>
        public bool CheckIfValid(Guid apiSessionId)
        {
            return apiSessionDataStore.CheckIfValid(apiSessionId);
        }

        /// <summary>
        /// Method used to extend an API session
        /// </summary>
        /// <param name="session">API session ID</param>
        public void Extend(Guid apiSessionId)
        {
            apiSessionDataStore.Extend(apiSessionId);
        }

        /// <summary>
        /// Method used to create a session for the authenticated user
        /// </summary>
        /// <param name="session">API session information</param>
        public void Insert(ApiSession session)
        {
            session.ApiSessionId = guidGenerator.GenerateComb();
            apiSessionDataStore.Insert(session);
        }

        /// <summary>
        /// Method used to log out the API session of the current user
        /// </summary>
        /// <param name="session">API session information</param>
        public void Logout(ApiSession session)
        {
            apiSessionDataStore.Logout(session);
        }
        #endregion
    }
}
