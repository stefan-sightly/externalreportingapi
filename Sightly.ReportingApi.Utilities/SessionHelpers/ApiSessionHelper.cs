﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Users;
using Sightly.ReportingApi.Business.Interfaces.Utilities.SessionHelpers;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;

namespace Sightly.ReportingApi.Utilities.SessionHelpers
{
    /// <summary>
    /// Class that provides helper methods for the API session
    /// </summary>
    public class ApiSessionHelper : IApiSessionHelper
    {
        #region Fields
        private IApiSessionDataStore apiSessionDataStore;
        private IUserDataStore userDataStore;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor of the API session helper
        /// </summary>
        /// <param name="apiSessionDataStore">API session data store</param>
        /// <param name="userDataStore">User data store</param>
        public ApiSessionHelper(
                   IApiSessionDataStore apiSessionDataStore,
                   IUserDataStore userDataStore
               )
        {
            this.apiSessionDataStore = apiSessionDataStore;
            this.userDataStore = userDataStore;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to the retrieve the user of the specified API session ID
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>User information</returns>
        public User GetUser(Guid apiSessionId)
        {
            var userId = apiSessionDataStore.GetUserId(apiSessionId);

            return userDataStore.GetById(userId);
        }
        #endregion
    }
}
