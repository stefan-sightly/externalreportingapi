﻿using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using System.Configuration;

namespace Sightly.ReportingApi.Utilities.ConfigurationStores
{
    /// <summary>
    /// Class in charge of accessing configuration values stored in web.config
    /// </summary>
    public class WebConfigurationStore : IConfigurationStore
    {
        /// <summary>
        /// Method used for retrieval of key values
        /// </summary>
        /// <param name="key">Key name of configuration value to be retrieved</param>
        /// <returns>Value of key provided</returns>
        public string GetKeyValue(ConfigurationKey key)
        {
            return ConfigurationManager.AppSettings[key.ToString()];
        }
    }
}
