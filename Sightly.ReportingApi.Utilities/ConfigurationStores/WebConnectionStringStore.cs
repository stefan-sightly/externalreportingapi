﻿using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using System.Configuration;

namespace Sightly.ReportingApi.Utilities.ConfigurationStores
{
    /// <summary>
    /// Class in charge of accessing connection strings stored in web.config
    /// </summary>
    public class WebConnectionStringStore : IConnectionStringStore
    {
        /// <summary>
        /// Method used for fetching a connection string value using the provided key
        /// </summary>
        /// <param name="key">The key to the connection string value to be retrieved</param>
        /// <returns>The connection string value obtained using the connection string key</returns>
        public string GetKeyValue(ConnectionStringKey key)
        {
            return ConfigurationManager.ConnectionStrings[key.ToString()].ConnectionString;
        }
    }
}
