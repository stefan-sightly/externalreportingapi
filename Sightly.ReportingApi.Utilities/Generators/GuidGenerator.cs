﻿using Sightly.ReportingApi.Business.Interfaces.Utilities.Generators;
using System;

namespace Sightly.ReportingApi.Utilities.Generators
{
    /// <summary>
    /// Class in charge of creating unique identifiers or IDs
    /// </summary>
    public class GuidGenerator : IGuidGenerator
    {
        /// <summary>
        /// Method used to generate a unique identifier or ID
        /// </summary>
        /// <returns>Generated ID or unique identifier</returns>
        public Guid GenerateComb()
        {
            byte[] destinationArray = Guid.NewGuid().ToByteArray();
            DateTime time = new DateTime(0x76c, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan span = new TimeSpan(now.Ticks - time.Ticks);
            TimeSpan timeOfDay = now.TimeOfDay;
            byte[] bytes = BitConverter.GetBytes(span.Days);
            byte[] array = BitConverter.GetBytes((long)(timeOfDay.TotalMilliseconds / 3.333333));
            Array.Reverse(bytes);
            Array.Reverse(array);
            Array.Copy(bytes, bytes.Length - 2, destinationArray, destinationArray.Length - 6, 2);
            Array.Copy(array, array.Length - 4, destinationArray, destinationArray.Length - 4, 4);

            return new Guid(destinationArray);
        }

        /// <summary>
        /// Method used to generate multiple IDs or unique identifiers
        /// </summary>
        /// <param name="count">Number of IDs to generate</param>
        /// <returns>A collection of IDs or unique identifiers</returns>
        public Guid[] GenerateCombs(int count)
        {
            Guid[] ids = new Guid[count];

            for (int i = 0; i < count; i++)
                ids[i] = GenerateComb();

            return ids;
        }
    }
}
