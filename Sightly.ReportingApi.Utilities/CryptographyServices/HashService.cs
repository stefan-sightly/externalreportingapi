﻿using Sightly.ReportingApi.Business.Interfaces.Utilities.CryptographyServices;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Sightly.ReportingApi.Utilities.CryptographyServices
{
    /// <summary>
    /// Salted password hashing with PBKDF2-SHA1.
    /// Author: havoc AT defuse.ca
    /// www: http://crackstation.net/hashing-UserManagement.htm
    /// </summary>
    public class HashService : IHashService
    {
        #region Fields
        // The following constants may be changed without breaking existing hashes.
        private const int SaltBytes = 56;
        private const int HashBytes = 56;
        private const int Pbkdf2Iterations = 1199;

        private const int IterationIndex = 0;
        private const int SaltIndex = 1;
        private const int Pbkdf2Index = 2;
        #endregion

        #region Helper Methods
        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from 
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private bool SlowEquals(IList<byte> a, IList<byte> b)
        {
            uint diff = (uint)a.Count ^ (uint)b.Count;
            for (var i = 0; i < a.Count && i < b.Count; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        /// <summary>
        /// Computes the PBKDF2-SHA1 hash of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="iterations">The PBKDF2 iteration count.</param>
        /// <param name="outputBytes">The length of the hash to generate, in bytes.</param>
        /// <returns>A hash of the password.</returns>
        private byte[] Pbkdf2(string password, byte[] salt, int iterations, int outputBytes)
        {
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt) { IterationCount = iterations })
            {
                return pbkdf2.GetBytes(outputBytes);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates a salted PBKDF2 hash of the password.
        /// </summary>
        /// <param name="password">
        ///     The password to hash.
        /// </param>
        /// <returns>
        ///     The hash of the password.
        /// </returns>
        public string CreateHash(string password)
        {
            // Generate a random salt
            using (var csprng = new RNGCryptoServiceProvider())
            {
                var salt = new byte[SaltBytes];
                csprng.GetBytes(salt);
                // Hash the password and encode the parameters
                byte[] hash = Pbkdf2(password, salt, Pbkdf2Iterations, HashBytes);
                return string.Format("{0}:{1}:{2}", Pbkdf2Iterations, Convert.ToBase64String(salt), Convert.ToBase64String(hash));
            }
        }

        /// <summary>
        /// Method used for generating a SHA-256 hash of string values.
        /// </summary>
        /// <param name="input">
        ///     String value to hash.
        /// </param>
        /// <returns>
        ///     Hashed form of the input string.
        /// </returns>
        public string GetSha256Hash(string input)
        {
            var data = Encoding.UTF8.GetBytes(input);
            var encoder = SHA256.Create();
            var hash = encoder.ComputeHash(data);

            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="access">
        ///     Hashed password input to check
        /// </param>
        /// <param name="goodHash">
        ///     A hash of the correct password.
        /// </param>
        /// <returns>
        ///     True if the password is correct. False otherwise.
        /// </returns>
        public bool ValidateString(string access, string goodHash)
        {
            return GetSha256Hash(access) == goodHash;
        }
        #endregion
    }
}
