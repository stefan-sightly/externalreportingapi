﻿using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Users;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Users
{
    /// <summary>
    /// Class that contains the data access workflows related to user
    /// </summary>
    public class UserDataStore : DapperDataStore, IUserDataStore
    {
        #region Constructor
        /// <summary>
        /// Constructor of the user data store
        /// </summary>
        /// <param name="connectionStringStore">Configuration data store for connection strings</param>
        public UserDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore, ConnectionStringKey.UserManagement)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method used for checking if the user has a valid credentials
        /// </summary>
        /// <param name="user">User information</param>
        /// <returns>True: user has a valid credentials; False: user does not have a valid credentials</returns>
        public bool CheckIfValidCredential(User user)
        {
            return QueryScalar<bool>(
                "Users.UserCredential_ValidateByEmail",
                new
                {
                    Email = user.EmailAddress
                }
            );
        }

        /// <summary>
        /// Method used for retrieving a user by email
        /// </summary>
        /// <param name="email">Email address</param>
        /// <returns>User information</returns>
        public User GetByProperty(string email)
        {
            return QueryScalar<User>(
                    "Users.User_GetByEmail",
                    new
                    {
                        Email = email
                    }
            );
        }

        /// <summary>
        /// Method used for retrieving a user by User ID
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>User information</returns>
        public User GetById(Guid userId)
        {
            return QueryScalar<User>(
                    "Users.User_GetById",
                    new
                    {
                        UserId = userId
                    }
            );
        }

        /// <summary>
        /// Method used for fetching a list of users by User IDs
        /// </summary>
        /// <param name="userIds">IDs of users</param>
        /// <returns>List of users</returns>
        public List<User> GetListByIds(List<Guid> userIds)
        {
            return QueryList<User>(
                    "Users.UserList_GetByIds",
                    new
                    {
                        UserIds = string.Join(",", userIds)
                    }
            );
        }
        #endregion
    }
}
