﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to order.
    /// </summary>
    public class OrderDataStore : DapperDataStore, IOrderDataStore
    {
        #region Constructor
        public OrderDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the target population
        /// on a specific order.
        /// </summary>
        /// <param name="orderId">Order ID.</param>
        /// <returns>Target population.</returns>
        public long? GetTargetPopulation(Guid orderId)
        {
            return QueryScalar<long?>(
                    "Orders.OrderTargetPopulation_Get",
                    new
                    {
                        OrderId = orderId
                    }
                );
        }

        /// <summary>
        /// Method used for retrieving list of orders under a 
        /// given request.
        /// </summary>
        /// <param name="campaignOrdersRequest">
        ///     Object that holds filtering values that will 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     List of orders under the specified request.
        /// </returns>
        public List<Order> GetCampaignOrders(CampaignOrdersRequest campaignOrdersRequest)
        {
            return QueryList<Order>(
                       "Orders.CampaignOrders_Get",
                       new
                       {
                           CampaignId = campaignOrdersRequest.CampaignId,
                           EndDate = campaignOrdersRequest.EndDate,
                           StartDate = campaignOrdersRequest.StartDate,
                           OrderStatusIds = String.Join(",", campaignOrdersRequest.OrderStatusIds)
                       }
                   );
        }

        /// <summary>
        /// Method used for retrieving a list of order
        /// reference codes together with the order ID
        /// under the user's account and its 
        /// sub-account.
        /// </summary>
        /// <param name="accountId">
        ///     Account ID of the user.
        /// </param>
        /// <param name="subAccountIds">
        ///     Sub-account IDs based from the account 
        ///     ID of the user.
        /// </param>
        /// <returns>
        ///     List of order reference codes together 
        ///     with their order ID.
        /// </returns>
        public List<OrderRefCodeOption> GetReferenceCodeList(Guid accountId, List<Guid> subAccountIds, Guid apiSessionId)
        {
            return QueryList<OrderRefCodeOption>(
                       "Orders.OrderReferenceCodes_Get_WithSession",
                       new
                       {
                           AccountId = accountId,
                           SubAccountIds = String.Join(",", subAccountIds),
                           ApiSessionId = apiSessionId
                       }
                   );
        }

        public List<OrderContext> GetOrderContextBySessionId(Guid sessionId)
        {
            return QueryList<OrderContext>(
                "tvdb.GetOrderContextBySessionId",
                new
                {
                    SessionId = sessionId
                });
        }

        public OrderFlightData GetOrderFlightDataByOrderId(Guid orderId)
        {
            return QueryScalar<OrderFlightData>(
                "tvdb.OrderFlightDataByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        #endregion
    }
}