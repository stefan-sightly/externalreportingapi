﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to advertiser.
    /// </summary>
    public class AdvertiserDataStore : DapperDataStore, IAdvertiserDataStore
    {
        #region Constructor
        public AdvertiserDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all advertisers under 
        /// the user's account and its sub-account. 
        /// </summary>
        /// <param name="accountId">
        ///     Account id that owns the advertisers that 
        ///     would be retrieved.
        /// </param>
        /// <param name="subAccountIds">
        ///     List of sub-account ids that owns advertisers 
        ///     that would be included in the result set.
        /// </param>
        /// <returns>List of advertisers under the provided account ids.</returns>
        public List<Advertiser> GetAdvertiserList(Guid accountId, List<Guid> subAccountIds, Guid apiSessionId)
        {
            return QueryList<Advertiser>(
                        "Accounts.AdvertiserRefList_GetBySubAccountIds_WithSession",
                        new
                        {
                            AccountId = accountId,
                            SubAccountIds = String.Join(",", subAccountIds),
                            ApiSessionId = apiSessionId
                        }
                   );
        }

        public List<AdvertiserRefCode> GetAdvertiserRefCodeListByAccountRefCode(string accountRefCode, Guid apiSessionId)
        {
            return QueryList<AdvertiserRefCode>(
                        "Accounts.AdvertiserRefCodeList_GetByAccountRefCode_WithSession",
                        new
                        {
                            AccountRefCode = accountRefCode,
                            ApiSessionId = apiSessionId
                        }
            );
        }

        #endregion
    }
}
