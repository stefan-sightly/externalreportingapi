﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to gender stats.
    /// </summary>
    public class GenderStatsDataStore : DapperDataStore, IGenderStatsDataStore
    {
        #region Constructors
        public GenderStatsDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all gender 
        /// stats under an order record.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the related order ID record.
        /// </param>
        /// <returns>
        ///     List of all gender stats under the
        ///     order record.
        /// </returns>
        public List<GenderStats> GetListByParentId(Guid orderId)
        {
            return QueryList<GenderStats>(
                        "Orders.GenderStats_GetByOrderId",
                        new
                        {
                            OrderId = orderId
                        }
                   );
        }

        /// <summary>
        /// Method used for retrieving aggregated device
        /// stats under a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats.
        /// </returns>
        public List<GenderStats> GetListByCampaignId(Guid campaignId)
        {
            return QueryList<GenderStats>(
                        "Orders.AggregatedGenderStats_GetByCampaignId",
                        new
                        {
                            CampaignId = campaignId
                        }
                    );
        }

        /// <summary>
        /// Method used to retrieve aggregated gender stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the gender stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the gender stats.
        /// </param>
        /// <returns>
        ///     List of aggregated gender stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<GenderStats> GetListByOrderIds(
                                            List<Guid> orderIds,
                                            DateTime startDate,
                                            DateTime endDate
                                   )
        {
            return QueryList<GenderStats>(
                        "Orders.AggregatedGenderStats_GetByOrderIds",
                        new
                        {
                            OrderIds = string.Join(",", orderIds),
                            StartDate = startDate,
                            EndDate = endDate
                        }
                    );
        }
        #endregion
    }
}