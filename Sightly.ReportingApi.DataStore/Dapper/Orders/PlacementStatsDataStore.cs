﻿using System;
using System.Collections.Generic;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    public class PlacementStatsDataStore : DapperDataStore, IPlacementStatsDataStore
    {
        public PlacementStatsDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }

        public List<Placement> GetKnownPlacementsByAdvertiser(Guid advertiserId)
        {
            return QueryList<Placement>(
                "Orders.GetKnownPlacementsByAdvertiser",
                new {AdvertiserId = advertiserId}
            );
        }

        public List<PlacementStats> GetPlacementStatsAggregatedByDates(string placement, DateTime startDate,
            DateTime endDate)
        {
            return QueryList<PlacementStats>(
                "tvdb.GetPlacementStatsAggregatedByDates",
                new
                {
                    Placement = placement,
                    StartDate = startDate,
                    EndDate = endDate
                }
            );
        }

        public List<PlacementStatsWithDate> GetPlacementStatsDailyAggregatedByDates(string placement,
            DateTime startDate, DateTime endDate)
        {
            return QueryList<PlacementStatsWithDate>(
                "tvdb.GetPlacementStatsDailyAggregatedByDates",
                new
                {
                    Placement = placement,
                    StartDate = startDate,
                    EndDate = endDate
                }
            );
        }

        public List<PlacementStats> GetAllStatsByRefCodeAndDates(string advertiserRefCode, DateTime startDate,
            DateTime endDate)
        {
            return QueryList<PlacementStats>(
                "tvdb.GetAllPlacementAggregatedByRefCodeAndDate",
                new
                {
                    AdvertiserRefCode = advertiserRefCode,
                    StartDate = startDate,
                    EndDate = endDate
                }
            );
        }

        public List<PlacementStatsWithDate> GetAllDailyStatsByRefCodeAndDates(string advertiserRefCode,
            DateTime startDate, DateTime endDate)
        {
            return QueryList<PlacementStatsWithDate>(
                "tvdb.GetAllPlacementStatsDailyAggregatedByRefCodeAndDates",
                new
                {
                    AdvertiserRefCode = advertiserRefCode,
                    StartDate = startDate,
                    EndDate = endDate
                }
            );
        }

        public List<AdvertiserPlacementStatsWithDate> GetDailyPlacementStatsByAccountRefCodeAndDates(
            string accountRefCode, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<AdvertiserPlacementStatsWithDate>(
                "tvdb.GetDailyPlacementStatsByAccountRefCodeAndDates_WithSession",
                new
                {
                    AccountRefCode = accountRefCode,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                }
            );
        }

        public ExtendedPlacementStats GetExtendedStatsAggregatedByPlacementAndDate(string placementId,
            DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryScalar<ExtendedPlacementStats>(
                "[raw].[ExtendedStatsAggregatedByPlacementAndDate]",
                new
                {
                    PlacementValue = placementId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }

        public List<ExtendedPlacementStats> GetExtendedStatsDailyByPlacementAndDate(string placementId,
            DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<ExtendedPlacementStats>(
                "[raw].[ExtenedStatsDailyByPlacementAndDate_WithSession]",
                new
                {
                    PlacementValue = placementId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }

        public List<DeviceStatsByPlacement> GetAggregatedDeviceStatsByPlacementAndDates(string placementId,
            DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<DeviceStatsByPlacement>(
                "[tvdb].[GetAggregatedDeviceStatsByPlacementAndDates]",
                new
                {
                    PlacementValue = placementId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }

        public List<DeviceStatsByPlacement> GetDailyDeviceStatsByPlacementAndDates(string placementId,
            DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<DeviceStatsByPlacement>(
                "[tvdb].[GetDailyDeviceStatsByPlacementAndDates]",
                new
                {
                    PlacementValue = placementId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }

        public List<PlacementWithExtendedStats> GetDailyPlusExtendedStatsByPlacementAndDate_WithSession(string placement, DateTime startDate, DateTime endDate,
            Guid apiSessionId)
        {
            return QueryList<PlacementWithExtendedStats>(
                "[tvdb].[DailyPlusExtendedStatsByPlacementAndDate_WithSession]",
                new
                {
                    PlacementValue = placement,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }
    }
}