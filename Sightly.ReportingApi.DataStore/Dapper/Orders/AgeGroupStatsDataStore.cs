﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to age group stats.
    /// </summary>
    public class AgeGroupStatsDataStore : DapperDataStore, IAgeGroupStatsDataStore
    {
        #region Constructor
        public AgeGroupStatsDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of
        /// age group stats under the specified
        /// order.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the related order record.
        /// </param>
        /// <returns>
        ///     List of age group stats under the 
        ///     specified order record.
        /// </returns>
        public List<AgeGroupStats> GetListByParentId(Guid orderId)
        {
            return QueryList<AgeGroupStats>(
                        "Orders.AgeGroupStats_GetByOrderId",
                        new
                        {
                            OrderId = orderId
                        }
                   );
        }

        /// <summary>
        /// Method used for retrieving aggregated age group
        /// stats under a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats.
        /// </returns>
        public List<AgeGroupStats> GetListByCampaignId(Guid campaignId)
        {
            return QueryList<AgeGroupStats>(
                        "Orders.AggregatedAgeGroupStats_GetByCampaignId",
                        new
                        {
                            CampaignId = campaignId
                        }
                    );
        }

        /// <summary>
        /// Method used to retrieve aggregated age group stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date of the filter for the age group stats.
        /// </param>
        /// <param name="endDate">
        ///     End date of the filter for the age group stats.
        /// </param>
        /// <returns>
        ///     List of aggregated age group stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<AgeGroupStats> GetListByOrderIds(
                                            List<Guid> orderIds,
                                            DateTime startDate,
                                            DateTime endDate
                                   )
        {
            return QueryList<AgeGroupStats>(
                        "Orders.AggregatedAgeGroupStats_GetByOrderIds",
                        new
                        {
                            OrderIds = string.Join(",", orderIds),
                            StartDate = startDate,
                            EndDate = endDate
                        }
                    );
        }
        #endregion
    }
}