﻿using Dapper;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Data;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to campaign stats.
    /// </summary>
    public class CampaignStatsDataStore : DapperDataStore, ICampaignStatsDataStore
    {
        #region Constructors
        public CampaignStatsDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of aggregated
        /// campaign stats records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Object request that holds filtering values
        ///     that would be used for querying data.
        /// </param>
        /// <returns>
        ///     Aggregated campaign stats under a campaign.
        /// </returns>
        public CampaignStats GetAggregatedCampaignStats(AggregatedCampaignStatRequest campaignStatsRequest)
        {
            return QueryScalar<CampaignStats>(
                "Orders.AggregatedOrderStats_GetByCampaignId",
                new
                {
                    CampaignId = campaignStatsRequest.CampaignId,
                    StartDate = campaignStatsRequest.StartDate,
                    EndDate = campaignStatsRequest.EndDate,
                    OrderStatusIds = String.Join(",", campaignStatsRequest.OrderStatusIds)
                }
            );
        }

        /// <summary>
        /// Method that retrieves budget pacing summary records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="user">
        ///     User that requested the budget pacing summary records.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes budget pacing records.
        /// </returns>
        public SubsetRequestResult<CampaignStats> GetBudgetPacingCampaignStats(
                                                CampaignStatsRequest campaignStatsRequest,
                                                SubsetRequest targetSubset,
                                                User user
                                           )
        {
            var subsetRequestResult = new SubsetRequestResult<CampaignStats>();
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "StartDate", campaignStatsRequest.StartDate);
            AddInputParameter(parameters, "EndDate", campaignStatsRequest.EndDate);
            AddInputParameter(parameters, "SearchTerm", campaignStatsRequest.SearchTerm);
            AddInputParameter(parameters, "AdvertiserId", campaignStatsRequest.AdvertiserId);
            AddInputParameter(parameters, "AccountId", campaignStatsRequest.AccountId);
            AddInputParameter(parameters, "SubAccountId", campaignStatsRequest.SubAccountId);
            AddInputParameter(parameters, "OrderStatusIds", String.Join(",", campaignStatsRequest.OrderStatusIds));
            AddInputParameter(parameters, "SortDirection", targetSubset.SortDirection);
            AddInputParameter(parameters, "SortField", targetSubset.SortField);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);

            subsetRequestResult.RequestedSubset = QueryList<CampaignStats>("Orders.BudgetPacingSummary_Search", parameters);
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }

        /// <summary>
        /// Method that retrieves campaign executive performance
        /// summary of the specified order.
        /// </summary>
        /// <param name="orderId">
        ///     ID of the order where to get campaign stats from.
        /// </param>
        /// <returns>
        ///     Campaign executive performance summary of the
        ///     specified order.
        /// </returns>
        public CampaignStats GetCampaignExecutivePerformanceById(Guid orderId)
        {
            return QueryScalar<CampaignStats>(
                       "Orders.OrderPerformanceSummary_GetByOrderId",
                       new
                       {
                           OrderId = orderId
                       }
                   );
        }

        /// <summary>
        /// Method that retrieves executive performance summary records.
        /// </summary>
        /// <param name="campaignStatsRequest">
        ///     Campaign stats request object that holds filtering 
        ///     values that would be used for querying data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine 
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <param name="user">
        ///     User that requested the executive performance 
        ///     summary records.
        /// </param>
        /// <returns>
        /// List campaign stats instances that describes executive performance summary records.
        /// </returns>
        public SubsetRequestResult<CampaignStats> GetExecutivePerformanceCampaignStats(
                                                        CampaignStatsRequest campaignStatsRequest,
                                                        SubsetRequest targetSubset,
                                                        User user,
                                                        Guid apiSessionId
                                                  )
        {
            var subsetRequestResult = new SubsetRequestResult<CampaignStats>();
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "StartDate", campaignStatsRequest.StartDate);
            AddInputParameter(parameters, "EndDate", campaignStatsRequest.EndDate);
            AddInputParameter(parameters, "SearchTerm", campaignStatsRequest.SearchTerm);
            AddInputParameter(parameters, "AdvertiserId", campaignStatsRequest.AdvertiserId);
            AddInputParameter(parameters, "AccountId", campaignStatsRequest.AccountId);
            AddInputParameter(parameters, "SubAccountId", campaignStatsRequest.SubAccountId);
            AddInputParameter(parameters, "OrderStatusIds", String.Join(",", campaignStatsRequest.OrderStatusIds));
            AddInputParameter(parameters, "SortDirection", targetSubset.SortDirection);
            AddInputParameter(parameters, "SortField", targetSubset.SortField);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);
            AddInputParameter(parameters, "ApiSessionId", apiSessionId);

            subsetRequestResult.RequestedSubset = QueryList<CampaignStats>("tvdb.OrderPerformanceSummary_Search_WithSession", parameters);
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }
        #endregion
    }
}