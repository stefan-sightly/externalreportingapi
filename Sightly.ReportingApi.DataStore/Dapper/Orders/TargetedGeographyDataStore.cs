﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to targeted geography.
    /// </summary>
    public class TargetedGeographyDataStore : DapperDataStore, ITargetedGeographyDataStore
    {
        #region Constructor
        public TargetedGeographyDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        ///     Method used for retrieving list of targeted geographies
        ///     under a campaign id.
        /// </summary>
        /// <param name="campaignTargetedGeographieRequest">
        ///     Object that holds the filtering of values that 
        ///     would be used for querying data.
        /// </param>
        /// <returns>
        ///     List of targeted geographies under the specified
        ///     campaign id.
        /// </returns>
        public List<CampaignTargetedGeography> GetCampaignTargetedGeographies(CampaignTargetedGeographyRequest campaignTargetedGeographieRequest)
        {
            return QueryList<CampaignTargetedGeography>(
                       "Orders.CampaignTargetedGeographies_Get",
                       new
                       {
                           CampaignId = campaignTargetedGeographieRequest.CampaignId,
                           StartDate = campaignTargetedGeographieRequest.StartDate,
                           EndDate = campaignTargetedGeographieRequest.EndDate,
                           OrderStatusIds = String.Join(",", campaignTargetedGeographieRequest.OrderStatusIds)
                       }
                   );
        }
        #endregion
    }
}