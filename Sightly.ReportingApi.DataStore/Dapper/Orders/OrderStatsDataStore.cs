﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    public class OrderStatsDataStore : DapperDataStore, IOrderStatsDataStore
    {
        public OrderStatsDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }


        public SubsetRequestResult<OrderStats> GetExecutivePerformanceOrderStats(
                                                    OrderStatsRequest orderStatsRequest,
                                                    SubsetRequest targetSubset, 
                                                    User user)
        {
            var subsetRequestResult = new SubsetRequestResult<OrderStats>();
            var parameters = new DynamicParameters();
            AddInputParameter(parameters, "StartDate", orderStatsRequest.StartDate);
            AddInputParameter(parameters, "EndDate", orderStatsRequest.EndDate);
            AddInputParameter(parameters, "AccountId", orderStatsRequest.AccountId);
            AddInputParameter(parameters, "SortDirection", targetSubset.SortDirection);
            AddInputParameter(parameters, "SortField", targetSubset.SortField);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);

            subsetRequestResult.RequestedSubset = QueryList<OrderStats>("Orders.OrderPerformanceSummary_AccountAndDate", parameters);
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }
    }

}
