﻿using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to device stats.
    /// </summary>
    public class DeviceStatsDataStore : DapperDataStore, IDeviceStatsDataStore
    {
        #region Constructors
        public DeviceStatsDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving all device 
        /// stats under an order record.
        /// </summary>
        /// <param name="orderId">
        ///     ID of parent order record.
        /// </param>
        /// <returns>
        ///     List of all device stats under the
        ///     order record.
        /// </returns>
        public List<DeviceStats> GetListByParentId(Guid orderId)
        {
            return QueryList<DeviceStats>(
                        "Orders.DeviceStats_GetByOrderId",
                        new
                        {
                            OrderId = orderId
                        }
                   );
        }

        /// <summary>
        /// Method used for retrieving all aggregated device 
        /// under a given campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id that will retrieve records.
        /// </param>
        /// <returns>
        ///     List of all aggregated device stats under the
        ///     campaign id given.
        /// </returns>
        public List<DeviceStats> GetListByCampaignId(Guid campaignId)
        {
            return QueryList<DeviceStats>(
                        "Orders.AggregatedDeviceStats_GetByCampaignId",
                        new
                        {
                            CampaignId = campaignId
                        }
                    );
        }

        /// <summary>
        /// Method used to retrieve aggregated device stats
        /// using the provided order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     ID of orders where stats will be fetched from.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter device stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter device stats.
        /// </param>
        /// <returns>
        ///     List of aggregated device stats based on
        ///     the specified order IDs.
        /// </returns>
        public List<DeviceStats> GetListByOrderIds(
                                     List<Guid> orderIds,
                                     DateTime startDate,
                                     DateTime endDate
                                 )
        {
            return QueryList<DeviceStats>(
                        "Orders.AggregatedDeviceStats_GetByOrderIds",
                        new
                        {
                            OrderIds = string.Join(",", orderIds),
                            StartDate = startDate,
                            EndDate = endDate
                        }
                    );
        }

        public List<DeviceStats> GetDeviceStatsByCampaignAndDate(Guid campaignId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<DeviceStats>(
                "tvdb.DeviceStats_GetByCampaignId_WithSession",
                new
                {
                    CampaignId = campaignId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                }
            );
        }

        public List<DeviceStats> GetDeviceStatsByOrderAndDates(Guid orderId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            return QueryList<DeviceStats>(
                "tvdb.DeviceStats_GetByOrderId_WithSession",
                new
                {
                    OrderId = orderId,
                    StartDate = startDate,
                    EndDate = endDate,
                    ApiSessionId = apiSessionId
                });
        }

        #endregion
    }
}