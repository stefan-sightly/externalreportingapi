﻿using Dapper;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.Core.DataManagement;
using Sightly.ReportingApi.DomainModels.Accounts;
using Sightly.ReportingApi.DomainModels.Orders;
using System;
using System.Collections.Generic;
using System.Data;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to ad stats.
    /// </summary>
    public class AdStatsDataStore : DapperDataStore, IAdStatsDataStore
    {
        #region Constructors
        public AdStatsDataStore(IConnectionStringStore connectionStringStore) :
            base(connectionStringStore)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method that retrieves ad stats data.
        /// </summary>
        /// <param name="orderId">
        ///     Order ID is used for filtering 
        ///     ad stats data.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used 
        ///     to determine which subset would be 
        ///     retrieved from the database.
        /// </param>
        /// <param name="user">
        ///     User that requested the ad stats records.
        /// </param>
        /// <returns>
        ///     List ad stats instances for Campaign/Order 
        ///     Performance.
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStats(Guid orderId, SubsetRequest targetSubset, User user)
        {
            var subsetRequestResult = new SubsetRequestResult<AdStats>();
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "OrderId", orderId);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);

            subsetRequestResult.RequestedSubset = QueryList<AdStats>("Orders.AdStats_Get", parameters);
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }

        /// <summary>
        /// Method used for retrieving ad stats under
        /// a specified campaign id.
        /// </summary>
        /// <param name="campaignId">
        ///     Campaign id.
        /// </param>
        /// <returns>
        ///     List of ad stats retrieved by campaign id.
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStatsByCampaignId(Guid campaignId, SubsetRequest targetSubset, Guid apiSessionId)
        {
            var subsetRequestResult = new SubsetRequestResult<AdStats>();
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "CampaignId", campaignId);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);
            AddInputParameter(parameters, "ApiSessionId", apiSessionId);

            subsetRequestResult.RequestedSubset = QueryList<AdStats>(
                                                                "Orders.AdStats_GetSubsetByCampaignId_WithSession",
                                                                parameters
                                                            );
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }

        /// <summary>
        /// Method used to retrieve ad stats by order IDs.
        /// </summary>
        /// <param name="orderIds">
        ///     Order IDs that is used for filtering
        ///     ad stats data.
        /// </param>
        /// <param name="startDate">
        ///     Start date used to filter ad stats.
        /// </param>
        /// <param name="endDate">
        ///     End date used to filter ad stats.
        /// </param>
        /// <param name="targetSubset">
        ///     Paging information that would be used to determine
        ///     which subset would be retrieved from the database.
        /// </param>
        /// <returns>
        ///     List of aggregated ad stats instances for
        ///     Campaign/Order Performance
        /// </returns>
        public SubsetRequestResult<AdStats> GetAdStatsByOrderIds(
                                                List<Guid> orderIds,
                                                DateTime startDate,
                                                DateTime endDate,
                                                SubsetRequest targetSubset
                                            )
        {
            var subsetRequestResult = new SubsetRequestResult<AdStats>();
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "OrderIds", string.Join(",", orderIds));
            AddInputParameter(parameters, "StartDate", startDate);
            AddInputParameter(parameters, "EndDate", endDate);
            AddInputParameter(parameters, "Count", targetSubset.Count);
            AddInputParameter(parameters, "Offset", targetSubset.Offset);
            AddOutputParameter(parameters, "TotalCount", DbType.Int32);

            subsetRequestResult.RequestedSubset = QueryList<AdStats>(
                                                      "Orders.AdStats_GetSubsetByOrderIds",
                                                      parameters
                                                  );
            subsetRequestResult.TotalCount = parameters.Get<int>("TotalCount");

            return subsetRequestResult;
        }


        public AdStatsAggregate GetAdStatByCampaignAndDate(Guid campaignId,
                                                DateTime startDate,
                                                DateTime endDate,
                                                Guid apiSessionId)
        {
            var parameters = new DynamicParameters();

            AddInputParameter(parameters, "CampaignId", campaignId);
            AddInputParameter(parameters, "StartDate", startDate);
            AddInputParameter(parameters, "EndDate", endDate);
            AddInputParameter(parameters, "ApiSessionId", apiSessionId);

            return QueryScalar<AdStatsAggregate>(
                                    "tvdb.AdStats_GetByCampaignAndDate_WithSession",
                                    parameters
                                    );
        }

        public AdStatsAggregate GetAdStatsByOrderAndDates(Guid orderId, DateTime startDate, DateTime endDate, Guid apiSessionId)
        {
            var parameters = new DynamicParameters();
            AddInputParameter(parameters, "OrderId", orderId);
            AddInputParameter(parameters, "StartDate", startDate);
            AddInputParameter(parameters, "EndDate", endDate);
            AddInputParameter(parameters, "ApiSessionId", apiSessionId);

            return QueryScalar<AdStatsAggregate>(
                                    "tvdb.AdStats_GetByOrderAndDate_WithSession",
                                    parameters
                                    );
        }

        #endregion
    }
}
