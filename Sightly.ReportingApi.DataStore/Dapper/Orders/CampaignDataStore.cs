﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Orders;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Orders;
using System;

namespace Sightly.ReportingApi.DataStore.Dapper.Orders
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to campaign.
    /// </summary>
    public class CampaignDataStore : DapperDataStore, ICampaignDataStore
    {
        #region Constructor
        public CampaignDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        ///     Method used for retrieving the campaign
        ///     targeted population on a specific campaign
        /// </summary>
        /// <param name="request">
        ///     Object that holds filtering values that would 
        ///     be used for querying data.
        /// </param>
        /// <returns>
        ///     Campaign targeted population.
        /// </returns>
        public long? GetCampaignTargetedPopulation(CampaignTargetedPopulationRequest request)
        {
            return QueryScalar<long?>(
                       "Orders.CampaignTargetPopulation_Get",
                       new
                       {
                           CampaignId = request.CampaignId,
                           EndDate = request.EndDate,
                           StartDate = request.StartDate,
                           OrderStatusIds = String.Join(",", request.OrderStatusIds)
                       }
                   );
        }
        #endregion
    }
}