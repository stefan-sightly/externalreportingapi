﻿using Dapper;
using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Sightly.ReportingApi.DomainModels.Orders;

namespace Sightly.ReportingApi.DataStore.Dapper
{
    /// <summary>
    /// Class that implements data-accessing helper methods that utilizes the Dapper library.
    /// </summary>
    public class DapperDataStore
    {
        #region Fields
        private ConnectionStringKey? connectionStringKey;
        private IConnectionStringStore connectionStringStore;
        #endregion

        #region Constructor
        /// <summary>
        /// Basic constructor of the data store that utilizes the Dapper library
        /// </summary>
        /// <param name="connectionStringStore">Connection string data store</param>
        public DapperDataStore(IConnectionStringStore connectionStringStore)
        {
            this.connectionStringStore = connectionStringStore;
        }

        /// <summary>
        /// Constructor of the Dapper data store that accepts a custom connection string key
        /// </summary>
        /// <param name="connectionStringStore">Connection string data store</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        public DapperDataStore(
                   IConnectionStringStore connectionStringStore,
                   ConnectionStringKey connectionStringKey)
            : this(connectionStringStore)
        {
            this.connectionStringKey = connectionStringKey;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Method used for creating a connection to the database
        /// </summary>
        /// <returns>Database connection object</returns>
        private DbConnection CreateDbConnection()
        {
            var key = connectionStringKey.HasValue ? connectionStringKey.Value : ConnectionStringKey.ReportingInterface;
            var connectionString = connectionStringStore.GetKeyValue(key);

            return new SqlConnection(connectionString);
        }

        /// <summary>
        /// Method used for creating a connection to the database
        /// using the specified connection string key
        /// </summary>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Database connection object</returns>
        private DbConnection CreateDbConnection(
                                  ConnectionStringKey connectionStringKey
                             )
        {
            var connectionString = connectionStringStore.GetKeyValue(connectionStringKey);

            return new SqlConnection(connectionString);
        }
        #endregion

        #region Protected Methods
        #region Connection Helpers
        /// <summary>
        /// Connection helper method for accessing the database asynchronously
        /// that is used for use for buffered queries that return a type
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="getData">Delegate that mandates how data is retrieved</param>
        /// <returns>Asynchronous data accessing task with the data results</returns>
        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            using (var connection = CreateDbConnection())
            {
                await connection.OpenAsync();
                return await getData(connection);
            }
        }

        /// <summary>
        /// Connection helper method for accessing the database asynchronously
        /// that is used for use for buffered queries that return a type
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="getData">Delegate that mandates how data is retrieved</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task with the data results</returns>
        protected async Task<T> WithConnection<T>(
                                    Func<IDbConnection, Task<T>> getData,
                                    ConnectionStringKey connectionStringKey
                                )
        {
            using (var connection = CreateDbConnection(connectionStringKey))
            {
                await connection.OpenAsync();
                return await getData(connection);
            }
        }

        /// <summary>
        /// Connection helper method for accessing the database asynchronously
        /// that is used for buffered queries that do not return a type
        /// </summary>
        /// <param name="getData">Delegate that mandates that data access</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task WithConnection(Func<IDbConnection, Task> getData)
        {
            using (var connection = CreateDbConnection())
            {
                await connection.OpenAsync();
                await getData(connection);
                connection.Close();
            }
        }

        /// <summary>
        /// Connection helper method for accessing the database asynchronously
        /// that is used for buffered queries that do not return a type
        /// </summary>
        /// <param name="getData">Delegate that mandates that data access</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task WithConnection(
                                 Func<IDbConnection, Task> getData,
                                 ConnectionStringKey connectionStringKey
                             )
        {
            using (var connection = CreateDbConnection(connectionStringKey))
            {
                await connection.OpenAsync();
                await getData(connection);
                connection.Close();
            }
        }

        protected async Task<TResult> WithConnection<TRead, TResult>(
                                          Func<IDbConnection,
                                          Task<TRead>> getData,
                                          Func<TRead, Task<TResult>> process
                                      )
        {
            using (var connection = CreateDbConnection())
            {
                await connection.OpenAsync();
                var data = await getData(connection);
                var processedData = await process(data);
                connection.Close();

                return processedData;
            }
        }

        protected async Task<TResult> WithConnection<TRead, TResult>(
                                          Func<IDbConnection,
                                          Task<TRead>> getData,
                                          Func<TRead, Task<TResult>> process,
                                          ConnectionStringKey connectionStringKey
                                      )
        {
            using (var connection = CreateDbConnection(connectionStringKey))
            {
                await connection.OpenAsync();
                var data = await getData(connection);
                var processedData = await process(data);
                connection.Close();

                return processedData;
            }
        }
        #endregion

        #region Querying and CRUD Helpers
        protected void AddInputParameter(DynamicParameters parameters, string name, object value)
        {
            parameters.Add("@" + name, value, direction: ParameterDirection.Input);
        }

        protected void AddOutputParameter(DynamicParameters parameters, string name)
        {
            parameters.Add("@" + name, direction: ParameterDirection.Output);
        }

        protected void AddOutputParameter(DynamicParameters parameters, string name, DbType dbType)
        {
            parameters.Add("@" + name, direction: ParameterDirection.Output, dbType: dbType);
        }

        /// <summary>
        /// Method used for executing a stored procedure in the database without parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        protected void Execute(string storedProcedureName)
        {
            Execute(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for executing a stored procedure in the database without parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        protected void Execute(
                           string storedProcedureName,
                           ConnectionStringKey connectionStringKey
                       )
        {
            Execute(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for executing a stored procedure with parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        protected void Execute(string storedProcedureName, object parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                dbConnection.Execute(
                    storedProcedureName,
                    parameterSet,
                    commandType: CommandType.StoredProcedure,
                    transaction: null
                );

                dbConnection.Close();
            }
        }

        /// <summary>
        /// Method used for executing a stored procedure with parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        protected void Execute(
                           string storedProcedureName,
                           object parameterSet,
                           ConnectionStringKey connectionStringKey
                       )
        {
            using (var dbConnection = CreateDbConnection(connectionStringKey))
            {
                dbConnection.Execute(
                    storedProcedureName,
                    parameterSet,
                    commandType: CommandType.StoredProcedure,
                    transaction: null
                );

                dbConnection.Close();
            }
        }

        /// <summary>
        /// Method used for executing a stored procedure in the database asynchronously without parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task ExecuteAsync(string storedProcedureName)
        {
            await ExecuteAsync(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for executing a stored procedure in the database asynchronously without parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task ExecuteAsync(
                                 string storedProcedureName,
                                 ConnectionStringKey connectionStringKey
                             )
        {
            await ExecuteAsync(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for executing a stored procedure asynchronously with parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task ExecuteAsync(string storedProcedureName, object parameterSet)
        {
            await WithConnection(
                async c =>
                {
                    await c.ExecuteAsync(
                                storedProcedureName,
                                parameterSet,
                                commandType: CommandType.StoredProcedure,
                                transaction: null
                            );
                }
            );
        }

        /// <summary>
        /// Method used for executing a stored procedure asynchronously with parameters
        /// </summary>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task</returns>
        protected async Task ExecuteAsync(
                                 string storedProcedureName,
                                 object parameterSet,
                                 ConnectionStringKey connectionStringKey
                             )
        {
            await WithConnection(
                async c =>
                {
                    await c.ExecuteAsync(
                                storedProcedureName,
                                parameterSet,
                                commandType: CommandType.StoredProcedure,
                                transaction: null
                            );
                },
                connectionStringKey
            );
        }

        /// <summary>
        /// Method used for querying the database for a single value without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <returns>Value result</returns>
        public T QueryScalar<T>(string storedProcedureName)
        {
            return QueryScalar<T>(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for querying the database for a single value without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Value result</returns>
        public T QueryScalar<T>(
                      string storedProcedureName,
                      ConnectionStringKey connectionStringKey
                 )
        {
            return QueryScalar<T>(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for querying the database for a single value with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>Value result</returns>
        public T QueryScalar<T>(string storedProcedureName, object parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                T result = dbConnection.Query<T>(
                                            storedProcedureName,
                                            parameterSet,
                                            commandType: CommandType.StoredProcedure,
                                            transaction: null
                                        ).FirstOrDefault();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database for a single value with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Value result</returns>
        public T QueryScalar<T>(
                     string storedProcedureName,
                     object parameterSet,
                     ConnectionStringKey connectionStringKey
                 )
        {
            using (var dbConnection = CreateDbConnection(connectionStringKey))
            {
                T result = dbConnection.Query<T>(
                                            storedProcedureName,
                                            parameterSet,
                                            commandType: CommandType.StoredProcedure,
                                            transaction: null
                                        ).FirstOrDefault();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a single value without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <returns>Asynchronous data accessing task with the value result</returns>
        protected async Task<T> QueryScalarAsync<T>(string storedProcedureName)
        {
            return await QueryScalarAsync<T>(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a single value without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task with the value result</returns>
        protected async Task<T> QueryScalarAsync<T>(
                                    string storedProcedureName,
                                    ConnectionStringKey connectionStringKey
                                )
        {
            return await QueryScalarAsync<T>(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a single value with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>Asynchronous data accessing task with the value result</returns>
        protected async Task<T> QueryScalarAsync<T>(string storedProcedureName, object parameterSet)
        {
            return await WithConnection(async c =>
            {
                var result = await c.QueryAsync<T>(
                                         storedProcedureName,
                                         parameterSet,
                                         commandType: CommandType.StoredProcedure,
                                         transaction: null
                                     );

                return result.FirstOrDefault();
            });
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a single value with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task with the value result</returns>
        protected async Task<T> QueryScalarAsync<T>(
                                    string storedProcedureName,
                                    object parameterSet,
                                    ConnectionStringKey connectionStringKey
                                )
        {
            return await WithConnection(
                async c =>
                {
                    var result = await c.QueryAsync<T>(
                                             storedProcedureName,
                                             parameterSet,
                                             commandType: CommandType.StoredProcedure,
                                             transaction: null
                                         );

                    return result.FirstOrDefault();
                },
                connectionStringKey
            );
        }

        /// <summary>
        /// Method used for querying the database for a list without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(string storedProcedureName)
        {
            return QueryList<T>(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for querying the database for a list without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(
                           string storedProcedureName,
                           ConnectionStringKey connectionStringKey
                       )
        {
            return QueryList<T>(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for querying the database for a list with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(string storedProcedureName, object parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                var result = dbConnection.Query<T>(
                                              storedProcedureName,
                                              parameterSet,
                                              commandType: CommandType.StoredProcedure,
                                              transaction: null
                                          ).ToList();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database for a list with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(
                           string storedProcedureName,
                           object parameterSet,
                           ConnectionStringKey connectionStringKey
                       )
        {
            using (var dbConnection = CreateDbConnection(connectionStringKey))
            {
                var result = dbConnection.Query<T>(
                                              storedProcedureName,
                                              parameterSet,
                                              commandType: CommandType.StoredProcedure,
                                              transaction: null
                                          ).ToList();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database for a list with dynamic parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(string storedProcedureName, DynamicParameters parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                var result = dbConnection.Query<T>(
                                              storedProcedureName,
                                              parameterSet,
                                              commandType: CommandType.StoredProcedure,
                                              transaction: null
                                          ).ToList();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database for a list with dynamic parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>List of result value</returns>
        public List<T> QueryList<T>(
                           string storedProcedureName,
                           DynamicParameters parameterSet,
                           ConnectionStringKey connectionStringKey
                       )
        {
            using (var dbConnection = CreateDbConnection(connectionStringKey))
            {
                var result = dbConnection.Query<T>(
                                              storedProcedureName,
                                              parameterSet,
                                              commandType: CommandType.StoredProcedure,
                                              transaction: null
                                          ).ToList();
                dbConnection.Close();

                return result;
            }
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a list without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <returns>Asynchronous data accessing task with the list of result value</returns>
        protected async Task<List<T>> QueryListAsync<T>(string storedProcedureName)
        {
            return await QueryListAsync<T>(storedProcedureName, null);
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a list without parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task with the list of result value</returns>
        protected async Task<List<T>> QueryListAsync<T>(
                                          string storedProcedureName,
                                          ConnectionStringKey connectionStringKey
                                      )
        {
            return await QueryListAsync<T>(storedProcedureName, null, connectionStringKey);
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a list with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <returns>Asynchronous data accessing task with the list of result value</returns>
        protected async Task<List<T>> QueryListAsync<T>(string storedProcedureName, object parameterSet)
        {
            return await WithConnection(async c =>
            {
                var result = await c.QueryAsync<T>(
                                         storedProcedureName,
                                         parameterSet,
                                         commandType: CommandType.StoredProcedure,
                                         transaction: null
                                     );

                return result.ToList();
            });
        }

        /// <summary>
        /// Method used for querying the database asynchronously for a list with parameters
        /// </summary>
        /// <typeparam name="T">Result data type</typeparam>
        /// <param name="storedProcedureName">Stored procedure name</param>
        /// <param name="parameterSet">Parameters</param>
        /// <param name="connectionStringKey">Custom connection string key</param>
        /// <returns>Asynchronous data accessing task with the list of result value</returns>
        protected async Task<List<T>> QueryListAsync<T>(
                                          string storedProcedureName,
                                          object parameterSet,
                                          ConnectionStringKey connectionStringKey
                                      )
        {
            return await WithConnection(
                async c =>
                {
                    var result = await c.QueryAsync<T>(
                                             storedProcedureName,
                                             parameterSet,
                                             commandType: CommandType.StoredProcedure,
                                             transaction: null
                                         );

                    return result.ToList();
                },
                connectionStringKey
            );
        }
        #endregion
        #endregion

    }
}
