﻿using Sightly.ReportingApi.Business.Enums;
using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingApi.DataStore.Dapper.Accounts
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to account.
    /// </summary>
    public class AccountDataStore : DapperDataStore, IAccountDataStore
    {
        #region Constructors
        public AccountDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore, ConnectionStringKey.Operations)
        {
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Method used for retrieving all sub-account ids under an account.
        /// </summary>
        /// <param name="accountId">Parent account of account ids to be fetched.</param>
        /// <returns>List of sub-account ids under an account.</returns>
        public List<Guid> GetSubAccountIds(Guid accountId, Guid apiSessionId)
        {
            return QueryList<Guid>(
                        "Accounts.SubAccountIds_Get_WithSession",
                        new
                        {
                            AccountId = accountId,
                            ApiSessionId = apiSessionId
                        }
                   );
        }

        /// <summary>
        /// Method used for retrieving all sub-accounts under an account.
        /// </summary>
        /// <param name="accountId">Parent account of accounts to be fetched.</param>
        /// <returns>List of sub-accounts under an account.</returns>
        public List<Account> GetSubAccounts(Guid accountId, Guid apiSessionId)
        {
            return QueryList<Account>(
                        "Accounts.SubAccounts_GetByParentAccountId_WithSession",
                        new
                        {
                            AccountId = accountId,
                            ApiSessionId = apiSessionId
                        }
                   );
        }

        public List<AccountRefCode> GetSubAccountsByParentRefCode(string accountRefCode, Guid apiSessionId)
        {
            return QueryList<AccountRefCode>(
                    "Accounts.SubAccountRefCodes_GetByParentRefCode_WithSession",
                    new
                    {
                        AccountRefCode = accountRefCode,
                        ApiSessionId = apiSessionId
                    }
                );
        }

        #endregion
    }
}
