﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Accounts;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingApi.DataStore.Dapper.Accounts
{
    /// <summary>
    /// Class that contains the data access workflows related to the API session
    /// </summary>
    public class ApiSessionDataStore : DapperDataStore, IApiSessionDataStore
    {
        #region Constructors
        /// <summary>
        /// Constructor of the API session data store
        /// </summary>
        /// <param name="connectionStringStore">Configuration data store for connection strings</param>
        public ApiSessionDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to check if an API session is valid
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>True if the API session is valid; otherwise false</returns>
        public bool CheckIfValid(Guid apiSessionId)
        {
            return QueryScalar<bool>(
                       "Users.ApiSession_CheckIfValid",
                       new { ApiSessionId = apiSessionId }
                   );
        }

        /// <summary>
        /// Method used to extend an API session
        /// </summary>
        /// <param name="session">API session ID</param>
        public void Extend(Guid apiSessionId)
        {
            Execute(
                "Users.ApiSession_Extend",
                new { ApiSessionId = apiSessionId }
            );
        }

        /// <summary>
        /// Method used to retrieve the ID of the user of the specified API session
        /// </summary>
        /// <param name="apiSessionId">API session ID</param>
        /// <returns>The ID of the user of the specified API session</returns>
        public Guid GetUserId(Guid apiSessionId)
        {
            return QueryScalar<Guid>(
                       "Users.ApiSessionUserId_Get",
                       new { ApiSessionId = apiSessionId }
                   );
        }

        /// <summary>
        /// Method used to create an API session for the authenticated user
        /// </summary>
        /// <param name="session">API session information</param>
        public void Insert(ApiSession session)
        {
            Execute("Users.ApiSession_Insert",
                    new
                    {
                        session.ApiSessionId,
                        session.UserId
                    }
            );
        }

        /// <summary>
        /// Method used to log out the API session of the current user
        /// </summary>
        /// <param name="session">API session information</param>
        public void Logout(ApiSession session)
        {
            Execute("Users.ApiSession_Logout", new { session.ApiSessionId });
        }
        #endregion
    }
}
