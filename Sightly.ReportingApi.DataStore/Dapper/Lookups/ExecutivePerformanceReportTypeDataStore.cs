﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to report type.
    /// </summary>
    public class ExecutivePerformanceReportTypeDataStore : DapperDataStore, IExecutivePerformanceReportTypeDataStore
    {
        #region Constructor
        public ExecutivePerformanceReportTypeDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to retrieve report type lookups from a data store
        /// </summary>
        /// <param name="authentication">Claims principal</param>
        /// <returns>List of report type lookup</returns>
        public List<ReportType> GetList()
        {
            return new List<ReportType>
            {
                new ReportType
                {
                    ReportTypeId = new Guid("47ef665d-abb1-40cb-bea7-a5670094a710"),
                    Name = "Executive Summary CSV"
                },
                new ReportType
                {
                    ReportTypeId = new Guid("06ba7a87-ce19-4d5c-bb04-a5670094a710"),
                    Name = "Executive Summary PDF"
                }
               
            };
        }
        #endregion
    }
}