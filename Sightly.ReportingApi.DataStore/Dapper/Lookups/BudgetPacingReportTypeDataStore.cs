﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data accessing
    /// members related to report type.
    /// </summary>
    public class BudgetPacingReportTypeDataStore : DapperDataStore, IBudgetPacingReportTypeDataStore
    {
        #region Constructor
        public BudgetPacingReportTypeDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to retrieve report type lookups from
        /// a data store.
        /// </summary>
        /// <returns>
        ///     List of report types lookup.
        /// </returns>
        public List<ReportType> GetList()
        {
            return new List<ReportType>
            {
                new ReportType
                {
                    ReportTypeId = new Guid("C6C74E50-C242-483C-BEC5-60383522B950"),
                    Name = "Budget Pacing CSV"
                },
                new ReportType
                {
                    ReportTypeId = new Guid("0D301E56-B8C9-40D9-837E-4FB8205992A1"),
                    Name = "Budget Pacing PDF"
                }
            };
        }
        #endregion
    }
}
