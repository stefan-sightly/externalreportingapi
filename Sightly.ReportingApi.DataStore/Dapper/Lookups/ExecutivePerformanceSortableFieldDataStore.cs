﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to executive performance
    /// sortable fields.
    /// </summary>
    public class ExecutivePerformanceSortableFieldDataStore :
        DapperDataStore, IExecutivePerformanceSortableFieldDataStore
    {
        #region Constructors
        public ExecutivePerformanceSortableFieldDataStore(
                    IConnectionStringStore connectionStringStore
               )
            : base(connectionStringStore)
        {

        }
        #endregion

        #region Properties
        /// <summary>
        /// Method used for retrieving the list of 
        /// executive performance sortable fields.
        /// </summary>
        /// <returns>
        ///     List of executive performance sortable fields.
        /// </returns>
        public List<ExecutivePerformanceSortableField> GetList()
        {
            return QueryList<ExecutivePerformanceSortableField>(
                        "zLookups.ExecutivePerformanceSortableFields_Get"
                   );
        }
        #endregion
    }
}