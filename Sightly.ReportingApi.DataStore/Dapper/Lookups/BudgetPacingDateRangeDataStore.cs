﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to budget pacing date range.
    /// </summary>
    public class BudgetPacingDateRangeDataStore : DapperDataStore, IBudgetPacingDateRangeDataStore
    {
        #region Constructors
        public BudgetPacingDateRangeDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of budget pacing date range.
        /// </summary>
        /// <returns>List of budget pacing date range.</returns>
        public List<BudgetPacingDateRange> GetList()
        {
            return QueryList<BudgetPacingDateRange>(
                        "zLookups.BudgetPacingDateRange_GetList");
        }
        #endregion
    }
}