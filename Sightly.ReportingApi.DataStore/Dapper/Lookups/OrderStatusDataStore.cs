﻿using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to order status.
    /// </summary>
    public class OrderStatusDataStore : DapperDataStore, IOrderStatusDataStore
    {
        #region Constructors
        public OrderStatusDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of order status.
        /// </summary>
        /// <returns>List of order status.</returns>
        public List<OrderStatus> GetList()
        {
            return QueryList<OrderStatus>(
                        "zLookups.OrderStatus_GetList"
                   );
        }
        #endregion
    }
}