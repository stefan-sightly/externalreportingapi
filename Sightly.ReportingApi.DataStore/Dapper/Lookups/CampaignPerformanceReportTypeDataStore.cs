﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing
    /// members related to report type.
    /// </summary>
    public class CampaignPerformanceReportTypeDataStore : DapperDataStore, ICampaignPerformanceReportTypeDataStore
    {
        #region Constructor
        public CampaignPerformanceReportTypeDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to retrieve report type lookups from
        /// a data store
        /// </summary>
        /// <returns>
        ///     List of report type lookup
        /// </returns>
        public List<ReportType> GetList()
        {
            return new List<ReportType>
            {
                new ReportType
                {
                    ReportTypeId = new Guid("1F947311-ABE0-4CDC-A7AD-7D5CA0BD34C5"),
                    Name = "Campaign Summaries CSV"
                },
                new ReportType
                {
                    ReportTypeId = new Guid("37908B4B-E0E5-4D41-965C-FFD1F50FC7A8"),
                    Name = "Campaign Summaries Combined PDF"
                },
                new ReportType
                {
                    ReportTypeId = new Guid("3ceae943-f4ff-48c8-92c0-a5670094a710"),
                    Name = "Campaign Summaries Separate PDFs"
                }
            };
        }
        #endregion
    }
}
