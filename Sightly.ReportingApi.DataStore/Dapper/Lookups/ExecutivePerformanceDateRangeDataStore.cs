﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to executive performance
    /// date range.
    /// </summary>
    public class ExecutivePerformanceDateRangeDataStore : DapperDataStore, IExecutivePerformanceDateRangeDataStore
    {
        #region Constructors
        public ExecutivePerformanceDateRangeDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Method used for retrieving executive performance date range list.
        /// </summary>
        /// <returns>List of executive performance date range records.</returns>
        public List<ExecutivePerformanceDateRange> GetList()
        {
            return QueryList<ExecutivePerformanceDateRange>(
                        "zLookups.PerformanceSummaryReportType_GetList"
                   );
        }
        #endregion
    }
}