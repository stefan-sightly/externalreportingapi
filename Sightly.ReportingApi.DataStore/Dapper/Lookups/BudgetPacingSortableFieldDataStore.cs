﻿using Sightly.ReportingApi.Business.Interfaces.DataStores.Lookups;
using Sightly.ReportingApi.Business.Interfaces.Utilities.ConfigurationStores;
using Sightly.ReportingApi.DomainModels.Lookups;
using System.Collections.Generic;

namespace Sightly.ReportingApi.DataStore.Dapper.Lookups
{
    /// <summary>
    /// Class that implements the data-accessing 
    /// members related to budget pacing sortable
    /// fields.
    /// </summary>
    public class BudgetPacingSortableFieldDataStore : DapperDataStore, IBudgetPacingSortableFieldDataStore
    {
        #region Constructors
        public BudgetPacingSortableFieldDataStore(IConnectionStringStore connectionStringStore)
            : base(connectionStringStore)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used for retrieving the list of 
        /// budget pacing sortable fields.
        /// </summary>
        /// <returns>
        ///     List of budget pacing sortable fields.
        /// </returns>
        public List<BudgetPacingSortableField> GetList()
        {
            return QueryList<BudgetPacingSortableField>(
                        "zLookups.BudgetPacingFieldList_Get"
                   );
        }
        #endregion
    }
}