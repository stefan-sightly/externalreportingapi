﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated get T entity list by IDs method
    /// </summary>
    /// <typeparam name="T">Type of entities that would be retrieved</typeparam>
    /// <typeparam name="TKey">Type of the entity's primary key</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedListRetrieverByIds<T, TKey, A>
    {
        /// <summary>
        /// Method used for fetching list of T entities by Id
        /// </summary>
        /// <param name="ids">List of Ids of T entity to be fetch</param>
        /// <param name="authentication">Authentication value that would be used to retrieve the requested list via their ids</param>
        /// <returns>List of T entities</returns>
        List<T> GetListByIds(List<TKey> ids, A authentication);
    }
}