﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated insert T entity method
    /// </summary>
    /// <typeparam name="T">Type of entity to be inserted</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedInsertor<T, A>
    {
        /// <summary>
        /// Method used for inserting T entity
        /// </summary>
        /// <param name="entity">Entity to be inserted</param>
        /// <param name="authentication">Authentication value that would be used to insert the entity</param>
        void Insert(T entity, A authentication);
    }
}