﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated T entity list retriever method
    /// </summary>
    /// <typeparam name="T">Type of entities to be retrieved</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedListRetriever<T, A>
    {
        /// <summary>
        /// Method used for fetching list of T entities
        /// </summary>
        /// <returns>List of T entities</returns>
        /// <param name="authentication">Authentication value that would be used to retrieve the requested list.</param>
        List<T> GetList(A authentication);
    }
}