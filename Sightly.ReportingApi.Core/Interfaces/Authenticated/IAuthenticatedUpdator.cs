﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of an authenticated update T entity method
    /// </summary>
    /// <typeparam name="T">Entity that would be updated</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedUpdator<T, A>
    {
        /// <summary>
        /// Method used for updating entity passed
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        /// <param name="authentication">Authentication value that would be used to update the entity</param>
        void Update(T entity, A authentication);
    }
}