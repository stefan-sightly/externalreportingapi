﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of T authenticated entity existence checker method
    /// </summary>
    /// <typeparam name="T">Type of entity to be checked</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedExistenceChecker<T, A>
    {
        /// <summary>
        /// Method used for checking existence of T entity
        /// </summary>
        /// <param name="entity">Entity to be checked</param>
        /// <param name="authentication">Authentication value that would be used to checked the entity's existence.</param>
        /// <returns>Value that indicates the entity's existence</returns>
        bool CheckIfExisting(T entity, A authentication);
    }
}