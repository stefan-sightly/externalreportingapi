﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated T entity list retriever by parent ID
    /// </summary>
    /// <typeparam name="T">Type of entities to be retrieved</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedListRetrieverByParentId<T, TKey, A>
    {
        /// <summary>
        /// Method used for retrieving a list of T entities by their parent ID
        /// </summary>
        /// <typeparam name="TKey">Type of the entity's parent ID</typeparam>
        /// <param name="parentId">Parent ID that will be used to retrieve T entity list</param>
        /// <param name="authentication">Authentication value that would be used to retrieve entity list by their parent id.</param>
        /// <returns>List of T entities associated with the parent ID provided</returns>
        List<T> GetListByParentId(TKey parentId, A authentication);
    }
}