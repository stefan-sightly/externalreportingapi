﻿using Sightly.ReportingApi.Core.DataManagement;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Interfaces.Authenticated
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated T entity filtered list subset retriever method
    /// </summary>
    /// <typeparam name="T">Type of entities that would be retrieved</typeparam>
    /// <typeparam name="TFilter">Type of filter object</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedFilteredSubsetRetriever<T, TFilter, A>
    {
        /// <summary>
        /// Method used for fetching a filtered segment of a list
        /// </summary>
        /// <param name="filter">The object used for filtering the list of T entities</param>
        /// <param name="targetSubset">Information about the target subset under a data collection.</param>
        /// <param name="authentication">Authentication value that would be used to query the requested subset.</param>
        /// <returns>Filtered list segment of T entities</returns>
        SubsetRequestResult<T> GetFilteredSubset(TFilter filter, SubsetRequest targetSubset, A authentication);
    }
}
