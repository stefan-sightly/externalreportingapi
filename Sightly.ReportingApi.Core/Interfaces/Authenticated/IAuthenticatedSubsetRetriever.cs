﻿using Sightly.ReportingApi.Core.DataManagement;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated T entity subset retriever methods
    /// </summary>
    /// <typeparam name="T">Type of entities that would be retrieved</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedSubsetRetriever<T, A>
    {
        /// <summary>
        /// Method used for retrieving requested subsets.
        /// </summary>
        /// <param name="subsetRequest">Parameter that hold requested subset information.</param>
        /// <param name="authentication">Authentication entity that would be utilized to retrieve the requested subset.</param>
        /// <returns>Subset request results.</returns>
        SubsetRequestResult<T> GetSubset(SubsetRequest subsetRequest, A authentication);
    }
}