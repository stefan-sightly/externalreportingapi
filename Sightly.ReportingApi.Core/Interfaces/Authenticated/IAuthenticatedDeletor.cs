﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated delete T entity method.
    /// </summary>
    /// <typeparam name="T">Type of entity to be deleted</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedDeletor<T, A>
    {
        /// <summary>
        /// Method used for deleting T entity
        /// </summary>
        /// <param name="entity">Entity to be deleted</param>
        /// <param name="authentication">Authentication value that would be used to delete the entity</param>
        void Delete(T entity, A authentication);
    }
}