﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated deactivate T entity method
    /// </summary>
    /// <typeparam name="T">Entity to be deactivated</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedDeactivator<T, A>
    {
        /// <summary>
        /// Method used for deactivating T entity
        /// </summary>
        /// <param name="entity">Entity to be deactivated</param>
        /// <param name="authentication">Authentication value that would be used to deactivate the entity</param>
        void Deactivate(T entity, A authentication);
    }
}