﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated activate T entity method
    /// </summary>
    /// <typeparam name="T">Type of entity to be activated</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedActivator<T, A>
    {
        /// <summary>
        /// Method used for activating T entity
        /// </summary>
        /// <param name="entity">Entity to be activated</param>
        /// <param name="authentication">Authentication value that would be used to activate the entity</param>
        void Activate(T entity, A authentication);
    }
}