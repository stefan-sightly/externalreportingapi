﻿namespace Sightly.ReportingApi.Core.Authenticated.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of authenticated retrieve T entity by ID method
    /// </summary>
    /// <typeparam name="T">Entity that would be retrieved using ID</typeparam>
    /// <typeparam name="TKey">Type of the entity's primary key</typeparam>
    /// <typeparam name="A">Type of authentication entity</typeparam>
    public interface IAuthenticatedRetrieverById<T, TKey, A>
    {
        /// <summary>
        /// Method used for retrieving entity by its ID
        /// </summary>
        /// <param name="id">ID of entity to be retrieved</param>
        /// <param name="authentication">Authentication value that would be used to retrieve requested entity via its id.</param>
        /// <returns>The requested entity.</returns>
        T GetById(TKey id, A authentication);
    }
}