﻿using Sightly.ReportingApi.Core.DataManagement;
using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of subset retriever method
    /// </summary>
    /// <typeparam name="T">Type of entities that would be retrieved</typeparam>
    public interface ISubsetRetriever<T>
    {
        /// <summary>
        /// Method used for retrieving a entity list subsets.
        /// </summary>
        /// <param name="subsetRequest">Parameter that hold requested subset information.</param>
        /// <returns>Subset request results.</returns>
        SubsetRequestResult<T> GetSubset(SubsetRequest subsetRequest);
    }
}