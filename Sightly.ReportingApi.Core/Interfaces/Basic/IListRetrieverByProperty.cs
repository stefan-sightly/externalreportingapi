﻿
namespace Sightly.ReportingApi.Core.Interfaces.Basic
{
    // <summary>
    /// Interface used for mandating implementation of retrieve T entity via property method
    /// </summary>
    /// <typeparam name="T">Entity that would be retrieved</typeparam>
    /// <typeparam name="TKey">Type of the entity's property that would be used to retrieve entity</typeparam>
    public interface IRetrieverByProperty<T, TKey>
    {
        /// <summary>
        /// Method used for retrieving entity by its property.
        /// </summary>
        /// <param name="value">Property value of entity to be retrieved</param>
        /// <returns>T Entity</returns>
        T GetByProperty(TKey value);
    }
}
