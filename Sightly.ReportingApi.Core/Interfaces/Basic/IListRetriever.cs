﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of T entity list retriever method
    /// </summary>
    /// <typeparam name="T">Type of entities to be retrieved</typeparam>
    public interface IListRetriever<T>
    {
        /// <summary>
        /// Method used for fetching list of T entities
        /// </summary>
        /// <returns>List of T entities</returns>
        List<T> GetList();
    }
}