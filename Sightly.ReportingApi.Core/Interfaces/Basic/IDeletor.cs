﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of delete T entity method.
    /// </summary>
    /// <typeparam name="T">Type of entity to be deleted</typeparam>
    public interface IDeletor<T>
    {
        /// <summary>
        /// Method used for deleting T entity
        /// </summary>
        /// <param name="entity">Entity to be deleted</param>
        void Delete(T entity);
    }
}