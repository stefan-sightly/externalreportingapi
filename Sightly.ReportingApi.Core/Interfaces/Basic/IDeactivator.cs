﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of deactivate T entity method
    /// </summary>
    /// <typeparam name="T">Entity to be deactivated</typeparam>
    public interface IDeactivator<T>
    {
        /// <summary>
        /// Method used for deactivating T entity
        /// </summary>
        /// <param name="entity">Entity to be deactivated</param>
        void Deactivate(T entity);
    }
}