﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of retrieve T entity by ID method
    /// </summary>
    /// <typeparam name="T">Entity that would be retrieved using ID</typeparam>
    /// <typeparam name="TKey">Type of the entity's primary key</typeparam>
    public interface IRetrieverById<T, TKey>
    {
        /// <summary>
        /// Method used for retrieving entity by its ID
        /// </summary>
        /// <param name="id">ID of entity to be retrieved</param>
        /// <returns>T Entity</returns>
        T GetById(TKey id);
    }
}