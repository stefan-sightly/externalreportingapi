﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of T entity list retriever by parent ID
    /// </summary>
    /// <typeparam name="T">Type of entities to be retrieved</typeparam>
    public interface IListRetrieverByParentId<T, TKey>
    {
        /// <summary>
        /// Method used for retrieving a list of T entities by their parent ID
        /// </summary>
        /// <typeparam name="TKey">Type of the entity's parent ID</typeparam>
        /// <param name="parentId">Parent ID that will be used to retrieve T entity list</param>
        /// <returns>List of T entities associated with the parent ID provided</returns>
        List<T> GetListByParentId(TKey parentId);
    }
}