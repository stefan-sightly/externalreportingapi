﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of get T entity list by IDs method
    /// </summary>
    /// <typeparam name="T">Type of entities that would be retrieved</typeparam>
    /// <typeparam name="TKey">Type of the entity's primary key</typeparam>
    public interface IListRetrieverByIds<T, TKey>
    {
        List<T> GetListByIds(List<TKey> ids);
    }
}