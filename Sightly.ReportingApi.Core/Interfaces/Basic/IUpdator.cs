﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of an update T entity method
    /// </summary>
    /// <typeparam name="T">Entity that would be updated</typeparam>
    public interface IUpdator<T>
    {
        /// <summary>
        /// Method used for updating entity passed
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        void Update(T entity);
    }
}