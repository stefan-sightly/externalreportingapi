﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of insert T entity method
    /// </summary>
    /// <typeparam name="T">Type of entity to be inserted</typeparam>
    public interface IInsertor<T>
    {
        /// <summary>
        /// Method used for inserting T entity
        /// </summary>
        /// <param name="entity">Entity to be inserted</param>
        void Insert(T entity);
    }
}