﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of T entity existence checker method
    /// </summary>
    /// <typeparam name="T">Type of entity to be checked</typeparam>
    public interface IExistenceChecker<T>
    {
        /// <summary>
        /// Method used for checking existence of T entity
        /// </summary>
        /// <param name="entity">Entity to be checked</param>
        /// <returns>Value that indicates the entity's existence</returns>
        bool CheckIfExisting(T entity);
    }
}