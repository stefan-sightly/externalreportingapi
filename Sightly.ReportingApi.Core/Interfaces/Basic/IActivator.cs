﻿namespace Sightly.ReportingApi.Core.Basic.Interfaces
{
    /// <summary>
    /// Interface used for mandating implementation of activate T entity method
    /// </summary>
    /// <typeparam name="T">Type of entity to be activated</typeparam>
    public interface IActivator<T>
    {
        /// <summary>
        /// Method used for activating T entity
        /// </summary>
        /// <param name="entity">Entity to be activated</param>
        void Activate(T entity);
    }
}