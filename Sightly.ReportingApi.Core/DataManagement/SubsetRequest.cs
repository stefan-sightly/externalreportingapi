﻿namespace Sightly.ReportingApi.Core.DataManagement
{
    /// <summary>
    /// Class that represents information about the requested subset of a data collection.
    /// </summary>
    public class SubsetRequest
    {
        /// <summary>
        /// Property that represents requested subset 
        /// item count.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Property that determines how many subsets 
        /// exists prior to the requested subset.
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Property that determines the sort direction 
        /// of the requested data.
        /// </summary>
        public string SortDirection { get; set; }

        /// <summary>
        /// Property that determines the field that 
        /// would be used to sort the requested data.
        /// </summary>
        public string SortField { get; set; }
    }
}