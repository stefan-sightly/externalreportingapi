﻿using System.Collections.Generic;

namespace Sightly.ReportingApi.Core.DataManagement
{
    /// <summary>
    /// Class that represents the result of a subset request.
    /// </summary>
    /// <typeparam name="T">Type of entities to be retrieved.</typeparam>
    public class SubsetRequestResult<T>
    {
        /// <summary>
        /// Property that holds the result of the subset request.
        /// </summary>
        public List<T> RequestedSubset { get; set; }

        /// <summary>
        /// Property that holds the subset count.
        /// </summary>
        public int? SubsetCount
        {
            get
            {
                if (this.RequestedSubset == null)
                    return null;

                return this.RequestedSubset.Count;
            }
        }

        /// <summary>
        /// Property that holds the total count of the parent set.
        /// </summary>
        public int TotalCount { get; set; }
    }
}